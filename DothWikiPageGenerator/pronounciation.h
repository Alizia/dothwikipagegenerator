#pragma once

#include <string>
#include <vector>
#include <memory>
#include <map>

#pragma execution_character_set("utf-8")

using namespace std;

class Character
{
	public:
		Character(string c);

		string strChar;
		bool isVowel;
		bool isFrictative;
		bool isNasal;
		bool isApproximant;
		int priority;

		bool IsFrictative();
		bool IsNasal();
		bool IsApproximant();

		string GetIPA(bool& needJump, shared_ptr<Character> Before, shared_ptr<Character> After, bool isSyLastChar, bool isNextSyStressed);
};

bool IsDoubleChar(string dc);
bool IsGeminate(string s);

class Syllable
{
	public:
		Syllable();

		vector<shared_ptr<Character>> charList;
		string form;
		bool stressed;
		bool haveVowel;

		void AddChar(shared_ptr<Character> C);
};