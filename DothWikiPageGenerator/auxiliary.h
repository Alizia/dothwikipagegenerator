#pragma once

#include "word.h"

using namespace std;

class Aux : public Word
{
	public:
		Aux(string w, string wt, string t);

		string GetHeader();
};