#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "word.h"
#include "vocabularyData.h"
#include "wikiPage.h"

using namespace std;

int main()
{
	ifstream wordListTab("DothrakiVocabularyFromWiki.txt", ios::in);

	if (wordListTab)  // If the file is open
	{
		string line;
		//cout << "Bite 1" << endl;
		//system("PAUSE");
		while (getline(wordListTab, line)) // Get the next line
		{
			if (line != "")
			{
				istringstream ss(line);
				string val;

				vector<string> valsLine;

				while (getline(ss, val, ';')) // Get each information on the line
				{
					//cout << val << endl;
					valsLine.push_back(val);
				}
				
				VocabularyData vocabData(valsLine);
				
				WikiPage wikipage(vocabData);
				//cout << "Bite 2 line=" << line << endl;
				//system("PAUSE");
				wikipage.generateWikiPage();

			}
		}

		wordListTab.close();
		cout << "Generation done !" << endl;
		system("PAUSE");
	}
	else
	{
		cout << "Fail to open DothrakiVocabularyFromWiki.txt !" << endl;
		system("PAUSE");
	}
}