#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "word.h"
#include "vocabularyData.h"
#include "wikiPage.h"
#include "pronounciation.h"

using namespace std;

int main()
{
	const clock_t begin_time = clock();


	ifstream wordListTab("DothrakiVocabularyFromWiki.txt", ios::in);

	if (wordListTab)  // If the file is open
	{
		cout << "Generation in progress..." << endl;

		string line;
		//cout << "Bite 1" << endl;
		//system("PAUSE");
		while (getline(wordListTab, line)) // Get the next line
		{
			if (line != "")
			{
				istringstream ss(line);
				string val;

				vector<string> valsLine;

				while (getline(ss, val, ';')) // Get each information on the line
				{
					//cout << val << endl;
					valsLine.push_back(val);
				}
				//cout << "Bite 2 line=" << line << endl;
				VocabularyData vocabData(valsLine);
				//cout << "Bite 3 line=" << line << endl;
				WikiPage wikipage(vocabData);
				//cout << "Bite 4 line=" << line << endl;
				//system("PAUSE");

				wikipage.GenerateWikiPage(true);

			}
		}

		wordListTab.close();

		cout << "Generation done in " << float(clock() - begin_time) / CLOCKS_PER_SEC << "s !" << endl;
		system("PAUSE");
	}
	else
	{
		cout << "Fail to open DothrakiVocabularyFromWiki.txt !" << endl;
		system("PAUSE");
	}
}