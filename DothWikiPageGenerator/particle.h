#pragma once

#include "word.h"

using namespace std;

class Part : public Word
{
	public:
		Part(string w, string wt, string t);

		string GetHeader();
};