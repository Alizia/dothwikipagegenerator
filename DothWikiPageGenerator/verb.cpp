#include "verb.h"

Verb::Verb(string w, string wt, string t, string past) : Word(w, wt, t)
{
	bool isPhrase = IsPhrase(w);
	string verb;

	if (isPhrase)
	{
		verb = GetVerbFromPhrase(w);
	}
	else
	{
		verb = w;
	}
	
	verbType = GetVerbType(w, past);
	verbAssign = GetVerbAssign(wt);

	if (verbType == VerbType::LAT)
	{
		root = verb.substr(0, verb.size()-3);  // Remove "lat"
		//thema = "|thema=";
		thema += root.back();                  // Get the last vowel
		root = root.substr(0, root.size()-1);  // Get the root
		// Never epenthesis with -lat verbs
	}
	else
	{
		root = verb.substr(0, verb.size()-2);  // Remove "at", get the root
		//thema = "|epe=";

		if (IsEpenthesisCase(root))
		{
			thema += "e";
		}
	}
	conjugated = "";
}

Verb::Verb(const Verb& V) : Word(V)
{
	verbType = V.verbType;
	verbAssign = V.verbAssign;
	root = V.root;
	thema = V.thema;
	conjugated = V.conjugated;
}

string Verb::GetHeader()
{
	if (conjugated == "") // Infinitive
	{
		return "===Verb===\n{{head|doth|verb|infinitive}}\n\n";
	}
	else
	{
		return "===Verb===\n{{head|doth|verb form}}\n\n";
	}
}

string Verb::GetTranslation()
{
	return translation+"\n";
}

string Verb::GetInflection()
{
	string miniType;
	string inflection;
	switch (verbType)
	{
		case VerbType::LAT:
			if (IsVowelStarting(word))
			{
				miniType = "vVV";
			}
			else
			{
				miniType = "vCV";
			}
			break;
		case VerbType::AT:
			if (IsVowelStarting(word))
			{
				miniType = "vVC";
			}
			else
			{
				miniType = "vCC";
			}
			break;
	}

	if (verbType == VerbType::LAT)
	{
		inflection = "=====Inflection=====\n{{Template:dothra-"+miniType+"|root="+root+"|thema="+thema+"}}\n\n";
	}
	else
	{
		inflection = "=====Inflection=====\n{{Template:dothra-"+miniType+"|root="+root+"|epe="+thema+"}}\n\n";
	}

	return inflection;
}

string Verb::GetPast(VerbPositive vp, VerbPronoun vpn)
{
	string conj;
	
	if (word == "tat" && vp == VerbPositive::POSITIVE && (vpn == VerbPronoun::SG_FIRST)) // tat exception
	{
		return "et";
	}

	switch (verbType)
	{
		case VerbType::LAT:
			switch (vpn)
			{
				case VerbPronoun::SG_FIRST:
				case VerbPronoun::SG_SECOND:
				case VerbPronoun::SG_THIRD:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+thema;
						case VerbPositive::NEGATIVE:
							return root+"o";
						default:
							break;
					}
					break;
					
				case VerbPronoun::PL_FIRST:
				case VerbPronoun::PL_SECOND:
				case VerbPronoun::PL_THIRD:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+thema+"sh";
						case VerbPositive::NEGATIVE:
							return root+"osh"; //Replace the last vowel by 'osh'.
					}
					break;
			}
			break;


		case VerbType::AT:
			switch (vpn)
			{
				case VerbPronoun::SG_FIRST:
				case VerbPronoun::SG_SECOND:
				case VerbPronoun::SG_THIRD:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							if (IsEpenthesisCase(root))
							{
								return root+"e";
							}
							else
							{
								return root;
							}
							break;
						case VerbPositive::NEGATIVE:
							return root+"o";
							break;
					}
					break;
				case VerbPronoun::PL_FIRST:
				case VerbPronoun::PL_SECOND:
				case VerbPronoun::PL_THIRD:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+"ish";
						case VerbPositive::NEGATIVE:
							return root+"osh";
						default:
							break;
					}
					break;
			}
			break;
	}

	return ""; // Never used.
}

string Verb::GetPresent(VerbPositive vp, VerbPronoun vpn)
{
	switch (verbType)
	{
		case VerbType::LAT:
			switch (vpn)
			{
				case VerbPronoun::SG_FIRST:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+thema+"k";
						case VerbPositive::NEGATIVE:
							return root+"ok";
					}
					break;
				case VerbPronoun::PL_FIRST:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+thema+"ki";
						case VerbPositive::NEGATIVE:
							return root+"oki";
					}
					break;
				case VerbPronoun::SG_SECOND:
				case VerbPronoun::SG_THIRD:
				case VerbPronoun::PL_SECOND:
				case VerbPronoun::PL_THIRD:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+thema+"e";
						case VerbPositive::NEGATIVE:
							return root+thema+"o";
					}
					break;
			}
			break;


		case VerbType::AT:
			switch (vpn)
			{
				case VerbPronoun::SG_FIRST:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+"ak";
						case VerbPositive::NEGATIVE:
							return root+"ok";
					}
					break;
				case VerbPronoun::SG_THIRD:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+"a";
						case VerbPositive::NEGATIVE:
							return root+"o";
					}
					break;
				case VerbPronoun::PL_FIRST:
					switch (vp)
					{
						case VerbPositive::POSITIVE:
							return root+"aki";
						case VerbPositive::NEGATIVE:
							return root+"oki";
					}
					break;
				case VerbPronoun::SG_SECOND:
				case VerbPronoun::PL_SECOND:
				case VerbPronoun::PL_THIRD:
					return root+"i";
					break;
			}
			break;
	}

	return ""; // Never used.
}

string Verb::GetFuture(VerbPositive vp, VerbPronoun vpn)
{
	string present = GetPresent(vp, vpn);
	if (IsVowelStarting(present))
	{
		return "v"+present;
	}
	else
	{
		switch (vp)
		{
			case VerbPositive::POSITIVE:
				return "a"+present;
			case VerbPositive::NEGATIVE:
				present = GetPresent(VerbPositive::POSITIVE, vpn);
				return "o"+present;
		}
	}
	return ""; // Never used.
}

string Verb::GetFormal(VerbPositive vp)
{
	switch (verbType)
	{
		case VerbType::LAT:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return root+thema;
				case VerbPositive::NEGATIVE:
					return root+"o";
			}
			break;
		case VerbType::AT:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return root+"i";
				case VerbPositive::NEGATIVE:
					return root+"o";
			}
			break;
	}
	return ""; // Never used.
}

string Verb::GetInformal(VerbPositive vp)
{
	switch (verbType)
	{
		case VerbType::LAT:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return root+thema+"s";
				case VerbPositive::NEGATIVE:
					return root+"os";
			}
			break;
		case VerbType::AT:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return root+"as";
				case VerbPositive::NEGATIVE:
					return root+"os";
			}
			break;
	}
	return ""; // Never used.
}

string Verb::GetPastPart()
{
	switch (verbType)
	{
		case VerbType::LAT:
			return root+thema+"y";
		case VerbType::AT:
			return root+"ay";
	}

	return ""; // Never used.
}

string GetVerbFromPhrase(string phrase)
{
	istringstream ss(phrase);
	string word;
	vector<string> words;

	while (getline(ss, word, ' ')) // Get each information on the line
	{
		if (word.substr(word.size()-2, 2) == "at")
		{
			return word;
		}
	}

	return "";
}

VerbType GetVerbType(string v, string past)
{
	string ending = v.substr(v.size()-3, 3);

	bool isVowelEnding = true;

	if (ending == "lat")
	{
		if (past != "")
		{
			isVowelEnding = IsVowelEnding(past);
		}

		if (isVowelEnding)
		{
			return VerbType::LAT;
		}
		else
		{
			return VerbType::AT;
		}
	}
	else
	{
		return VerbType::AT;
	}
};

VerbAssign GetVerbAssign(string wt)
{
	if (wt.find("abl."))
	{
		return VerbAssign::ABLATIVE;
	}
	else if (wt.find("all."))
	{
		return VerbAssign::ALLATIVE;
	}
	else if (wt.find("gen."))
	{
		return VerbAssign::GENITIVE;
	}
	return VerbAssign::NONE;
}