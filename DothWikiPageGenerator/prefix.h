#pragma once

#include "word.h"

using namespace std;

class Prefix : public Word
{
	public:
		Prefix(string w, string wt, string t);

		string GetHeader();
};