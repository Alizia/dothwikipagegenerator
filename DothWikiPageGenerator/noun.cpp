#include "noun.h"

NounType GetNounType(string t)
{
	if (t == "n.")
	{
		return NounType::UNKNOWN;
	}
	else if (t == "ni.")
	{
		return NounType::INANIMATE_A;
	}
	else if (t == "na.")
	{
		return NounType::ANIMATE;
	}
	else if (t == "ni. / na.")
	{
		return NounType::BOTH;
	}
	else
	{
		return NounType::UNKNOWN;
	}
}

Noun::Noun(string w, string wt, string t, string acc) : Word(w, wt, t)
{
	nounType = GetNounType(wt);

	bool isVowelEnding = IsVowelEnding(w);
	bool isEpenthesisCase;

	if (isVowelEnding)
	{
		root = w.substr(0, w.size()-1);
		thema = w.back();

		if (w == "zoqwa")
		{
			//cout<< "Bite 1 w.back()=" << w.back() << endl;
		}
	}
	else
	{
		if (w == "zoqwa")
		{
			//cout<< "Bite 2" << endl;
		}
		
		root = w;
		thema = "";
	}

	isEpenthesisCase = IsEpenthesisCase(root);

	if (nounType == NounType::INANIMATE_A)
	{
		if (acc != "")
		{
			if (acc.back() == 'e')
			{
				string accRoot = acc.substr(0, acc.size()-1);
				if (accRoot == root)
				{
					thema += "|class=b";
					nounType = NounType::INANIMATE_B;
				}
				else
				{
					thema += "|stem="+accRoot+"|class=b";
				}
			}
			else
			{
				if (acc != root)
				{
					thema += "|stem="+acc;
				}
			}
		}
		else
		{
			if (isEpenthesisCase)
			{
				thema += "|class=b";
				nounType = NounType::INANIMATE_B;
			}
		}
	}
}

string Noun::GetHeader()
{
	string animate;

	switch (nounType)
	{
		case NounType::ANIMATE:
			animate = "animate";
			break;
		case NounType::INANIMATE_A:
		case NounType::INANIMATE_B:
			animate = "inanimate";
			break;
		case NounType::BOTH:
			animate = "animate/inanimate";
			break;
		case NounType::UNKNOWN:
			animate = "unknown animate type";
			break;
	}

	return "====Noun====\n{{head|doth|noun|nominative, "+animate+"}}\n\n";
}

string Noun::GetInflection()
{
	string mini_animate;
	string inflection;

	switch (nounType)
	{
		case NounType::ANIMATE:
			mini_animate = "na";
			break;
		case NounType::INANIMATE_A:
		case NounType::INANIMATE_B:
			mini_animate = "ni";
			break;
		case NounType::BOTH:
			mini_animate = "ni";
			break;
		case NounType::UNKNOWN:
			mini_animate = "ni";
			break;
		default:
			break;
	}


	inflection = "=====Inflection=====\n{{Template:dothra-" + mini_animate + "|root=" + root + "|thema=" + thema + "}}\n\n";

	if (nounType == NounType::BOTH) // if the noun can be both, just add the animate at the end
	{
		inflection += "{{Template:dothra-na|root=" + root + "|thema=" + thema + "}}\n";
	}

	return inflection;
}
