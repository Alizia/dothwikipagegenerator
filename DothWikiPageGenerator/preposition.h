#pragma once

#include "word.h"

using namespace std;

class Prep : public Word
{
	public:
		Prep(string w, string wt, string t);

		string GetHeader();
};