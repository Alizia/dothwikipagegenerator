#pragma once

#include "word.h"

using namespace std;

class Num : public Word
{
	public:
		Num(string w, string wt, string t);

		string GetHeader();
};