﻿#include "pronounciation.h"
#include "word.h"

vector<string> frictativeList
{
	"th",
	"s",
	"z",
	"zh",
	"kh",
	"f",
	"v",
};

vector<string> nasalList
{
	"m",
	"n",
};

vector<string> approximantList
{
	"w",
	"y",
	"r",
	"l",
};

vector<string> doubleCharList
{
	"ch",
	"sh",
	"th",
	"zh",
	"kh",
};

map<string, string> simpleIPAMap
{
	{"f" , "f"},
	{"g" , "g"},
	{"k" , "k"},
	{"m" , "m"},
	{"q" , "q"},
	{"s" , "s"},
	{"v" , "v"},
	{"w" , "w"},
	{"y" , "j"},
	{"z" , "z"},
	{"ch", "t͡ʃ"},
	{"kh", "x"},
	{"sh", "ʃ"},
	{"th", "θ"},
	{"zh", "ʒ"},
};

map<string, string> geminateMap
{
	{"cch", "t.t͡ʃ"},
	{"hh" , "ħ.h"},
	{"jj" , "d.d͡ʒ"},
	{"kkh", "x.x"},
	{"rr" , "r.r"},
	{"ssh", "ʃ.ʃ"},
	{"tth", "θ.θ"},
	{"zzh", "ʒ.ʒ"},
};

map<string, string> geminateStressedMap
{
	{"cch", "t.ˈt͡ʃ"},
	{"hh" , "ħ.ˈh"},
	{"jj" , "d.ˈd͡ʒ"},
	{"kkh", "x.ˈx"},
	{"rr" , "r.ˈr"},
	{"ssh", "ʃ.ˈʃ"},
	{"tth", "θ.ˈθ"},
	{"zzh", "ʒ.ˈʒ"},
};

map<string, string> tdnlMap
{
	{"t", "t̪"},
	{"d" , "d̪"},
	{"n" , "n̪"},
	{"l", "l̪"},
};

map<string, string> qaeiMap
{
	{"a", "ɑ"},
	{"e" , "ɛ"},
	{"i" , "e"},
};

map<string, int> charPrioMap
{
	{"j" , 1},
	{"ch", 1},
	{"q" , 1},
	{"q" , 1},
	{"t" , 1},
	{"d" , 1},
	{"g" , 1},
	{"k" , 1},

	{"h" , 2},
	{"th", 2},
	{"s" , 2},
	{"sh", 2},
	{"z" , 2},
	{"zh", 2},
	{"kh", 2},
	{"f" , 2},
	{"k" , 2},
	{"v" , 2},

	{"m" , 3},
	{"n" , 3},

	{"w" , 4},
	{"y" , 4},
	{"r" , 4},
	{"l" , 4},

	{"a" , 5},
	{"e" , 5},
	{"i" , 5},
	{"o" , 5},
};

Character::Character(string c)
{
	strChar = c;
	isVowel = IsVowel(strChar);
	isFrictative = IsFrictative();
	isNasal = IsNasal();
	isApproximant = IsApproximant();
	priority = charPrioMap[c];
}

bool Character::IsFrictative()
{
	if (find(frictativeList.begin(), frictativeList.end(), strChar) != frictativeList.end())
	{
		return true;
	}
	return false;
}

bool Character::IsNasal()
{
	if (find(nasalList.begin(), nasalList.end(), strChar) != nasalList.end())
	{
		return true;
	}
	return false;
}

bool Character::IsApproximant()
{
	if (find(approximantList.begin(), approximantList.end(), strChar) != approximantList.end())
	{
		return true;
	}
	return false;
}

string Character::GetIPA(bool& needJump, shared_ptr<Character> before, shared_ptr<Character> after, bool isSyLastChar, bool isNextSyStressed)
{
	//cout << "Bite 1 char=" << strChar << endl;
	
	if (simpleIPAMap.find(strChar) != simpleIPAMap.end())
	{
		return simpleIPAMap[strChar];
	}
	
	if (after != nullptr)
	{
		//if (simpleIPAMap.find(strChar+after->strChar) != simpleIPAMap.end())
		//{
		//	index++;
		//	return simpleIPAMap[strChar+after->strChar];
		//}
		
		string geminate = strChar+after->strChar;
		map<string, string>::iterator it = geminateMap.find(geminate);

		if (it != geminateMap.end()) // if it's a geminate
		{
			needJump = true; // Jump the next char.
			if (isNextSyStressed)
			{
				return geminateStressedMap.find(geminate)->second; // If the next syllable is stressed, need the use the stressed map
			}
			return it->second;
		}

		if (before != nullptr)
		{
			if (strChar == "r")
			{
				return "ɾ";
			}
		}

		if (strChar == "n")
		{
			if (after->strChar == "k" 
				|| after->strChar == "g"
				|| after->strChar == "kh")
			{
				return "ŋ";
			}
		}

		if (strChar == "t" 
			|| strChar == "d" 
			|| strChar == "n" 
			|| strChar == "l")
		{
			if (after->strChar == "s" || after->strChar == "z")
			{
				return strChar;
			}
		}
		
	}
	else // end of the word
	{
		if (strChar == "r")
		{
			return "r";
		}
	}

	if (before != nullptr)
	{
		if (strChar == "o")
		{
			if (before->strChar == "t"
				|| before->strChar == "d"
				|| before->strChar == "n"
				|| before->strChar == "l")
			{
				return "ɤ";
			}

			if (before->strChar == "k"
				|| before->strChar == "g"
				|| before->strChar == "kh")
			{
				return "u";
			}

			if (before->strChar == "q")
			{
				return "ɔ";
			}
		}

		if (strChar == "a")
		{
			if (before->strChar == "q")
			{
				return "ɑ";
			}
		}

		if (strChar == "e")
		{
			if (before->strChar == "q")
			{
				return "ɛ";
			}
		}

		if (strChar == "i")
		{
			if (before->strChar == "q")
			{
				return "e";
			}
		}

		if (strChar == "a" 
			|| strChar == "e" 
			|| strChar == "i")
		{
			if (before->strChar == "q")
			{
				return qaeiMap[strChar];
			}
		}

		if (strChar == "n")
		{
			if (before->strChar == "k" 
				|| before->strChar == "g"
				|| before->strChar == "kh")
			{
				return "ŋ";
			}
		}

		if (strChar == "t" 
			|| strChar == "d" 
			|| strChar == "n" 
			|| strChar == "l")
		{
			if (before->strChar == "s" || before->strChar == "z")
			{
				return strChar;
			}
		}
	}
	else // start of the word
	{
		if (strChar == "r")
		{
			return "r";
		}
	}

	if (strChar == "t" 
		|| strChar == "d" 
		|| strChar == "n" 
		|| strChar == "l")
	{
		return tdnlMap[strChar];
	}

	if (strChar == "a" 
		|| strChar == "e" 
		|| strChar == "i" 
		|| strChar == "o")
	{
		return strChar;
	}

	if (strChar == "h")
	{
		if (isSyLastChar)
		{
			return "ħ";
		}
		else
		{
			return "h";
		}
	}

	//cout << "Bite 2" << endl;
	return "";
}

bool IsDoubleChar(string dc)
{
	if (find(doubleCharList.begin(), doubleCharList.end(), dc) != doubleCharList.end())
	{
		return true;
	}
	return false;
}

bool IsGeminate(string s)
{
	if (geminateMap.find(s) != geminateMap.end())
	{
		return true;
	}
	return false;
}

Syllable::Syllable()
{
	stressed = false;
	haveVowel = false;
	form = "";
}

void Syllable::AddChar(shared_ptr<Character> C)
{
	//charList.push_back(C);
	charList.insert(charList.begin(), C); // Syllabification is reversed, right to left

	string f;
	if (C->isVowel)
	{
		f = "V";
	}
	else
	{
		f = "C";
	}

	form.insert(0, f);
}