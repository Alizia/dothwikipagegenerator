#include "vocabularyData.h"

vector<string> WordTypeList
{
	"n.",
	"ni.",
	"na.",
	"ni./na.",
	"prop.n.",
	"v.",
	"v.&rarr-abl.",
	"v.&rarr-all.",
	"v.&rarr-gen.",
	"vin.",
	"vtr.",
	"vtr.&rarr-abl.",
	"vtr.&rarr-all.",
	"vtr.&rarr-gen.",
	"vtr.&rarr-acc.",
	"adj.",
	"adv.",
	"loc. adv.",
	"num.",
	"det.",
	"det.&rarr-gen.",
	"dem. adj.",
	"dem. pn.",
	"pn.",
	"prep.",
	"prep.&rarr-abl.",
	"prep.&rarr-all.",
	"prep.&rarr-gen.",
	"prep.&rarr-nom.",
	"v. aux.",
	"intj.",
	"conj.",
	"part.",
	"prefix",
	"phrase",
};

VocabularyData::VocabularyData(vector<string> vs)
{
	word = vs.at(0);
	pronunciation = vs.at(1);

	for(int i=2; i<vs.size(); i++)
	{
		string value = vs.at(i);

		if (IsWordType(value))
		{
			DefinitionData def;
			def.wordtype = value;
			value = vs.at(i+1);
			def.definition = value;
			i++; //Jump the definition value.

			definitions.push_back(def);
		}
		else if (IsSup(value))
		{
			PastAccData sup;

			istringstream ss(value);
			string val;
			vector<string> supData;

			while (getline(ss, val, '|')) // Get each information on the line
			{
				//cout << val << endl;
				supData.push_back(val);
			}

			if (supData[0] == "Vsup")
			{
				sup.supType = SupType::VSUP;
			}
			else
			{
				sup.supType = SupType::NSUP;
			}
			sup.value = supData[1];

			sups.push_back(sup);
		}
		else if (IsQuote(value))
		{
			QuoteData quote;

			istringstream ss(value);
			string val;
			vector<string> quoteData;

			while (getline(ss, val, '|')) // Get each information on the line
			{
				//cout << val << endl;
				quoteData.push_back(val);
			}

			quote.dothraki = quoteData[1];
			quote.english = quoteData[2];

			quotes.push_back(quote);
		}
		else
		{
			cout << "Unknown value = '" << value << "' index = " << i << " in the word '" << word << "'" << endl;
			system("PAUSE");
		}
	}
}

bool VocabularyData::IsWordType(string s)
{
	if (find(WordTypeList.begin(), WordTypeList.end(), s) != WordTypeList.end())
	{
		return true;
	}
	
	return false;
}

bool VocabularyData::IsSup(string s)
{
	if (s.find("Vsup|") != string::npos)
	{
		return true;
	}
	if (s.find("Nsup|") != string::npos)
	{
		return true;
	}
	return false;
}

bool VocabularyData::IsQuote(string s)
{
	if (s.find("Quote|") != string::npos)
	{
		return true;
	}
	
	return false;
}
