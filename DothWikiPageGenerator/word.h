#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "vocabularyData.h"
#include "pronounciation.h"

using namespace std;

enum class WordType
{
	NOUN,
	VERB,
	ADJECTIVE,
	INTERJECTION,
	ADVERB,
	CONJUNCTION,
	PHRASE,
	NUMERAL,
	AUXILIARY,
	DETERMINER,
	PRONOUN,
	PREPOSITION,
	PARTICLE,
	PREFIX,
	UNKNOWN,
};

enum class AdjType
{
	POSITIVE,
	COMPARATIVE,
	SUPERLATIVE,
	NEGATIVE,
	CONTRASTIVE,
	SUBLATIVE,
};

class Word
{
	public:
		Word(string w, string wt, string translation);

		string word;
		WordType wordType;
		string translation;

		virtual string GetHeader();
		string GetTranslation();
		virtual string GetInflection();
};

class Default : public Word
{
	public:
		Default(string w, string wt, string t);
};

WordType GetWordType(string t);
bool IsVowel(string c);
bool IsVowelEnding(string w);
bool IsVowelStarting(string w);
bool IsEpenthesisCase(string w);
bool IsPhrase(string w);