#pragma once

#include "word.h"

using namespace std;

class Adj : public Word
{
	public:
		Adj(string w, string wt, string t, AdjType at);

		AdjType adjType;
		string compared;

		string GetHeader();
		string GetTranslation();

		string GetComparative();
		string GetSuperlative();
		string GetNegative();
		string GetContrastive();
		string GetSublative();
};