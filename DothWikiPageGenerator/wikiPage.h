#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#include "adjective.h"
#include "adverb.h"
#include "auxiliary.h"
#include "conjunction.h"
#include "determiner.h"
#include "interjection.h"
#include "noun.h"
#include "numeral.h"
#include "particle.h"
#include "phrase.h"
#include "prefix.h"
#include "preposition.h"
#include "pronoun.h"
#include "verb.h"
#include "word.h"

using namespace std;

class WikiPage
{
	public:
		WikiPage(VocabularyData vd);
		WikiPage(shared_ptr<Word> w);

		string word;
		string pronunciation;
		vector<vector<shared_ptr<Syllable>>> syllableList;
		vector<shared_ptr<Word>> definitionsList;
		vector<QuoteData> quoteList;
		string initTemplate;

		void GenerateWikiPage(bool etymology);
		string GetInitTemplate(bool etymology);

		void Syllabify();
		void Stressify();
		void SetPronunciation();
};

void WriteComparisonWikiPage(string w, string fileName, AdjType at);
void GenerateComparisonWikiPage(shared_ptr<Adj> A);

string GetConjugatedDefinition(string infinitive, VerbPositive vp, VerbTense vt, VerbPronoun vpn);
void AddConjugationToList(vector<Verb>& ConjugationObjectList, Verb& Conjugated, VerbPositive vp, VerbTense vt, VerbPronoun vpn);
void WriteConjugationWikiPage(vector<Verb>& ConjugationObjectList, string initTemplate);
void GenerateConjugationWikiPage(shared_ptr<Verb> V, string initTemplate);