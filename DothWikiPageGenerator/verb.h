#pragma once

#include "word.h"

using namespace std;

enum class VerbType
{
	LAT,
	AT,
};

enum class VerbAssign
{
	NONE,
	ABLATIVE,
	ALLATIVE,
	GENITIVE,
};

enum class VerbPositive
{
	POSITIVE,
	NEGATIVE,
};

enum class VerbTense
{
	INFINITIVE,
	PRESENT,
	PAST,
	FUTURE,
	FORMAL,
	INFORMAL,
	PASTPART,
};

enum class VerbPronoun
{
	SG_FIRST,
	SG_SECOND,
	SG_THIRD,
	PL_FIRST,
	PL_SECOND,
	PL_THIRD,
};

class Verb : public Word
{
	public:
		Verb(string w, string wt, string t, string past="");
		Verb(const Verb& V);

		VerbType verbType;
		VerbAssign verbAssign;

		string root;
		string thema;
		string conjugated;

		string GetHeader();
		string GetTranslation();
		string GetInflection();

		string GetPast(VerbPositive vp, VerbPronoun vpn);
		string GetPresent(VerbPositive vp, VerbPronoun vpn);
		string GetFuture(VerbPositive vp, VerbPronoun vpn);
		string GetFormal(VerbPositive vp);
		string GetInformal(VerbPositive vp);
		string GetPastPart();
};

string GetVerbFromPhrase(string phrase);
VerbType GetVerbType(string v, string past);
VerbAssign GetVerbAssign(string wt);