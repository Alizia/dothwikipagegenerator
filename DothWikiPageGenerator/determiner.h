#pragma once

#include "word.h"

using namespace std;

class Det : public Word
{
	public:
		Det(string w, string wt, string t);

		string GetHeader();
};