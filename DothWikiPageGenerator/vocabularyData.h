#pragma once

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

enum class SupType
{
	VSUP,
	NSUP,
};

struct DefinitionData
{
	string wordtype;
	string definition;
};

struct PastAccData
{
	SupType supType;
	string value;
};

struct QuoteData
{
	string dothraki;
	string english;
};

class VocabularyData
{
	public:
		VocabularyData(vector<string> vs);

		string word;
		string pronunciation;
		vector<DefinitionData> definitions;
		vector<PastAccData> sups;
		vector<QuoteData> quotes;

		bool IsWordType(string s);
		bool IsSup(string s);
		bool IsQuote(string s);
};