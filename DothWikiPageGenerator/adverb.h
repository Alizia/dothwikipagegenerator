#pragma once

#include "word.h"

using namespace std;

class Adverb : public Word
{
	public:
		Adverb(string w, string wt, string t);

		string GetHeader();
};