#pragma once

#include "word.h"

using namespace std;

class Conj : public Word
{
	public:
		Conj(string w, string wt, string t);

		string GetHeader();
};
