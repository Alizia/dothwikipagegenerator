#include "adjective.h"

Adj::Adj(string w, string wt, string t, AdjType at) : Word(w, wt, t)
{
	adjType = at;
	compared = "";
}

string Adj::GetHeader()
{
	if (adjType == AdjType::POSITIVE)
	{
		return "====Adjective====\n{{doth-adj}}\n\n";
	}
	else
	{
		return "====Adjective====\n{{doth-adj|~}}\n\n";
	}
}

string Adj::GetTranslation()
{
	string temp;
	switch (adjType)
	{
		case AdjType::POSITIVE:
			return "# "+translation;
			break;
		case AdjType::COMPARATIVE:
			temp = "comparative";
			break;
		case AdjType::SUPERLATIVE:
			temp = "superlative";
			break;
		case AdjType::NEGATIVE:
			temp = "negative basic";
			break;
		case AdjType::CONTRASTIVE:
			temp = "contrastive";
			break;
		case AdjType::SUBLATIVE:
			temp = "sublative";
			break;
	}
	
	return "# {{doth-"+temp+" of|"+word+"}}";
}

string Adj::GetComparative()
{
	string comp;

	if (IsVowelStarting(word))
	{
		comp = "as";
	}
	else
	{
		comp = "a";
	}

	comp += word;

	if (IsVowelEnding(word))
	{
		comp += "n";
	}
	else
	{
		comp += "an";
	}

	return comp;
}

string Adj::GetSuperlative()
{
	string sup;

	if (IsVowelStarting(word))
	{
		sup = "as";
	}
	else
	{
		sup = "a";
	}

	sup += word;

	if (IsVowelEnding(word))
	{
		sup += "naz";
	}
	else
	{
		sup += "anaz";
	}

	return sup;
}

string Adj::GetNegative()
{
	string neg;

	if (IsVowelStarting(word))
	{
		neg = "os";
	}
	else
	{
		neg = "o";
	}

	neg += word;

	return neg;
}

string Adj::GetContrastive()
{
	string cont;

	if (IsVowelStarting(word))
	{
		cont = "os";
	}
	else
	{
		cont = "o";
	}

	cont += word;

	if (IsVowelEnding(word))
	{
		cont += "n";
	}
	else
	{
		cont += "an";
	}

	return cont;
}

string Adj::GetSublative()
{
	string sub;

	if (IsVowelStarting(word))
	{
		sub = "as";
	}
	else
	{
		sub = "a";
	}

	sub += word;

	if (IsVowelEnding(word))
	{
		sub += "noz";
	}
	else
	{
		sub += "anoz";
	}

	return sub;
}