#include "word.h"

vector<string> wikiNounTypeList
{
	"n.",
	"ni.",
	"na.",
	"ni./na.",
};

vector<string> wikiVerbTypeList
{ 
	"v.",
	"v.&rarr-abl.",
	"v.&rarr-all.",
	"v.&rarr-gen.",
	"vin.",
	"vtr.",
	"vtr.&rarr-abl.",
	"vtr.&rarr-all.",
	"vtr.&rarr-gen.",
	"vtr.&rarr-acc.",
};

vector<string> wikiDetTypeList
{
	"det.",
	"det.&rarr-gen.",
	"dem. adj.",
	"dem. pn.",
};

vector<string> wikiPrepTypeList
{
	"prep.",
	"prep.&rarr-abl.",
	"prep.&rarr-all.",
	"prep.&rarr-gen.",
	"prep.&rarr-nom.",
};

WordType GetWordType(string t)
{
	if (find(wikiNounTypeList.begin(), wikiNounTypeList.end(), t) != wikiNounTypeList.end())
	{
		return WordType::NOUN;
	}
	else if (find(wikiVerbTypeList.begin(), wikiVerbTypeList.end(), t) != wikiVerbTypeList.end())
	{
		return WordType::VERB;
	}
	else if (t == "adj.")
	{
		return WordType::ADJECTIVE;
	}
	else if (t == "adv." || t == "loc. adv.")
	{
		return WordType::ADVERB;
	}
	else if (t == "num.")
	{
		return WordType::NUMERAL;
	}
	else if (find(wikiDetTypeList.begin(), wikiDetTypeList.end(), t) != wikiDetTypeList.end())
	{
		return WordType::DETERMINER;
	}
	else if (t == "pn.")
	{
		return WordType::PRONOUN;
	}
	else if (find(wikiPrepTypeList.begin(), wikiPrepTypeList.end(), t) != wikiPrepTypeList.end())
	{
		return WordType::PREPOSITION;
	}
	else if (t == "v. aux.")
	{
		return WordType::AUXILIARY;
	}
	else if (t == "intj.")
	{
		return WordType::INTERJECTION;
	}
	else if (t == "conj.")
	{
		return WordType::CONJUNCTION;
	}
	else if (t == "part.")
	{
		return WordType::PARTICLE;
	}
	else if (t == "prefix")
	{
		return WordType::PREFIX;
	}
	else if (t == "phrase")
	{
		return WordType::PHRASE;
	}
	else
	{
		return WordType::UNKNOWN;
	}
}

vector<string> vowelList
{
	"a",
	"e",
	"i",
	"o",
};

bool IsVowel(string c)
{
	if (find(vowelList.begin(), vowelList.end(), c) != vowelList.end())
	{
		return true;
	}
	return false;
}

bool IsVowelEnding(string w)
{
	string ending(1,w.back());
	return IsVowel(ending);
}

bool IsVowelStarting(string w)
{
	string starting(1,w.front());
	return IsVowel(starting);
}

bool IsEpenthesisCase(string w)
{
	if (w.back() == 'w' or w.back() == 'g' or w.back() == 'q')
	{
		return true;
	}

	if (w.back() == 'r' or w.back() == 'l' or w.back() == 'y')
	{
		string s(1, w[w.size() - 2]);
		if (!IsVowel(s))
		{
			return true;
		}
	}

	return false;
}

Word::Word(string w, string wt, string t)
{
	word = w;
	wordType = GetWordType(wt);
	translation = t;
}

string Word::GetHeader()
{
	return "";
}

string Word::GetTranslation()
{
	return "# "+translation+"\n";
}

string Word::GetInflection()
{
	return "";
}



Default::Default(string w, string wt, string t) : Word(w, wt, t)
{

}

bool IsPhrase(string w)
{
	istringstream ss(w);
	string word;
	vector<string> words;

	while (getline(ss, word, ' ')) // Get each information on the line
	{
		words.push_back(word);
	}

	if (words.size() > 1)
	{
		return true;
	}
	
	return false;
}