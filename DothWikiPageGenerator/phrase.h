#pragma once

#include "word.h"

using namespace std;

class Phrase : public Word
{
	public:
		Phrase(string w, string wt, string t);

		string GetHeader();
};
