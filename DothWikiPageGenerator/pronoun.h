#pragma once

#include "word.h"

using namespace std;

class Pronoun : public Word
{
	public:
		Pronoun(string w, string wt, string t);

		string GetHeader();
};