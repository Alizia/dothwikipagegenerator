﻿#include "wikiPage.h"

WikiPage::WikiPage(VocabularyData vd)
{
	word = vd.word;
	Syllabify();
	Stressify();
	SetPronunciation();

	for (int i = 0; i < vd.definitions.size(); i++)
	{
		string strWordType = vd.definitions[i].wordtype;
		WordType wt = GetWordType(strWordType);
		shared_ptr<Word> Word;

		switch (wt)
		{
			case WordType::NOUN:
				if (vd.sups.size() > 0)
				{
					Word = make_shared<Noun>(word, strWordType, vd.definitions[i].definition, vd.sups[0].value);
				}
				else
				{
					Word = make_shared<Noun>(word, strWordType, vd.definitions[i].definition);
				}
				break;
			case WordType::VERB:
				if (vd.sups.size() > 0)
				{
					Word = make_shared<Verb>(word, strWordType, vd.definitions[i].definition, vd.sups[0].value);
				}
				else
				{
					Word = make_shared<Verb>(word, strWordType, vd.definitions[i].definition);
				}
				break;
			case WordType::ADJECTIVE:
				Word = make_shared<Adj>(word, strWordType, vd.definitions[i].definition, AdjType::POSITIVE);
				break;
			case WordType::INTERJECTION:
				Word = make_shared<Intj>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::ADVERB:
				Word = make_shared<Adverb>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::CONJUNCTION:
				Word = make_shared<Conj>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::PHRASE:
				Word = make_shared<Phrase>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::NUMERAL:
				Word = make_shared<Num>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::AUXILIARY:
				Word = make_shared<Aux>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::DETERMINER:
				Word = make_shared<Det>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::PRONOUN:
				Word = make_shared<Pronoun>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::PREPOSITION:
				Word = make_shared<Prep>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::PARTICLE:
				Word = make_shared<Part>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::PREFIX:
				Word = make_shared<Prefix>(word, strWordType, vd.definitions[i].definition);
				break;
			case WordType::UNKNOWN:
				Word = make_shared<Default>(word, strWordType, vd.definitions[i].definition);
				break;
		}
		
		definitionsList.push_back(Word);				
	}

	for (int i = 0; i < vd.quotes.size(); i++)
	{
		quoteList.push_back(vd.quotes[i]);
	}
}

WikiPage::WikiPage(shared_ptr<Word> w)
{
	word = w->word;
	Syllabify();
	Stressify();
	SetPronunciation();

	definitionsList.push_back(w);
}

void WikiPage::GenerateWikiPage(bool etymology=false)
{
	string fileName = "generatedFile/" + word + ".txt";
	fileName.erase(remove(fileName.begin(), fileName.end(), '?'), fileName.end()); // Erase forbidden character in filename

	ifstream file(fileName, ios::in);
	string firstLine;
	getline(file, firstLine);
	file.close();

	ofstream wikipage(fileName, ios::app);
	if (wikipage)  // If the file is open
	{
		//cout << "Bite 1 word=" + word << endl;
		
		if (firstLine == "") // New file
		{
			wikipage << GetInitTemplate(etymology); // Add the commun start template (lexeme + invisible etymology [optional1] + pronunciation)
		}

		for (int i = 0; i < definitionsList.size(); i++)
		{
			shared_ptr<Word> WordRef = definitionsList[i];
			wikipage << WordRef->GetHeader();
			if (WordRef->wordType == WordType::VERB && !IsPhrase(WordRef->word))
			{
				shared_ptr<Verb> V = dynamic_pointer_cast<Verb>(WordRef);
				wikipage << V->GetTranslation();
			}
			else if(WordRef->wordType == WordType::ADJECTIVE)
			{
				shared_ptr<Adj> A = dynamic_pointer_cast<Adj>(WordRef);
				wikipage << A->GetTranslation();
			}
			else
			{
				wikipage << WordRef->GetTranslation();
			}
			

			for (int j = i+1; j < definitionsList.size(); j++) // get all other definition with the same type
			{
				shared_ptr<Word> OtherWord = definitionsList[j];

				if (OtherWord->wordType == WordRef->wordType)
				{
					if (OtherWord->wordType == WordType::VERB && !IsPhrase(OtherWord->word))
					{
						shared_ptr<Verb> V = dynamic_pointer_cast<Verb>(OtherWord);
						wikipage << V->GetTranslation();
					}
					else
					{
						wikipage << OtherWord->GetTranslation();
					}
					wikipage << OtherWord->GetTranslation();
					definitionsList.erase(definitionsList.begin()+j);
					j--; // IMPORTANT ! DON'T DELETE THIS STUPID ALIZIA OF THE FUTURE !
				}
			}

			for (int k = 0; k < quoteList.size(); k++) // quotes
			{
				wikipage << "\n";
				wikipage << "#:"+quoteList[k].dothraki+"\n";
				wikipage << "#::"+quoteList[k].english+"\n";
			}

			wikipage << "\n";
			if (WordRef->wordType == WordType::NOUN || WordRef->wordType == WordType::VERB)
			{
				wikipage << WordRef->GetInflection();
			}
			//wikipage << Word->GenerateTemplate();

			if (WordRef->wordType == WordType::ADJECTIVE && word.front() != 'o') // o- word exclusion, we don't know if it is negative form.
			{
				shared_ptr<Adj> A = dynamic_pointer_cast<Adj>(WordRef);
				if (A->adjType == AdjType::POSITIVE)
				{
					GenerateComparisonWikiPage(A);
				}
			}
			else if (WordRef->wordType == WordType::VERB && !IsPhrase(WordRef->word))
			{
				shared_ptr<Verb> V = dynamic_pointer_cast<Verb>(WordRef);
				if (V->conjugated == "")
				{
					GenerateConjugationWikiPage(V, GetInitTemplate(false));
				}
			}
		}

		wikipage.close();
	}
	else
	{
		cout << "Impossible to write into '"+fileName+"' location." << endl;
	}
}

string WikiPage::GetInitTemplate(bool etymology)
{
	if (etymology)
	{
		return "{{lexeme|Dothraki}}\n\n=== Etymology ===\n{{invis}}\n\n====Pronunciation====\n{{IPA|["+pronunciation+"]|lang=doth}}\n\n";
	}
	else
	{
		return "{{lexeme|Dothraki}}\n\n====Pronunciation====\n{{IPA|["+pronunciation+"]|lang=doth}}\n\n";
	}
}

void WikiPage::Syllabify()
{
	syllableList = {};
	
	shared_ptr<Syllable> currentSy;
	shared_ptr<Character> currentChar;

	istringstream ss(word);
	string w;
	vector<string> wordList;

	while (getline(ss, w, ' ')) // Get all word (in case of a phrase)
	{
		transform(w.begin(), w.end(), w.begin(), ::tolower);
		wordList.insert(wordList.begin(), w);

		if (word == "Hoyali Jeshi ma Vorsasi")
		{
			//cout << "Bite 1 w=" << w << endl;
		}
	}

	vector<shared_ptr<Syllable>> newWord;

	for (int h = 0; h < wordList.size(); h++)
	{
		currentSy = make_shared<Syllable>(); // New syllable
		string currentWord = wordList[h];
		newWord = {};
		syllableList.insert(syllableList.begin(), newWord);

		for (int i = currentWord.size()-1; i >= 0; i--) // Read the word reversed
		{
			string c = "";
			c += currentWord[i]; // Stringify char

			if (c == "?" || c == "'") // avoid punctuation
			{
				continue;
			}

			if (i > 0) // th, zh, ch, sh, kh
			{
				string dc = currentWord[i - 1] + c;
				if (IsDoubleChar(dc))
				{
					i--;
					c = dc;
				}
			}

			currentChar = make_shared<Character>(c);

			if (currentChar->isVowel) // vowel
			{
				if (currentSy->haveVowel)
				{
					syllableList[0].insert(syllableList[0].begin(), currentSy); // Current Syllable finished
					currentSy = make_shared<Syllable>(); // New syllable
				}

				currentSy->AddChar(currentChar);
				currentSy->haveVowel = true;
			}
			else // consonant
			{
				if (currentSy->charList.size() == 0)
				{
					currentSy->AddChar(currentChar);
				}
				else
				{// Same syllable
					if (((currentSy->charList.front()->priority > currentChar->priority
						&& ((currentChar->isFrictative 
							&& !(currentSy->charList.front()->isNasal 
								|| currentSy->charList.front()->isApproximant)) 
							|| !currentChar->isFrictative)) 
						|| (currentSy->charList.front()->strChar == "h"
							&& (currentChar->isNasal
								|| currentChar->isApproximant)))
						&& (!IsGeminate(currentChar->strChar+currentSy->charList.front()->strChar)))  // if(((priority and ((frictative and !nasal) or !frictative)) or h aside nasal/appro) and not geminate)
					{
						currentSy->AddChar(currentChar);
					}
					else // New syllable
					{
						//cout << "Bite 6 c1=" << currentSy->charList.front()->priority << " c2=" << currentChar->priority << " c1<=c2" << endl;
						syllableList[0].insert(syllableList[0].begin(), currentSy); // Current Syllable finished
						currentSy = make_shared<Syllable>(); // New syllable
						currentSy->AddChar(currentChar);
					}
				}
			}
		}
		syllableList[0].insert(syllableList[0].begin(), currentSy); // Add first syllable
	}
}

void WikiPage::Stressify()
{
	for (int i = 0; i < syllableList.size(); i++)
	{
		shared_ptr<Syllable> firstSyllable = syllableList[i].front();

		shared_ptr<Syllable> lastSyllable = syllableList[i].back();
		string lastForm(1,lastSyllable->form.back());

		if (lastForm == "C")
		{
			lastSyllable->stressed = true;
		}
		else
		{
			if (syllableList[i].size() > 2)
			{
				shared_ptr<Syllable> penultimateSyllable = syllableList[i][syllableList[i].size()-2];
				string penultimateLastForm(1,penultimateSyllable->form.back());

				if (penultimateLastForm == "C")
				{
					penultimateSyllable->stressed = true;
				}
				else
				{
					firstSyllable->stressed = true;
				}
			}
			else
			{
				firstSyllable->stressed = true;
			}
		}
	}
}

void WikiPage::SetPronunciation()
{
	string ipaAllo; // IPA allophony
	bool needJump = false;
	bool isSyLastChar = false;
	bool isNextSyStressed = false;
	bool syllableAlreadyStressed = false; // For geminate stress

	vector<shared_ptr<Syllable>> currentWord;
	shared_ptr<Syllable> currentSy;
	shared_ptr<Character> currentChar;

	shared_ptr<Character> beforeChar = nullptr;
	shared_ptr<Character> afterChar = nullptr;


	for (int i = 0; i < syllableList.size(); i++) // For each word
	{
		currentWord = syllableList[i];
		for (int j = 0; j < currentWord.size(); j++) // For each syllable
		{
			//ipaAllo += "-";
			isNextSyStressed = false;
			currentSy = syllableList[i][j];
			if (currentSy->stressed && !syllableAlreadyStressed) // Stress
			{
				ipaAllo += "ˈ";
			}

			for (int k = 0; k < currentSy->charList.size(); k++) // For each char
			{
				currentChar = currentSy->charList[k];
				isSyLastChar = (k == currentSy->charList.size() - 1);

				if(isSyLastChar)
				{
					if (j+1 < currentWord.size()) // There is a syllable after this one
					{
						afterChar = syllableList[i][j+1]->charList.front();
						isNextSyStressed = syllableList[i][j+1]->stressed;
					}
					else // End of the word
					{
						afterChar = nullptr;
					}
				}
				else
				{
					afterChar = currentSy->charList[k+1];
				}

				if (!needJump)
				{
					ipaAllo += currentChar->GetIPA(needJump, beforeChar, afterChar, isSyLastChar, isNextSyStressed);
					if (isNextSyStressed && needJump)
					{
						syllableAlreadyStressed = true;
					}
				}
				else
				{
					needJump = false;
				}
				

				beforeChar = currentChar;
			}
		}

		if (i+1 < syllableList.size()) // Space between word if phrase
		{
			ipaAllo += " ";
		}
	}

	pronunciation = ipaAllo;
}

void GenerateComparisonWikiPage(shared_ptr<Adj> A)
{
	shared_ptr<Adj> adj;
	shared_ptr<WikiPage> wp;

	for (int i = 1; i < 6; i++)
	{
		adj = make_shared<Adj>(A->word, "adj.", A->translation, static_cast<AdjType>(i));
		switch (adj->adjType)
		{
			case AdjType::COMPARATIVE:
				adj->compared = A->GetComparative();
				break;
			case AdjType::SUPERLATIVE:
				adj->compared = A->GetSuperlative();
				break;
			case AdjType::NEGATIVE:
				adj->compared = A->GetNegative();
				break;
			case AdjType::CONTRASTIVE:
				adj->compared = A->GetContrastive();
				break;
			case AdjType::SUBLATIVE:
				adj->compared = A->GetSublative();
				break;
		}

		wp = make_shared<WikiPage>(adj);

		wp->word = adj->compared; // For file name
		wp->Syllabify(); // Special case for adj, they need to keep the positive as word, just redo the pronunciation.
		wp->Stressify();
		wp->SetPronunciation();
		wp->GenerateWikiPage();
	}
}

string GetConjugatedDefinition(string infinitive, VerbPositive vp, VerbTense vt, VerbPronoun vpn)
{
	string pronounStr;
	string pastStr;
	switch (vpn)
	{
		case VerbPronoun::SG_FIRST:
			pronounStr = "# first person singular ";
			pastStr = "# singular ";
			break;
		case VerbPronoun::SG_SECOND:
			pronounStr = "# second person singular ";
			pastStr = "# singular ";
			break;
		case VerbPronoun::SG_THIRD:
			pronounStr = "# third person singular ";
			pastStr = "# singular ";
			break;
		case VerbPronoun::PL_FIRST:
			pronounStr = "# first person plural ";
			pastStr = "# plural ";
			break;
		case VerbPronoun::PL_SECOND:
			pronounStr = "# second person plural ";
			pastStr = "# plural ";
			break;
		case VerbPronoun::PL_THIRD:
			pronounStr = "# third person plural ";
			pastStr = "# plural ";
			break;
	}
	
	switch (vt)
	{
		case VerbTense::PRESENT:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return pronounStr+"present tense positive conjugation of ''[["+infinitive+"]]''\n";
				case VerbPositive::NEGATIVE:
					return pronounStr+"present tense negative conjugation of ''[["+infinitive+"]]''\n";
			}
		case VerbTense::PAST:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return pastStr+"past tense positive conjugation of ''[["+infinitive+"]]''\n";
				case VerbPositive::NEGATIVE:
					return pastStr+"past tense negative conjugation of ''[["+infinitive+"]]''\n";
			}
		case VerbTense::FUTURE:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return pronounStr+"future tense positive conjugation of ''[["+infinitive+"]]''\n";
				case VerbPositive::NEGATIVE:
					return pronounStr+"future tense negative conjugation of ''[["+infinitive+"]]''\n";
			}
		case VerbTense::FORMAL:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return "# formal positive imperative conjugation of ''[["+infinitive+"]]''\n";
				case VerbPositive::NEGATIVE:
					return "# formal negative imperative conjugation of ''[["+infinitive+"]]''\n";
			}
		case VerbTense::INFORMAL:
			switch (vp)
			{
				case VerbPositive::POSITIVE:
					return "# informal positive imperative conjugation of ''[["+infinitive+"]]''\n";
				case VerbPositive::NEGATIVE:
					return "# informal negative imperative conjugation of ''[["+infinitive+"]]''\n";
			}
		case VerbTense::PASTPART:
			return "# past participle conjugation of ''[["+infinitive+"]]''\n";
	}
	return "";
}

void AddConjugationToList(vector<Verb>& ConjugationObjectList, Verb& Conjugated, VerbPositive vp, VerbTense vt, VerbPronoun vpn)
{
	//cout << "Bite 1 AddConj, for Conjugated conjugated=" << Conjugated.conjugated << endl;
	for (int i = 0; i < ConjugationObjectList.size(); i++)
	{
		//cout << "Bite 2 AddConj, for ConjugationObjectList conjugated=" << ConjugationObjectList[i].conjugated << endl;
		//system("PAUSE");
		if (ConjugationObjectList[i].conjugated == Conjugated.conjugated)
		{
			ConjugationObjectList[i].translation += GetConjugatedDefinition(ConjugationObjectList[i].word, vp, vt, vpn);
			return;
		}
	}
	ConjugationObjectList.push_back(Conjugated);
	//cout << "Bite 2 AddConj ConjugationObjectList.size() = " << ConjugationObjectList.size() << endl;
	ConjugationObjectList.back().translation = GetConjugatedDefinition(ConjugationObjectList.back().word, vp, vt, vpn);
}

void WriteConjugationWikiPage(vector<Verb>& ConjugationObjectList, string initTemplate)
{
	//cout << "Bite WriteConj ConjugationObjectList.size() = " << ConjugationObjectList.size() << endl;
	shared_ptr<Verb> V;
	for (vector<Verb>::iterator it = ConjugationObjectList.begin(); it != ConjugationObjectList.end(); ++it)
	{
		V = make_shared<Verb>(*it);

		shared_ptr<WikiPage> wp = make_shared<WikiPage>(V);
		wp->word = V->conjugated; // For file name
		wp->Syllabify(); // Special case for verb, they need to keep the infinitive as word, just redo the pronunciation.
		wp->Stressify();
		wp->SetPronunciation();
		wp->GenerateWikiPage();
		continue;

		//cout << "Bite WriteConj V->conjugated = " + V->conjugated << endl;
		
		string fileName = "generatedFile/"+V->conjugated+".txt";

		ifstream file(fileName, ios::in);
		string firstLine;
		getline(file, firstLine);
		file.close();

		ofstream wikipage(fileName, ios::app);

		if (wikipage)  // If the file is open
		{		
			if (firstLine == "") // New file
			{
				wikipage << initTemplate; // Add the commun start template (lexeme + invisible etymology + pronunciation)
			}
			wikipage << V->GetHeader();
			wikipage << V->GetTranslation();
			wikipage << V->GetInflection();
			wikipage.close();
		}
		else
		{
			cout << "Impossible to write into '"+fileName+"' location." << endl;
		}
	}
}

void GenerateConjugationWikiPage(shared_ptr<Verb> V, string initTemplate)
{
	//cout << "Bite GenerateConjugationWikiPage V->word = " << V->word << endl;
	vector<Verb> ConjugationObjectList;

	Verb Conjugated = *V;

	Conjugated.conjugated = V->GetPastPart();
	
	AddConjugationToList(ConjugationObjectList, Conjugated, VerbPositive::POSITIVE, VerbTense::PASTPART, VerbPronoun::SG_FIRST);
	
	for (int i = 0; i < 2; i++) // Iterate VerbPositive
	{
		Conjugated = *V;
		Conjugated.conjugated = V->GetFormal(static_cast<VerbPositive>(i));
		
		AddConjugationToList(ConjugationObjectList, Conjugated, static_cast<VerbPositive>(i), VerbTense::FORMAL, VerbPronoun::SG_FIRST);

		Conjugated = *V;
		Conjugated.conjugated = V->GetInformal(static_cast<VerbPositive>(i));
		AddConjugationToList(ConjugationObjectList, Conjugated, static_cast<VerbPositive>(i), VerbTense::INFORMAL, VerbPronoun::SG_FIRST);

		Conjugated = *V;
		Conjugated.conjugated = V->GetPast(static_cast<VerbPositive>(i), VerbPronoun::SG_FIRST);
		AddConjugationToList(ConjugationObjectList, Conjugated, static_cast<VerbPositive>(i), VerbTense::PAST, VerbPronoun::SG_FIRST);

		Conjugated = *V;
		Conjugated.conjugated = V->GetPast(static_cast<VerbPositive>(i), VerbPronoun::PL_FIRST);
		AddConjugationToList(ConjugationObjectList, Conjugated, static_cast<VerbPositive>(i), VerbTense::PAST, VerbPronoun::PL_FIRST);
		
		for (int j = 0; j < 6; j++) // Iterate VerbPronoun
		{
			Conjugated = *V;
			Conjugated.conjugated = V->GetPresent(static_cast<VerbPositive>(i), static_cast<VerbPronoun>(j));
			AddConjugationToList(ConjugationObjectList, Conjugated, static_cast<VerbPositive>(i), VerbTense::PRESENT, static_cast<VerbPronoun>(j));

			Conjugated = *V;
			Conjugated.conjugated = V->GetFuture(static_cast<VerbPositive>(i), static_cast<VerbPronoun>(j));
			AddConjugationToList(ConjugationObjectList, Conjugated, static_cast<VerbPositive>(i), VerbTense::FUTURE, static_cast<VerbPronoun>(j));
		}
	}

	WriteConjugationWikiPage(ConjugationObjectList, initTemplate);
}