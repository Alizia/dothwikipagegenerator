#pragma once

#include "word.h"

using namespace std;

enum class NounType
{
	ANIMATE,
	INANIMATE_A,
	INANIMATE_B,
	BOTH,
	UNKNOWN,
};

class Noun : public Word
{
	public:
		Noun(string w, string wt, string t, string acc="");

		NounType nounType;
		string root;
		string thema;

		string GetHeader();
		string GetInflection();
};

NounType GetNounType(string t);