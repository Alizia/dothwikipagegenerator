{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈahaoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[hajolat]]''
# third person singular future tense positive conjugation of ''[[hajolat]]''
# second person plural future tense positive conjugation of ''[[hajolat]]''
# third person plural future tense positive conjugation of ''[[hajolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=haj|thema=o}}

