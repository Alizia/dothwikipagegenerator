{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪avaˈxas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

