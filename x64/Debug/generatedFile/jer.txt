{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈer]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[jerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jer|epe=}}

