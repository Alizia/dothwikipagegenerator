{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[voˈʃak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[oshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=osh|epe=}}

