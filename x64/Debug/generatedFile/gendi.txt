{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈgen̪d̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[gendat]]''
# second person singular present tense positive conjugation of ''[[gendat]]''
# second person plural present tense positive conjugation of ''[[gendat]]''
# third person plural present tense positive conjugation of ''[[gendat]]''
# second person singular present tense negative conjugation of ''[[gendat]]''
# second person plural present tense negative conjugation of ''[[gendat]]''
# third person plural present tense negative conjugation of ''[[gendat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gend|epe=}}

