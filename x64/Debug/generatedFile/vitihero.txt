{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvit̪iheɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[vitiherat]]''
# singular past tense negative conjugation of ''[[vitiherat]]''
# third person singular present tense negative conjugation of ''[[vitiherat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitiher|epe=}}

