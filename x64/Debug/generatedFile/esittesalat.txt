{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[esit̪t̪esaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to brag, to exaggerate

=====Inflection=====
{{Template:dothra-vVV|root=esittes|thema=a}}

