{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈot͡ʃaka]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[chakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chak|epe=}}

