{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪d̪ɤθˈɾaʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[addothralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=addothr|thema=a}}

