{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvar.rageɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[varragerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=varrager|epe=}}

