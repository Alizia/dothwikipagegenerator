{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[il̪meˈser]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# salad, greens, vegetables

=====Inflection=====
{{Template:dothra-na|root=ilmeser|thema=}}

