{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoɾemeki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[remekat]]''
# second person plural future tense negative conjugation of ''[[remekat]]''
# third person plural future tense negative conjugation of ''[[remekat]]''


=====Inflection=====
{{Template:dothra-vCC|root=remek|epe=}}

