{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈgen̪d̪ɤe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[gendolat]]''
# third person singular present tense positive conjugation of ''[[gendolat]]''
# second person plural present tense positive conjugation of ''[[gendolat]]''
# third person plural present tense positive conjugation of ''[[gendolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=gend|thema=o}}

