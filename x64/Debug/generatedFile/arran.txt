{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ar.ˈran̪]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[arranat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arran|epe=}}

