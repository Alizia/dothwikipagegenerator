{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[assamˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[assamvat]]''


=====Inflection=====
{{Template:dothra-vVC|root=assamv|epe=}}

