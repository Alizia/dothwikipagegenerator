{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvosti]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[ostat]]''
# second person plural future tense positive conjugation of ''[[ostat]]''
# third person plural future tense positive conjugation of ''[[ostat]]''
# second person singular future tense negative conjugation of ''[[ostat]]''
# second person plural future tense negative conjugation of ''[[ostat]]''
# third person plural future tense negative conjugation of ''[[ostat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ost|epe=}}

