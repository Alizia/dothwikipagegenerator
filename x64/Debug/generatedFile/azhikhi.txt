{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʒixi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[zhikhat]]''
# second person plural future tense positive conjugation of ''[[zhikhat]]''
# third person plural future tense positive conjugation of ''[[zhikhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zhikh|epe=}}

