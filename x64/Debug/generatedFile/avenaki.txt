{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaven̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[venat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ven|epe=}}

