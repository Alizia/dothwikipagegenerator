{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪aqɔki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[laqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=laq|epe=e}}

