{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvammat̪t̪eo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[ammattelat]]''
# third person singular future tense negative conjugation of ''[[ammattelat]]''
# second person plural future tense negative conjugation of ''[[ammattelat]]''
# third person plural future tense negative conjugation of ''[[ammattelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ammatt|thema=e}}

