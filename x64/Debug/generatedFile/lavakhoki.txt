{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪avaxuki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

