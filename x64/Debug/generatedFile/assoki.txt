{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈassoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[assilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ass|thema=i}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[assolat]]''
# first person plural present tense negative conjugation of ''[[assolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ass|thema=o}}

