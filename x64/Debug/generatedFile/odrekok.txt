{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[od̪ɾeˈkuk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[drekolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=drek|thema=o}}

