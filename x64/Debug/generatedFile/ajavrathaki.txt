{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaavɾaθaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[javrathat]]''


=====Inflection=====
{{Template:dothra-vCC|root=javrath|epe=}}

