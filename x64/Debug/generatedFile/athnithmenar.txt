{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθn̪iθmeˈn̪ar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# painlessness

=====Inflection=====
{{Template:dothra-ni|root=athnithmenar|thema=}}

