{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈzisoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[zisat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zis|epe=}}

