{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪iˈθas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[nithat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nith|epe=}}

