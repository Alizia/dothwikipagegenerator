{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vakkuaˈl̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[akkoalat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkoal|epe=}}

