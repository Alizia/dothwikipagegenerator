{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[hoeˈɾiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[hoerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hoer|epe=}}

