{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈajoma]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[yomat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yom|epe=}}

