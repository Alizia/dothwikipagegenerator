{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[akkel̪eˈn̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[akkelenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkelen|epe=}}

