{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvan̪n̪evae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[annevalat]]''
# third person singular future tense positive conjugation of ''[[annevalat]]''
# second person plural future tense positive conjugation of ''[[annevalat]]''
# third person plural future tense positive conjugation of ''[[annevalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=annev|thema=a}}

