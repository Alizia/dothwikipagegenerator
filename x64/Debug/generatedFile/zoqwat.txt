{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[zoˈqwat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to kiss

=====Inflection=====
{{Template:dothra-vCC|root=zoqw|epe=e}}

