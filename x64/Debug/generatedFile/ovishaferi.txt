{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoviʃafeɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[vishaferat]]''
# second person plural future tense negative conjugation of ''[[vishaferat]]''
# third person plural future tense negative conjugation of ''[[vishaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vishafer|epe=}}

