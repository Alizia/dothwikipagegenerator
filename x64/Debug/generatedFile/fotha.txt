{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈfoθa]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# throat
# sapwood, heartwood
# sapwood, heartwood

=====Inflection=====
{{Template:dothra-ni|root=foth|thema=a}}

