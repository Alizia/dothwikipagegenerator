{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaveɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[verat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ver|epe=}}

