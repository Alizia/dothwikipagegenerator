{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ʒean̪aˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to be beautiful (used for animate nouns)

=====Inflection=====
{{Template:dothra-vCV|root=zhean|thema=a}}

