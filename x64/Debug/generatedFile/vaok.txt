{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vaˈok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[aolat]]''
# first person singular future tense negative conjugation of ''[[aolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=a|thema=o}}

