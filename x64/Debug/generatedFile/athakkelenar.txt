{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθakkel̪eˈn̪ar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# decision, judgment

=====Inflection=====
{{Template:dothra-ni|root=athakkelenar|thema=}}

