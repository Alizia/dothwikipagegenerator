{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[sixˈt̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[sikhtelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=sikht|thema=e}}

