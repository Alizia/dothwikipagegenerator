{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈakuvaɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[kovarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kovar|epe=}}

