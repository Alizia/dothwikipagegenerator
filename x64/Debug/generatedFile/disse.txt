{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈd̪isse]|lang=doth}}

===Adverb===
{{head|doth|adverb}}

# only, simply, exclusively, just

#:Kash qoy qoyi thira disse.
#::Only while blood of my blood lives.

#:Khal vos zigereo adoroon anevasoe maan. Me zigeree sajosoon disse.
#::A khal doesn't need a chair to sit on. He only needs a steed.

