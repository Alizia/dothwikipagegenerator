{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qɑˈfok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[qafat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qaf|epe=}}

