{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvad.d͡ʒoɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[ajjorat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ajjor|epe=}}

