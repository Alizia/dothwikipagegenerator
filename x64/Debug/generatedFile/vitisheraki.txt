{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvit̪iʃeɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[vitisherat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitisher|epe=}}

