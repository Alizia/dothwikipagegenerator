{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaɾhan̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[rhanat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rhan|epe=}}

