{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈvoɾt̪ɤ]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# tooth

=====Inflection=====
{{Template:dothra-ni|root=vort|thema=o|stem=vorto}}

