{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈgid̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[gidat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gid|epe=}}

