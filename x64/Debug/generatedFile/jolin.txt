{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈl̪in̪]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[jolinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jolin|epe=}}

