{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈfon̪n̪ɤja]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# advice

=====Inflection=====
{{Template:dothra-ni|root=fonnoy|thema=a}}

