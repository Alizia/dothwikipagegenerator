{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈveeɾvao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[ejervalat]]''
# third person singular future tense negative conjugation of ''[[ejervalat]]''
# second person plural future tense negative conjugation of ''[[ejervalat]]''
# third person plural future tense negative conjugation of ''[[ejervalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ejerv|thema=a}}

