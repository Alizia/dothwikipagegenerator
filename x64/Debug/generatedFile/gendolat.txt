{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[gen̪d̪ɤˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to become ripped, to rip

=====Inflection=====
{{Template:dothra-vCV|root=gend|thema=o}}

