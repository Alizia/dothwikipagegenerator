{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈqɔɾasoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[qorasolat]]''
# third person singular present tense positive conjugation of ''[[qorasolat]]''
# second person plural present tense positive conjugation of ''[[qorasolat]]''
# third person plural present tense positive conjugation of ''[[qorasolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=qoras|thema=o}}

