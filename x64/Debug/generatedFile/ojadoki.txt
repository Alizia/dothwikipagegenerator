{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoad̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[jadolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=jad|thema=o}}

