{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪aˈvak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

