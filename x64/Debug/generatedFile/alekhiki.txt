{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪exiki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[lekhilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=lekh|thema=i}}

