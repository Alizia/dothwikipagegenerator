{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat͡ʃil̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[chilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chil|epe=}}

