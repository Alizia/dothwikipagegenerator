{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[keˈl̪en̪]|lang=doth}}

====Adjective====
{{doth-adj}}

# patterned, orderly
