{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[kaˈd̪ix]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# untamed captured animal

=====Inflection=====
{{Template:dothra-ni|root=kadikh|thema=}}

