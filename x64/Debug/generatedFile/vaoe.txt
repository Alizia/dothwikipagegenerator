{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[aolat]]''
# third person singular future tense positive conjugation of ''[[aolat]]''
# second person plural future tense positive conjugation of ''[[aolat]]''
# third person plural future tense positive conjugation of ''[[aolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=a|thema=o}}

