{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈʃio]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[shillat]]''
# singular past tense negative conjugation of ''[[shillat]]''


=====Inflection=====
{{Template:dothra-vCV|root=shi|thema=l}}

