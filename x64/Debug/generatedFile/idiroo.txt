{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈid̪iɾoo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[idirolat]]''
# third person singular present tense negative conjugation of ''[[idirolat]]''
# second person plural present tense negative conjugation of ''[[idirolat]]''
# third person plural present tense negative conjugation of ''[[idirolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=idir|thema=o}}

