{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪ɤmˈmaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[lommat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lomm|epe=}}

