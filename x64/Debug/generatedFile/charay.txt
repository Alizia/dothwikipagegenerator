{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃaˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[charat]]''


=====Inflection=====
{{Template:dothra-vCC|root=char|epe=}}

