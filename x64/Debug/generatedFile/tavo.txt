{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪avo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[tavat]]''
# singular past tense negative conjugation of ''[[tavat]]''
# third person singular present tense negative conjugation of ''[[tavat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tav|epe=}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# axe

=====Inflection=====
{{Template:dothra-ni|root=tav|thema=o}}

