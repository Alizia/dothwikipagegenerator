{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪aaˈhak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[najahat]]''


=====Inflection=====
{{Template:dothra-vCC|root=najah|epe=}}

====Noun====
{{head|doth|noun|nominative, animate}}

# winner

=====Inflection=====
{{Template:dothra-na|root=najahak|thema=}}

