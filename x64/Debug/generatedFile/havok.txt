{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[havolat]]''
# first person singular present tense negative conjugation of ''[[havolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hav|thema=o}}

