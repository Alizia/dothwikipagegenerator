{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[l̪aˈn̪at̪ ˈki ˈveʒi]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

# to dare

=====Inflection=====
{{Template:dothra-vCC|root=lan|epe=}}

