{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qweˈhok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[qwehat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qweh|epe=}}

