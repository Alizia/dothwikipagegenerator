{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[afaʒoˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to become hot, to blush

=====Inflection=====
{{Template:dothra-vVV|root=afazh|thema=o}}

