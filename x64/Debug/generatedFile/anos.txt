{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[anat]]''


=====Inflection=====
{{Template:dothra-vVC|root=an|epe=}}

