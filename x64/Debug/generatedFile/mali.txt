{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmal̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[malilat]]''
# singular past tense positive conjugation of ''[[malilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mal|thema=i}}

