{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪ihio]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[tihilat]]''
# third person singular present tense negative conjugation of ''[[tihilat]]''
# second person plural present tense negative conjugation of ''[[tihilat]]''
# third person plural present tense negative conjugation of ''[[tihilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=tih|thema=i}}

