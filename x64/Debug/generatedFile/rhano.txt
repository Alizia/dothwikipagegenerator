{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrhan̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[rhanat]]''
# singular past tense negative conjugation of ''[[rhanat]]''
# third person singular present tense negative conjugation of ''[[rhanat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rhan|epe=}}

