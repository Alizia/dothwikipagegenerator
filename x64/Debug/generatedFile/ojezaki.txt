{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoezaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[jezat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jez|epe=}}

