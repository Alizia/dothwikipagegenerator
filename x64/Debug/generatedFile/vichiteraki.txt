{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvit͡ʃit̪eɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[vichiterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vichiter|epe=}}

