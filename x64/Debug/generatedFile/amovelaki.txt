{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈamovel̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[movelat]]''


=====Inflection=====
{{Template:dothra-vCC|root=movel|epe=}}

