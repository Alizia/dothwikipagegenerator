{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈot͡ʃil̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[chilat]]''
# second person plural future tense negative conjugation of ''[[chilat]]''
# third person plural future tense negative conjugation of ''[[chilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chil|epe=}}

