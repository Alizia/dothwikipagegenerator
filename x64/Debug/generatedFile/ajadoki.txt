{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaad̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[jadolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=jad|thema=o}}

