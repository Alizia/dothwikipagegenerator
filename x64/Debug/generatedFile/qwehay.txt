{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qweˈhaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[qwehat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qweh|epe=}}

