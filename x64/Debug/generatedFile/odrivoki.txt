{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈod̪ɾivoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[drivolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=driv|thema=o}}

