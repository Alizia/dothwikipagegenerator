{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eeɾˈvoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[ejervalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ejerv|thema=a}}

