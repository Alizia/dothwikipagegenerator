{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈen̪t̪a]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# baby, infant

=====Inflection=====
{{Template:dothra-ni|root=ent|thema=a}}

