{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪ɤkit̪ˈt̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[nokittat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nokitt|epe=}}

