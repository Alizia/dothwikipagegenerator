{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈsoqɔ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[soqat]]''
# singular past tense negative conjugation of ''[[soqat]]''
# third person singular present tense negative conjugation of ''[[soqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=soq|epe=e}}

