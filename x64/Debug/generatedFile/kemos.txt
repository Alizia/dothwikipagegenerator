{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[keˈmos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[kemat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kem|epe=}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[kemolat]]''
# informal negative imperative conjugation of ''[[kemolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=kem|thema=o}}

