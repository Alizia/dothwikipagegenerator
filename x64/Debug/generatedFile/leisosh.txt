{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪eiˈsoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[leisolat]]''
# plural past tense negative conjugation of ''[[leisolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=leis|thema=o}}

