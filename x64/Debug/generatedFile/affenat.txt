{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[affeˈn̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to make something attach

=====Inflection=====
{{Template:dothra-vVC|root=affen|epe=}}

