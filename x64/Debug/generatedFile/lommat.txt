{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[l̪ɤmˈmat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to bathe

=====Inflection=====
{{Template:dothra-vCC|root=lomm|epe=}}

