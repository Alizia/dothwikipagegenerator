{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[amaˈl̪ik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[malilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mal|thema=i}}

