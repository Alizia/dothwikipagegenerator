{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪axaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[adakhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=adakh|epe=}}

