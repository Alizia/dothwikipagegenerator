{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈzifit͡ʃe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[zifichelat]]''
# singular past tense positive conjugation of ''[[zifichelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zifich|thema=e}}

