{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪d̪ɾivaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[addrivat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addriv|epe=}}

