{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[okˈkus]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[okkat]]''


=====Inflection=====
{{Template:dothra-vVC|root=okk|epe=}}

