{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[esit̪t̪eˈsaʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[esittesalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esittes|thema=a}}

