{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[viqɑfeˈɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[viqaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=viqafer|epe=}}

