{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ad.ˈd͡ʒin̪]|lang=doth}}

===Adverb===
{{head|doth|adverb}}

# now

#:Jin chiorisi zafra ajjin, majin kisha ataki morea ven me vallayafa kisha.
#::These women are slaves now to do with as we please.

