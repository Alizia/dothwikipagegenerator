{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat̪t̪ihoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[attihat]]''


=====Inflection=====
{{Template:dothra-vVC|root=attih|epe=}}

