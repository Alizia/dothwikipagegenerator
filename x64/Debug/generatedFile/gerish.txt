{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[geˈɾiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[gerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ger|epe=}}

