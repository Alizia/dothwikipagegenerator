{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vaɾan̪ˈn̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[arannat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arann|epe=}}

