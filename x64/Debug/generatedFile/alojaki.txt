{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪ɤaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[lojat]]''


=====Inflection=====
{{Template:dothra-vCC|root=loj|epe=}}

