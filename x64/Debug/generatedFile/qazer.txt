{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[qɑˈzer]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# apple

=====Inflection=====
{{Template:dothra-ni|root=qazer|thema=}}

