{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[od̪ɤθˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[dothralat]]''


=====Inflection=====
{{Template:dothra-vCV|root=dothr|thema=a}}

