{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪in̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[dinat]]''
# singular past tense negative conjugation of ''[[dinat]]''
# third person singular present tense negative conjugation of ''[[dinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=din|epe=}}

