{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrixuki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[rikholat]]''
# first person plural present tense negative conjugation of ''[[rikholat]]''


=====Inflection=====
{{Template:dothra-vCV|root=rikh|thema=o}}

