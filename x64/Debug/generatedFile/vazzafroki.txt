{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvazzafɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[azzafrolat]]''
# first person plural future tense negative conjugation of ''[[azzafrolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=azzafr|thema=o}}

