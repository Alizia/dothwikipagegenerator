{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaziɾejeseki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

