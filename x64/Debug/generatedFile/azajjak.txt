{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[azad.ˈd͡ʒak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[zajjat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zajj|epe=}}

