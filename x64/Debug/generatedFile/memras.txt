{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[meˈmɾas]|lang=doth}}

===Pronoun===
{{head|doth|pronoun}}

# (pronominal version of a preposition, see mra)

