{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[asˈsis]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[assilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ass|thema=i}}

