{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈʒas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[azhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azh|epe=}}

