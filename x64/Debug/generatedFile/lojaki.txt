{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪ɤaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[lojat]]''


=====Inflection=====
{{Template:dothra-vCC|root=loj|epe=}}

