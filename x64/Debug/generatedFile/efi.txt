{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈefi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[efat]]''
# second person singular present tense positive conjugation of ''[[efat]]''
# second person plural present tense positive conjugation of ''[[efat]]''
# third person plural present tense positive conjugation of ''[[efat]]''
# second person singular present tense negative conjugation of ''[[efat]]''
# second person plural present tense negative conjugation of ''[[efat]]''
# third person plural present tense negative conjugation of ''[[efat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ef|epe=}}

