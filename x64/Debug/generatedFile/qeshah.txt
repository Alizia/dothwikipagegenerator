{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[qɛˈʃaħ]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# sand

=====Inflection=====
{{Template:dothra-ni|root=qeshah|thema=}}

