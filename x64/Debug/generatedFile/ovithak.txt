{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oviˈθak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[vithalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vith|thema=a}}

