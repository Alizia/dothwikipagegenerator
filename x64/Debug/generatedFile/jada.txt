{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[jadat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jad|epe=}}

