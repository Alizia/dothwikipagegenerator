{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vaħ.haˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[ahhasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhas|epe=}}

