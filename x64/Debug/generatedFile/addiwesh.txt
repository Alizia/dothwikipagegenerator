{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪d̪iˈweʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[addiwelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=addiw|thema=e}}

