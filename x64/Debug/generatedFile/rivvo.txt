{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrivvo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[rivvat]]''
# singular past tense negative conjugation of ''[[rivvat]]''
# third person singular present tense negative conjugation of ''[[rivvat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rivv|epe=}}

