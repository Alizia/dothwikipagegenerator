{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[iˈfos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[ifat]]''


=====Inflection=====
{{Template:dothra-vVC|root=if|epe=}}

