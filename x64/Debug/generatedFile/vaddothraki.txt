{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvad̪d̪ɤθɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[addothralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=addothr|thema=a}}

