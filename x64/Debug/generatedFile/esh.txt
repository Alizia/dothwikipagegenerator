{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈeʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[elat]]''


=====Inflection=====
{{Template:dothra-vVV|root=|thema=e}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[eshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=esh|epe=}}

