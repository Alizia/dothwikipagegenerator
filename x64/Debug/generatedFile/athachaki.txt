{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaθat͡ʃaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[thachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=thach|epe=}}

