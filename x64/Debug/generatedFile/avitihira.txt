{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈavit̪ihiɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[vitihirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitihir|epe=}}

