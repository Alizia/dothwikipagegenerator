{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvasteɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[vasterat]]''
# singular past tense negative conjugation of ''[[vasterat]]''
# third person singular present tense negative conjugation of ''[[vasterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vaster|epe=}}

