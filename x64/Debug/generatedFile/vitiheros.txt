{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vit̪iheˈɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[vitiherat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitiher|epe=}}

