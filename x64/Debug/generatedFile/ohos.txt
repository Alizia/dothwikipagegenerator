{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈhos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[oholat]]''
# informal negative imperative conjugation of ''[[oholat]]''


=====Inflection=====
{{Template:dothra-vVV|root=oh|thema=o}}

