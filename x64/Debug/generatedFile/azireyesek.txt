{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aziɾejeˈsek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

