{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[fɾeˈd̪ɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[fredrilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=fredr|thema=i}}

