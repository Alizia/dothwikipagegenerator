{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[t̪ɤˈkik]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# fool, idiot

=====Inflection=====
{{Template:dothra-na|root=tokik|thema=}}

