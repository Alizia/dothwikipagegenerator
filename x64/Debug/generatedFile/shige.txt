{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈʃige]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[shigat]]''


=====Inflection=====
{{Template:dothra-vCC|root=shig|epe=e}}

