{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈagan̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[ganat]]''
# second person plural future tense positive conjugation of ''[[ganat]]''
# third person plural future tense positive conjugation of ''[[ganat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gan|epe=}}

