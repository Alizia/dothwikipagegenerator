{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪ihoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[tiholat]]''
# third person singular present tense positive conjugation of ''[[tiholat]]''
# second person plural present tense positive conjugation of ''[[tiholat]]''
# third person plural present tense positive conjugation of ''[[tiholat]]''


=====Inflection=====
{{Template:dothra-vCV|root=tih|thema=o}}

