{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈogaɾvoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[garvolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=garv|thema=o}}

