{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈelzaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[elzat]]''


=====Inflection=====
{{Template:dothra-vVC|root=elz|epe=}}

