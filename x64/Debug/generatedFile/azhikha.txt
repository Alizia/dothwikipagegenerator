{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʒixa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[zhikhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zhikh|epe=}}

