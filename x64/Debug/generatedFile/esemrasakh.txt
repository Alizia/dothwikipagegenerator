{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[esemɾaˈsax]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# exit

=====Inflection=====
{{Template:dothra-ni|root=esemrasakh|thema=}}

