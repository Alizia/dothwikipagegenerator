{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈasʃil̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[asshilat]]''
# second person singular present tense positive conjugation of ''[[asshilat]]''
# second person plural present tense positive conjugation of ''[[asshilat]]''
# third person plural present tense positive conjugation of ''[[asshilat]]''
# second person singular present tense negative conjugation of ''[[asshilat]]''
# second person plural present tense negative conjugation of ''[[asshilat]]''
# third person plural present tense negative conjugation of ''[[asshilat]]''


=====Inflection=====
{{Template:dothra-vVC|root=asshil|epe=}}

