{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈol̪ivan̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[livanolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=livan|thema=o}}

