{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈsas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[hasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=has|epe=}}

