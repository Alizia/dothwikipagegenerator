{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[avvit̪iʃeˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[avvitisherat]]''


=====Inflection=====
{{Template:dothra-vVC|root=avvitisher|epe=}}

