{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈagat͡ʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[gachat]]''
# second person plural future tense positive conjugation of ''[[gachat]]''
# third person plural future tense positive conjugation of ''[[gachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gach|epe=}}

