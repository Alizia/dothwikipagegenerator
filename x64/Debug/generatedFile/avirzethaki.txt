{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaviɾzeθaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[virzethat]]''


=====Inflection=====
{{Template:dothra-vCC|root=virzeth|epe=}}

