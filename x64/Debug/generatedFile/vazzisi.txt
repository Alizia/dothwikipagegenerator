{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvazzisi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[azzisat]]''
# second person plural future tense positive conjugation of ''[[azzisat]]''
# third person plural future tense positive conjugation of ''[[azzisat]]''
# second person singular future tense negative conjugation of ''[[azzisat]]''
# second person plural future tense negative conjugation of ''[[azzisat]]''
# third person plural future tense negative conjugation of ''[[azzisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azzis|epe=}}

