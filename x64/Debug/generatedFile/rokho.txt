{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈroxu]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[rokhat]]''
# singular past tense negative conjugation of ''[[rokhat]]''
# third person singular present tense negative conjugation of ''[[rokhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rokh|epe=}}

