{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmat̪t̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[mattelat]]''
# singular past tense negative conjugation of ''[[mattelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=matt|thema=e}}

