{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvazzohi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[azzohat]]''
# second person plural future tense positive conjugation of ''[[azzohat]]''
# third person plural future tense positive conjugation of ''[[azzohat]]''
# second person singular future tense negative conjugation of ''[[azzohat]]''
# second person plural future tense negative conjugation of ''[[azzohat]]''
# third person plural future tense negative conjugation of ''[[azzohat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azzoh|epe=}}

