{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈofen̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[fenat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fen|epe=}}

