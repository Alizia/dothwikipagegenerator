{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈexugaɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ekhogaralat]]''
# singular past tense positive conjugation of ''[[ekhogaralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ekhogar|thema=a}}

