{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪ɾivoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[drivat]]''


=====Inflection=====
{{Template:dothra-vCC|root=driv|epe=}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[drivolat]]''
# first person plural present tense negative conjugation of ''[[drivolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=driv|thema=o}}

