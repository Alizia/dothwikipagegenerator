{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[veɾiˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[erinat]]''


=====Inflection=====
{{Template:dothra-vVC|root=erin|epe=}}

