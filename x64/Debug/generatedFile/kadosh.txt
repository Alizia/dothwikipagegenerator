{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kaˈd̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[kadat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kad|epe=}}

