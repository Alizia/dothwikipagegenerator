{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aviɾzeˈθak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[virzethat]]''


=====Inflection=====
{{Template:dothra-vCC|root=virzeth|epe=}}

