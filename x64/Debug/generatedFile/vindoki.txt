{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvin̪d̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[indelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ind|thema=e}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[vindelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vind|thema=e}}

