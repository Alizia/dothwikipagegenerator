{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoɾhel̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[rhelalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=rhel|thema=a}}

