{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[heʒaˈhaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[hezhahat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hezhah|epe=}}

