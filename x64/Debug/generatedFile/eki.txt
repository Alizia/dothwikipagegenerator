{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈeki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[elat]]''


=====Inflection=====
{{Template:dothra-vVV|root=|thema=e}}

