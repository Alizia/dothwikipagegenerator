{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪ɤˈgak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[dogat]]''


=====Inflection=====
{{Template:dothra-vCC|root=dog|epe=e}}

