{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈziɾoqɔsoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[ziroqoselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziroqos|thema=e}}

