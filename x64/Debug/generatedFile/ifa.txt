{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈifa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[ifat]]''


=====Inflection=====
{{Template:dothra-vVC|root=if|epe=}}

