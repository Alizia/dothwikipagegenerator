{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eqɔɾaˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[eqorasalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=eqoras|thema=a}}

