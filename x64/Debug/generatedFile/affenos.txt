{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[affeˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[affenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affen|epe=}}

