{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[foˈn̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[fonat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fon|epe=}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[fonolat]]''
# first person singular present tense negative conjugation of ''[[fonolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=fon|thema=o}}

