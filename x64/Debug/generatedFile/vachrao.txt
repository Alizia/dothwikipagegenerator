{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvat͡ʃɾao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[achralat]]''
# third person singular future tense negative conjugation of ''[[achralat]]''
# second person plural future tense negative conjugation of ''[[achralat]]''
# third person plural future tense negative conjugation of ''[[achralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=achr|thema=a}}

