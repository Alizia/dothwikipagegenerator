{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zoˈɾiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[zorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zor|epe=}}

