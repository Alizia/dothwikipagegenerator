{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈn̪er.ro]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# foal

=====Inflection=====
{{Template:dothra-ni|root=nerr|thema=o|stem=ner}}

