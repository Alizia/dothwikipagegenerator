{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaθiɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[thirat]]''
# second person plural future tense positive conjugation of ''[[thirat]]''
# third person plural future tense positive conjugation of ''[[thirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=thir|epe=}}

