{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvid̪iɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[idirolat]]''
# first person plural future tense negative conjugation of ''[[idirolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=idir|thema=o}}

