{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[foˈn̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[fonat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fon|epe=}}

