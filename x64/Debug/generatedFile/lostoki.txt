{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪ɤstɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[lostat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lost|epe=}}

