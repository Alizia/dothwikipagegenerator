{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[movekˈxat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to be intented for, to be for

=====Inflection=====
{{Template:dothra-vCC|root=movekkh|epe=}}

