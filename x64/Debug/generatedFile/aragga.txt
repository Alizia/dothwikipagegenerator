{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈɾagga]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[raggat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ragg|epe=e}}

