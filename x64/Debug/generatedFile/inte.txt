{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈin̪t̪e]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# beetle

=====Inflection=====
{{Template:dothra-ni|root=int|thema=e}}

