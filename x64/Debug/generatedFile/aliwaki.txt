{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪iwaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[liwalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=liw|thema=a}}

