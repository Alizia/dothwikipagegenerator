{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvefit͡ʃisaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[efichisalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efichis|thema=a}}

