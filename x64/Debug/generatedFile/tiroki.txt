{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪iɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[tirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tir|epe=}}

