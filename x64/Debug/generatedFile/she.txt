{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈʃe]|lang=doth}}

===Preposition===
{{head|doth|preposition}}

# on, upon, in
# off of
# off of
# onto
# onto

#:Dalen rhaggat eveth ma ale vekhi she Vaes Seris.
#::There are thousands of ships in the free cities.

#:Eyel varthasoe she ilekaan rikhoya.
#::The rain will fall on your rotting skin.

