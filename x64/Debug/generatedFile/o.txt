{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[elat]]''
# singular past tense negative conjugation of ''[[elat]]''


=====Inflection=====
{{Template:dothra-vVV|root=|thema=e}}

