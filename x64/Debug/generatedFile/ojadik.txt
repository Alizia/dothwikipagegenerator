{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oaˈd̪ik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[jadilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=jad|thema=i}}

