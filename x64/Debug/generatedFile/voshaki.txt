{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvoʃaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[oshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=osh|epe=}}

