{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[exugaˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[ekhogaralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ekhogar|thema=a}}

