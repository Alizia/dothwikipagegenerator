{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vaeˈsis]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[vaesilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vaes|thema=i}}

