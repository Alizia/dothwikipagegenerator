{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zifiˈt͡ʃoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[zifichelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zifich|thema=e}}

