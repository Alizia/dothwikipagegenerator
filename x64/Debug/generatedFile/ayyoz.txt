{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ajˈjoz]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[ayyozat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ayyoz|epe=}}

