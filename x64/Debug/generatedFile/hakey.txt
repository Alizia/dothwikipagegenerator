{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈkej]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[hakelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hak|thema=e}}

