{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪ɤˈgak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[dogat]]''


=====Inflection=====
{{Template:dothra-vCC|root=dog|epe=e}}

