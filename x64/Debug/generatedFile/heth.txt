{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈheθ]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# rim, lips (of a person)

=====Inflection=====
{{Template:dothra-ni|root=heth|thema=}}

