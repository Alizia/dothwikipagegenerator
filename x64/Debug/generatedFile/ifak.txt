{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[iˈfak]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# foreigner

=====Inflection=====
{{Template:dothra-na|root=ifak|thema=}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ifat]]''


=====Inflection=====
{{Template:dothra-vVC|root=if|epe=}}

