{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[velˈzer]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[velzerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=velzer|epe=}}

