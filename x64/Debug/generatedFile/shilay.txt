{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʃiˈl̪aj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[shilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=shil|epe=}}

