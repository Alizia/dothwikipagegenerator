{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[gen̪ˈd̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[gendat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gend|epe=}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[gendolat]]''
# informal negative imperative conjugation of ''[[gendolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=gend|thema=o}}

