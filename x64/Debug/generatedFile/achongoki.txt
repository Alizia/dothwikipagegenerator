{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat͡ʃoŋguki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[chongolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=chong|thema=o}}

