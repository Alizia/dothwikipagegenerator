{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvit̪t̪eo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[ittelat]]''
# third person singular future tense negative conjugation of ''[[ittelat]]''
# second person plural future tense negative conjugation of ''[[ittelat]]''
# third person plural future tense negative conjugation of ''[[ittelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=itt|thema=e}}

