{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aviazeˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[vijazerolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vijazer|thema=o}}

