{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[assiˈl̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[assilat]]''


=====Inflection=====
{{Template:dothra-vVC|root=assil|epe=}}

