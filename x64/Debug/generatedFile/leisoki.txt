{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪eisoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[leisolat]]''
# first person plural present tense negative conjugation of ''[[leisolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=leis|thema=o}}

