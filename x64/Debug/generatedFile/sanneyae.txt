{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈsan̪n̪ejae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[sanneyalat]]''
# third person singular present tense positive conjugation of ''[[sanneyalat]]''
# second person plural present tense positive conjugation of ''[[sanneyalat]]''
# third person plural present tense positive conjugation of ''[[sanneyalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=sanney|thema=a}}

