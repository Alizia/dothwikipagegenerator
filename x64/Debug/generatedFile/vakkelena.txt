{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvakkel̪en̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[akkelenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkelen|epe=}}

