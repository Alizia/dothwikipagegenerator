{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ʃiˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to know (someone), to be familiar with

=====Inflection=====
{{Template:dothra-vCC|root=shil|epe=}}

