{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ot͡ʃoˈmok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[chomolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=chom|thema=o}}

