{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ol̪ˈd̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[oldat]]''


=====Inflection=====
{{Template:dothra-vVC|root=old|epe=}}

