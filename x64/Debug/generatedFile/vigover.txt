{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[viguˈver]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[vigoverat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vigover|epe=}}

