{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[hojaˈl̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[hoyalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hoyal|epe=}}

