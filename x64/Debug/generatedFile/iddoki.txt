{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈid̪d̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[iddelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=idd|thema=e}}

