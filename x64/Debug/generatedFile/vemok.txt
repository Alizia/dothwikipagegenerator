{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[veˈmok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[emat]]''


=====Inflection=====
{{Template:dothra-vVC|root=em|epe=}}

