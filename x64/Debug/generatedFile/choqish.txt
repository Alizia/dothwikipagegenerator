{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃoˈqeʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[choqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=choq|epe=e}}

