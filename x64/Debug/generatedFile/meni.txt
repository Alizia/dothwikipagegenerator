{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmen̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[menat]]''
# second person singular present tense positive conjugation of ''[[menat]]''
# second person plural present tense positive conjugation of ''[[menat]]''
# third person plural present tense positive conjugation of ''[[menat]]''
# second person singular present tense negative conjugation of ''[[menat]]''
# second person plural present tense negative conjugation of ''[[menat]]''
# third person plural present tense negative conjugation of ''[[menat]]''


=====Inflection=====
{{Template:dothra-vCC|root=men|epe=}}

