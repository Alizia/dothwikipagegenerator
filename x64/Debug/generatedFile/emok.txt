{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eˈmok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[emat]]''


=====Inflection=====
{{Template:dothra-vVC|root=em|epe=}}

