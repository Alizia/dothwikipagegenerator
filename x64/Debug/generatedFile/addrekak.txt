{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪d̪ɾeˈkak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[addrekat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addrek|epe=}}

