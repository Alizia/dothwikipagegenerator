{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvar.roxu]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[arrokhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arrokh|epe=}}

