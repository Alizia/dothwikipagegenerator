{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfɾaxa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[frakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=frakh|epe=}}

