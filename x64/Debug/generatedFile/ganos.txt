{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[gaˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[ganat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gan|epe=}}

