{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmaɾie]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[marilat]]''
# third person singular present tense positive conjugation of ''[[marilat]]''
# second person plural present tense positive conjugation of ''[[marilat]]''
# third person plural present tense positive conjugation of ''[[marilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mar|thema=i}}

