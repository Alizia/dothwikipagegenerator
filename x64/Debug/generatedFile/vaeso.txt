{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaeso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[vaesilat]]''
# singular past tense negative conjugation of ''[[vaesilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vaes|thema=i}}

