{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ahaˈl̪ek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[halelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hal|thema=e}}

