{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[viʃafeˈɾoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[vishaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vishafer|epe=}}

