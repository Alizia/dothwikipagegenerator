{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vat̪t̪eˈɾoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[vatterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vatter|epe=}}

