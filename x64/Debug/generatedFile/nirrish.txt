{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪ir.ˈriʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[nirrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nirr|epe=e}}

