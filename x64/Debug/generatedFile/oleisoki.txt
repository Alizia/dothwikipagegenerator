{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈol̪eisoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[leisolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=leis|thema=o}}

