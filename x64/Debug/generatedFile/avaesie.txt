{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈavaesie]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[vaesilat]]''
# third person singular future tense positive conjugation of ''[[vaesilat]]''
# second person plural future tense positive conjugation of ''[[vaesilat]]''
# third person plural future tense positive conjugation of ''[[vaesilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vaes|thema=i}}

