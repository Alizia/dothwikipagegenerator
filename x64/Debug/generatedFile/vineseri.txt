{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvin̪eseɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[vineserat]]''
# second person singular present tense positive conjugation of ''[[vineserat]]''
# second person plural present tense positive conjugation of ''[[vineserat]]''
# third person plural present tense positive conjugation of ''[[vineserat]]''
# second person singular present tense negative conjugation of ''[[vineserat]]''
# second person plural present tense negative conjugation of ''[[vineserat]]''
# third person plural present tense negative conjugation of ''[[vineserat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vineser|epe=}}

