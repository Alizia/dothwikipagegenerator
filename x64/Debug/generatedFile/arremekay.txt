{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ar.remeˈkaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[arremekat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arremek|epe=}}

