{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[mosˈkas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[moskat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mosk|epe=}}

