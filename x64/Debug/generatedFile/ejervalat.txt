{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[eeɾvaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to remove

#:Ishish chare acharoe hash me nem ejervae nharesoon.
#::Maybe the ear will listen if it is removed from the head.

=====Inflection=====
{{Template:dothra-vVV|root=ejerv|thema=a}}

