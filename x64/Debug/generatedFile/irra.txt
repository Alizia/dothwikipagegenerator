{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈir.ra]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# trout

=====Inflection=====
{{Template:dothra-ni|root=irr|thema=a}}

