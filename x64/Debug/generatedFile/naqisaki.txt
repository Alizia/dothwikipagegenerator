{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪aqesaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[naqisat]]''


=====Inflection=====
{{Template:dothra-vCC|root=naqis|epe=}}

