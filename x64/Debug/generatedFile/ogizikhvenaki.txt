{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈogizixven̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[gizikhvenat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gizikhven|epe=}}

