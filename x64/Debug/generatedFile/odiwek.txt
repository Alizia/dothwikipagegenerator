{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[od̪iˈwek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[diwelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=diw|thema=e}}

