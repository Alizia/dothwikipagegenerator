{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[veˈmɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[emralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=emr|thema=a}}

