{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t̪iˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[tirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tir|epe=}}

