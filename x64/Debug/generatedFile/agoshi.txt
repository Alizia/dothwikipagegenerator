{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaguʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[goshat]]''
# second person plural future tense positive conjugation of ''[[goshat]]''
# third person plural future tense positive conjugation of ''[[goshat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gosh|epe=}}

