{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[t̪iˈhat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to look, to see

=====Inflection=====
{{Template:dothra-vCC|root=tih|epe=}}

