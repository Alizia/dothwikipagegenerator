{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt͡ʃil̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[chilat]]''
# singular past tense negative conjugation of ''[[chilat]]''
# third person singular present tense negative conjugation of ''[[chilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chil|epe=}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[chilolat]]''
# singular past tense positive conjugation of ''[[chilolat]]''
# formal negative imperative conjugation of ''[[chilolat]]''
# singular past tense negative conjugation of ''[[chilolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=chil|thema=o}}

