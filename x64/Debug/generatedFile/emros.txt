{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eˈmɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[emralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=emr|thema=a}}

