{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qɑˈt͡ʃoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[qachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qach|epe=}}

