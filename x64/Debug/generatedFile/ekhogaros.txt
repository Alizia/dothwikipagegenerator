{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[exugaˈɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[ekhogaralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ekhogar|thema=a}}

