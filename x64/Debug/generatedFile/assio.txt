{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈassio]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[assilat]]''
# third person singular present tense negative conjugation of ''[[assilat]]''
# second person plural present tense negative conjugation of ''[[assilat]]''
# third person plural present tense negative conjugation of ''[[assilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ass|thema=i}}

