{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʃaˈɾoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[sharat]]''


=====Inflection=====
{{Template:dothra-vCC|root=shar|epe=}}

