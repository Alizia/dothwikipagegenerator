{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪aɾiˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[darinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=darin|epe=}}

