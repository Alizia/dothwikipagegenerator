{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vovvaˈhak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[ovvahat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ovvah|epe=}}

