{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪ɤmmo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[lommat]]''
# singular past tense negative conjugation of ''[[lommat]]''
# third person singular present tense negative conjugation of ''[[lommat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lomm|epe=}}

