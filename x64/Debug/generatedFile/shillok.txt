{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʃil̪ˈl̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[shillolat]]''
# first person singular present tense negative conjugation of ''[[shillolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=shill|thema=o}}

