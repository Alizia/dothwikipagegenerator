{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈqɔs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[oqolat]]''
# informal negative imperative conjugation of ''[[oqolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=oq|thema=o}}

