{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vohaˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[oharat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ohar|epe=}}

