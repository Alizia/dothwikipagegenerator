{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈzal̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[zalat]]''
# second person singular present tense positive conjugation of ''[[zalat]]''
# second person plural present tense positive conjugation of ''[[zalat]]''
# third person plural present tense positive conjugation of ''[[zalat]]''
# second person singular present tense negative conjugation of ''[[zalat]]''
# second person plural present tense negative conjugation of ''[[zalat]]''
# third person plural present tense negative conjugation of ''[[zalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zal|epe=}}

