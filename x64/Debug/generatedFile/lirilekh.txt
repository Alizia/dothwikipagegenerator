{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[l̪iɾiˈl̪ex]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# letter, number, symbol, glyph, graph

=====Inflection=====
{{Template:dothra-ni|root=lirilekh|thema=}}

