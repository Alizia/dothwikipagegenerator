{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aziˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[zisat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zis|epe=}}

