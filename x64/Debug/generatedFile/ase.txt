{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈase]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# word
# command
# command

=====Inflection=====
{{Template:dothra-ni|root=as|thema=e}}

