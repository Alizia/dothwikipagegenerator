{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[il̪ˈd̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[ildat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ild|epe=}}

