{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eˈmɾoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[emralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=emr|thema=a}}

