{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈsao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[sajat]]''
# singular past tense negative conjugation of ''[[sajat]]''
# third person singular present tense negative conjugation of ''[[sajat]]''


=====Inflection=====
{{Template:dothra-vCC|root=saj|epe=}}

====Noun====
{{head|doth|noun|nominative, animate}}

# steed, one's own horse

=====Inflection=====
{{Template:dothra-na|root=saj|thema=o}}

