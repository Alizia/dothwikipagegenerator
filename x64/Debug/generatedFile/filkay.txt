{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[fil̪ˈkaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[filkat]]''


=====Inflection=====
{{Template:dothra-vCC|root=filk|epe=}}

