{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈesemɾasoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[esemrasalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esemras|thema=a}}

