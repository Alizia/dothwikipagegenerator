{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈjamoɾi]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# youth

=====Inflection=====
{{Template:dothra-na|root=yamor|thema=i}}

