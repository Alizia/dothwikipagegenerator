{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[heθˈkiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[hethkat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hethk|epe=}}

