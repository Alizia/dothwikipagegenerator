{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪avɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[davralat]]''


=====Inflection=====
{{Template:dothra-vCV|root=davr|thema=a}}

