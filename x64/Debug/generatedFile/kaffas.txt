{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kafˈfas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[kaffat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kaff|epe=}}

