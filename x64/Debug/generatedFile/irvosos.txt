{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[iɾvoˈsos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[irvosat]]''


=====Inflection=====
{{Template:dothra-vVC|root=irvos|epe=}}

