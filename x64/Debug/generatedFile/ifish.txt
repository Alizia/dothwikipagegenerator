{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[iˈfiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[ifat]]''


=====Inflection=====
{{Template:dothra-vVC|root=if|epe=}}

