{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪ɤɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[lorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lor|epe=}}

