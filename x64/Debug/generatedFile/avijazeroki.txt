{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaviazeɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[vijazerolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vijazer|thema=o}}

