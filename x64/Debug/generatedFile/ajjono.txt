{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad.d͡ʒon̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[ajjonat]]''
# singular past tense negative conjugation of ''[[ajjonat]]''
# third person singular present tense negative conjugation of ''[[ajjonat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ajjon|epe=}}

