{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[d̪ɤθˈɾax]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# ride

=====Inflection=====
{{Template:dothra-ni|root=dothrakh|thema=}}

