{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[haoˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to grow strong

=====Inflection=====
{{Template:dothra-vCV|root=haj|thema=o}}

