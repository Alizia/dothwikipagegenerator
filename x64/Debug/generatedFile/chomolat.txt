{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[t͡ʃomoˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to grow respectful
to give respect to, to honour
# to give respect to, to honour

=====Inflection=====
{{Template:dothra-vCV|root=chom|thema=o}}

