{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈveʃoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[eshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=esh|epe=}}

