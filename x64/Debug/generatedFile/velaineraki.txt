{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvel̪ain̪eɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[velainerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=velainer|epe=}}

