{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪ɤsˈtak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[lostat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lost|epe=}}

