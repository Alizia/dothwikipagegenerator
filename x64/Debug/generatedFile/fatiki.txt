{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfat̪iki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[fatilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=fat|thema=i}}

