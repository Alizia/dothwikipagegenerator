{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zeɾˈqɔs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[zerqolat]]''
# informal negative imperative conjugation of ''[[zerqolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zerq|thema=o}}

