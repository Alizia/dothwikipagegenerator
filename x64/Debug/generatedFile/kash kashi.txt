{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈkaʃ ˈkaʃi]|lang=doth}}

===Adverb===
{{head|doth|adverb}}

# once

#:Ei kisha imeshish, kash kashi.
#::We were all young, once.

