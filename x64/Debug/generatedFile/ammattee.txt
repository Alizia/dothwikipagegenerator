{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈammat̪t̪ee]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[ammattelat]]''
# third person singular present tense positive conjugation of ''[[ammattelat]]''
# second person plural present tense positive conjugation of ''[[ammattelat]]''
# third person plural present tense positive conjugation of ''[[ammattelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ammatt|thema=e}}

