{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈesaʒoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[esazhalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esazh|thema=a}}

