{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[jasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jas|epe=}}

