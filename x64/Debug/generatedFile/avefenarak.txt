{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[avefen̪aˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[vefenarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vefenar|epe=}}

