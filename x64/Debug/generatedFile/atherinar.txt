{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθeɾiˈn̪ar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# kindness

=====Inflection=====
{{Template:dothra-ni|root=atherinar|thema=}}

