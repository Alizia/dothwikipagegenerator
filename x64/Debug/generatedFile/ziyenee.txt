{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈzijen̪ee]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[ziyenelat]]''
# third person singular present tense positive conjugation of ''[[ziyenelat]]''
# second person plural present tense positive conjugation of ''[[ziyenelat]]''
# third person plural present tense positive conjugation of ''[[ziyenelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziyen|thema=e}}

