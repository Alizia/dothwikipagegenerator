{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈki]|lang=doth}}

===Preposition===
{{head|doth|preposition}}

# by, because of, of
# thus, that (when preceding quoted speech)
# thus, that (when preceding quoted speech)

