{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈovefen̪aɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[vefenarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vefenar|epe=}}

