{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪avi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[tavat]]''
# second person singular present tense positive conjugation of ''[[tavat]]''
# second person plural present tense positive conjugation of ''[[tavat]]''
# third person plural present tense positive conjugation of ''[[tavat]]''
# second person singular present tense negative conjugation of ''[[tavat]]''
# second person plural present tense negative conjugation of ''[[tavat]]''
# third person plural present tense negative conjugation of ''[[tavat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tav|epe=}}

