{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[akkel̪eˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[akkelenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkelen|epe=}}

