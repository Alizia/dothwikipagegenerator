{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈʃaɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[sharat]]''


=====Inflection=====
{{Template:dothra-vCC|root=shar|epe=}}

