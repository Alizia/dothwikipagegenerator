{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ohaˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[oharat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ohar|epe=}}

