{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[affazoˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to replace

=====Inflection=====
{{Template:dothra-vVV|root=affaz|thema=o}}

