{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvefa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[efat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ef|epe=}}

