{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvammemoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[ammemat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammem|epe=}}

