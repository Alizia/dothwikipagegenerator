{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈid̪ɾisofi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[idrisofat]]''
# second person singular present tense positive conjugation of ''[[idrisofat]]''
# second person plural present tense positive conjugation of ''[[idrisofat]]''
# third person plural present tense positive conjugation of ''[[idrisofat]]''
# second person singular present tense negative conjugation of ''[[idrisofat]]''
# second person plural present tense negative conjugation of ''[[idrisofat]]''
# third person plural present tense negative conjugation of ''[[idrisofat]]''


=====Inflection=====
{{Template:dothra-vVC|root=idrisof|epe=}}

