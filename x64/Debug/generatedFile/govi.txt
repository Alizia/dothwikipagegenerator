{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈguvi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[govat]]''
# second person singular present tense positive conjugation of ''[[govat]]''
# second person plural present tense positive conjugation of ''[[govat]]''
# third person plural present tense positive conjugation of ''[[govat]]''
# second person singular present tense negative conjugation of ''[[govat]]''
# second person plural present tense negative conjugation of ''[[govat]]''
# third person plural present tense negative conjugation of ''[[govat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gov|epe=}}

