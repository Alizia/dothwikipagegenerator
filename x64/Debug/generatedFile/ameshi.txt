{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈameʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[meshat]]''
# second person plural future tense positive conjugation of ''[[meshat]]''
# third person plural future tense positive conjugation of ''[[meshat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mesh|epe=}}

