{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[avel̪ain̪eˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[velainerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=velainer|epe=}}

