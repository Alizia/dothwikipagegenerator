{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈvoɾsa]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# fire

=====Inflection=====
{{Template:dothra-na|root=vors|thema=a}}

