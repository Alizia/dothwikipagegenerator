{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrot͡ʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[rochat]]''
# second person singular present tense positive conjugation of ''[[rochat]]''
# second person plural present tense positive conjugation of ''[[rochat]]''
# third person plural present tense positive conjugation of ''[[rochat]]''
# second person singular present tense negative conjugation of ''[[rochat]]''
# second person plural present tense negative conjugation of ''[[rochat]]''
# third person plural present tense negative conjugation of ''[[rochat]]''


=====Inflection=====
{{Template:dothra-vCC|root=roch|epe=}}

