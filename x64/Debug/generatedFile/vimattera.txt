{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvimat̪t̪eɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[vimatterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vimatter|epe=}}

