{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[saqɔˈjoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[saqoyalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=saqoy|thema=a}}

