{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvejel̪iki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[eyelilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=eyel|thema=i}}

