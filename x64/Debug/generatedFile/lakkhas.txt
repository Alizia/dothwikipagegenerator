{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪akˈxas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[lakkhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lakkh|epe=}}

