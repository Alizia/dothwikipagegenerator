{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪evo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[nevalat]]''
# singular past tense negative conjugation of ''[[nevalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nev|thema=a}}

