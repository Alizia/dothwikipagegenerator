{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvelzeɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[velzerat]]''
# singular past tense negative conjugation of ''[[velzerat]]''
# third person singular present tense negative conjugation of ''[[velzerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=velzer|epe=}}

