{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoguɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[gorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gor|epe=}}

