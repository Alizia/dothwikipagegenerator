{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈon̪ɤt̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[notat]]''
# second person plural future tense negative conjugation of ''[[notat]]''
# third person plural future tense negative conjugation of ''[[notat]]''


=====Inflection=====
{{Template:dothra-vCC|root=not|epe=}}

