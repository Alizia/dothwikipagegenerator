{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vit̪iʃeˈɾiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[vitisherat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitisher|epe=}}

