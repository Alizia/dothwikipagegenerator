{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmiθɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[mithrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mithr|epe=e}}

