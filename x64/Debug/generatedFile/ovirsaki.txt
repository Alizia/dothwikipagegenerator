{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoviɾsaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[virsalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=virs|thema=a}}

