{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[velzeˈɾat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to stall, to procrastinate
to make excuses
# to make excuses

=====Inflection=====
{{Template:dothra-vCC|root=velzer|epe=}}

