{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[roˈxas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[rokhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rokh|epe=}}

