{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[heˈmix]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# pelt

=====Inflection=====
{{Template:dothra-ni|root=hemikh|thema=}}

