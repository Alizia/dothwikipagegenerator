{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪ivo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[nivat]]''
# singular past tense negative conjugation of ''[[nivat]]''
# third person singular present tense negative conjugation of ''[[nivat]]''


=====Inflection=====
{{Template:dothra-vCC|root=niv|epe=}}

