{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvit͡ʃomeɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[vichomerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vichomer|epe=}}

