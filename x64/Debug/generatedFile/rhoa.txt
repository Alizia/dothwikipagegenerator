{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈrhoa]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# animal, beast

=====Inflection=====
{{Template:dothra-ni|root=rho|thema=a}}

