{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈθoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[atholat]]''
# plural past tense negative conjugation of ''[[atholat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ath|thema=o}}

