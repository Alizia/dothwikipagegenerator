{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfɾed̪ɾiki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[fredrilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=fredr|thema=i}}

