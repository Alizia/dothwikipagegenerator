{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪eso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[nesat]]''
# singular past tense negative conjugation of ''[[nesat]]''
# third person singular present tense negative conjugation of ''[[nesat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nes|epe=}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[nesolat]]''
# singular past tense positive conjugation of ''[[nesolat]]''
# formal negative imperative conjugation of ''[[nesolat]]''
# singular past tense negative conjugation of ''[[nesolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nes|thema=o}}

