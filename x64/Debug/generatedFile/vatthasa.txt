{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaθ.θasa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[atthasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=atthas|epe=}}

