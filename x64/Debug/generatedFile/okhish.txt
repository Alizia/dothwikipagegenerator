{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈxiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[okhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=okh|epe=}}

