{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈven̪ɤe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[venolat]]''
# third person singular present tense positive conjugation of ''[[venolat]]''
# second person plural present tense positive conjugation of ''[[venolat]]''
# third person plural present tense positive conjugation of ''[[venolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ven|thema=o}}

