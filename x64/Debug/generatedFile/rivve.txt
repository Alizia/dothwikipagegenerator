{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈrivve]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# sniff

=====Inflection=====
{{Template:dothra-ni|root=rivv|thema=e|class=b}}

