{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vat̪t̪eˈɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[vatterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vatter|epe=}}

