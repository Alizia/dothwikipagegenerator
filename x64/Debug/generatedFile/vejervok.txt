{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[veeɾˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[ejervalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ejerv|thema=a}}

