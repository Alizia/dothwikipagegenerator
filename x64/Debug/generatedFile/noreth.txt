{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[n̪ɤˈɾeθ]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# hair

=====Inflection=====
{{Template:dothra-ni|root=noreth|thema=}}

