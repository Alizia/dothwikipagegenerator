{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪d̪iˈwes]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[addiwelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=addiw|thema=e}}

