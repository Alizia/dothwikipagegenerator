{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vasˈtak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[astat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ast|epe=}}

