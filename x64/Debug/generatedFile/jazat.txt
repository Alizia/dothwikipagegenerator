{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aˈzat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to block

=====Inflection=====
{{Template:dothra-vCC|root=jaz|epe=}}

