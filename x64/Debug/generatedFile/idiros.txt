{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[id̪iˈɾos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[idirolat]]''
# informal negative imperative conjugation of ''[[idirolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=idir|thema=o}}

