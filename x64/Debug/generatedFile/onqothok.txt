{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[on̪qɔˈθok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[onqothat]]''


=====Inflection=====
{{Template:dothra-vVC|root=onqoth|epe=}}

