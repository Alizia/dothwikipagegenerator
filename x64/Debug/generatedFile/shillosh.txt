{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʃil̪ˈl̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[shillolat]]''
# plural past tense negative conjugation of ''[[shillolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=shill|thema=o}}

