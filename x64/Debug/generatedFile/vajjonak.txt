{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vad.d͡ʒoˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[ajjonat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ajjon|epe=}}

