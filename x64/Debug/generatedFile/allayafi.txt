{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪l̪ajafi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[allayafat]]''
# second person singular present tense positive conjugation of ''[[allayafat]]''
# second person plural present tense positive conjugation of ''[[allayafat]]''
# third person plural present tense positive conjugation of ''[[allayafat]]''
# second person singular present tense negative conjugation of ''[[allayafat]]''
# second person plural present tense negative conjugation of ''[[allayafat]]''
# third person plural present tense negative conjugation of ''[[allayafat]]''


=====Inflection=====
{{Template:dothra-vVC|root=allayaf|epe=}}

