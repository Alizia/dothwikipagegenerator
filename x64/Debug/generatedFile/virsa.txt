{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈviɾsa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[virsalat]]''
# singular past tense positive conjugation of ''[[virsalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=virs|thema=a}}

