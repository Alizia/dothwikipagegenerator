{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈammiθɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[ammithrat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammithr|epe=e}}

