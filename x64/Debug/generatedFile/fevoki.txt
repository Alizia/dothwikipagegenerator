{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfevoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[fevelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=fev|thema=e}}

