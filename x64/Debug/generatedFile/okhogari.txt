{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoxugaɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[khogarat]]''
# second person plural future tense negative conjugation of ''[[khogarat]]''
# third person plural future tense negative conjugation of ''[[khogarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=khogar|epe=}}

