{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[l̪aiˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to spar, to train
to play, to compete (a game that is played can be introuced with ki)
# to play, to compete (a game that is played can be introuced with ki)

=====Inflection=====
{{Template:dothra-vCV|root=laj|thema=i}}

