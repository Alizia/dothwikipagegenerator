{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈn̪in̪d̪i]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# sausage

=====Inflection=====
{{Template:dothra-ni|root=nind|thema=i}}

