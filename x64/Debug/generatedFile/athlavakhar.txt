{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθl̪avaˈxar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# loudness

=====Inflection=====
{{Template:dothra-ni|root=athlavakhar|thema=}}

