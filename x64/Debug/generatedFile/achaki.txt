{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat͡ʃaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[chakat]]''
# second person plural future tense positive conjugation of ''[[chakat]]''
# third person plural future tense positive conjugation of ''[[chakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chak|epe=}}

