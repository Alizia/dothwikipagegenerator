{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈeʒiɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[ezhirat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ezhir|epe=}}

