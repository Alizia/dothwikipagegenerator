{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvasʃil̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[asshilat]]''


=====Inflection=====
{{Template:dothra-vVC|root=asshil|epe=}}

