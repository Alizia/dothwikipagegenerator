{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪aˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[navilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nav|thema=i}}

