{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪in̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[dinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=din|epe=}}

