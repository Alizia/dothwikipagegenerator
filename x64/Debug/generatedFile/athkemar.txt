{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθkeˈmar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# marriage

=====Inflection=====
{{Template:dothra-ni|root=athkemar|thema=}}

