{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈon̪avaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

