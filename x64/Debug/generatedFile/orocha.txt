{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoɾot͡ʃa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[rochat]]''


=====Inflection=====
{{Template:dothra-vCC|root=roch|epe=}}

