{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪aɾiˈn̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[darinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=darin|epe=}}

