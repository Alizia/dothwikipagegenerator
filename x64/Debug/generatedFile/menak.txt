{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[meˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[menat]]''


=====Inflection=====
{{Template:dothra-vCC|root=men|epe=}}

