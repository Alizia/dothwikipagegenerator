{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvimeʃo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[imeshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=imesh|epe=}}

