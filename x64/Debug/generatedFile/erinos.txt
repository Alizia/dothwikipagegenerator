{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eɾiˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[erinat]]''


=====Inflection=====
{{Template:dothra-vVC|root=erin|epe=}}

