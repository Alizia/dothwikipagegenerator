{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[haˈd̪ɤr]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# gust of wind

=====Inflection=====
{{Template:dothra-ni|root=hador|thema=}}

