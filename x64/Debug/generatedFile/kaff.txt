{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kafˈf]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[kaffat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kaff|epe=}}

