{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vin̪eseˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[vineserat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vineser|epe=}}

