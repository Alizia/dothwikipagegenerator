{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈhaoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[hajolat]]''
# third person singular present tense positive conjugation of ''[[hajolat]]''
# second person plural present tense positive conjugation of ''[[hajolat]]''
# third person plural present tense positive conjugation of ''[[hajolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=haj|thema=o}}

