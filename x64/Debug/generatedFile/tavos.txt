{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t̪aˈvos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[tavat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tav|epe=}}

