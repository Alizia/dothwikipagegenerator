{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈeɾva]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[jervat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jerv|epe=}}

