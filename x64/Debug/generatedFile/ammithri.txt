{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[amˈmiθɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ammithrat]]''
# second person singular present tense positive conjugation of ''[[ammithrat]]''
# second person plural present tense positive conjugation of ''[[ammithrat]]''
# third person plural present tense positive conjugation of ''[[ammithrat]]''
# second person singular present tense negative conjugation of ''[[ammithrat]]''
# second person plural present tense negative conjugation of ''[[ammithrat]]''
# third person plural present tense negative conjugation of ''[[ammithrat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammithr|epe=e}}

