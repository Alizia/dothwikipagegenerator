{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈid̪ɾie]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[idrilat]]''
# third person singular present tense positive conjugation of ''[[idrilat]]''
# second person plural present tense positive conjugation of ''[[idrilat]]''
# third person plural present tense positive conjugation of ''[[idrilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=idr|thema=i}}

