{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈkaffa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[kaffat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kaff|epe=}}

