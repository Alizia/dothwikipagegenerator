{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ejjeˈl̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[eyyelilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=eyyel|thema=i}}

