{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ammaˈsos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[ammasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammas|epe=}}

