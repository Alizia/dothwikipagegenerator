{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmat̪t̪eo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[mattelat]]''
# third person singular present tense negative conjugation of ''[[mattelat]]''
# second person plural present tense negative conjugation of ''[[mattelat]]''
# third person plural present tense negative conjugation of ''[[mattelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=matt|thema=e}}

