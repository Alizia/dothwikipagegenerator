{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vat̪t̪eˈɾiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[vatterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vatter|epe=}}

