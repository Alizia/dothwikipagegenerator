{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ziˈɾisso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[zirisselat]]''
# singular past tense negative conjugation of ''[[zirisselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziriss|thema=e}}

