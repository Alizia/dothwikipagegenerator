{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪ɤn̪]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[donat]]''


=====Inflection=====
{{Template:dothra-vCC|root=don|epe=}}

