{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ziɾoqɔˈsek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ziroqoselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziroqos|thema=e}}

