{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈqɛmmoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[qemmolat]]''
# first person plural present tense negative conjugation of ''[[qemmolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=qemm|thema=o}}

