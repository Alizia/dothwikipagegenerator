{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvel̪ain̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[elainat]]''


=====Inflection=====
{{Template:dothra-vVC|root=elain|epe=}}

