{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[voˈguk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[ogat]]''


=====Inflection=====
{{Template:dothra-vVC|root=og|epe=e}}

