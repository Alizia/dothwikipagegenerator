{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈviazeɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[vijazerat]]''
# singular past tense negative conjugation of ''[[vijazerat]]''
# third person singular present tense negative conjugation of ''[[vijazerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vijazer|epe=}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[vijazerolat]]''
# singular past tense positive conjugation of ''[[vijazerolat]]''
# formal negative imperative conjugation of ''[[vijazerolat]]''
# singular past tense negative conjugation of ''[[vijazerolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vijazer|thema=o}}

