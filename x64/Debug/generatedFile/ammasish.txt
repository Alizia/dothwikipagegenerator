{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ammaˈsiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[ammasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammas|epe=}}

