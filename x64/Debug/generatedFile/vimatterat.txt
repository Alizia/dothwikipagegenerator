{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[vimat̪t̪eˈɾat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to limp

=====Inflection=====
{{Template:dothra-vCC|root=vimatter|epe=}}

