{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaħ.hafi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[ahhafat]]''
# second person plural future tense positive conjugation of ''[[ahhafat]]''
# third person plural future tense positive conjugation of ''[[ahhafat]]''
# second person singular future tense negative conjugation of ''[[ahhafat]]''
# second person plural future tense negative conjugation of ''[[ahhafat]]''
# third person plural future tense negative conjugation of ''[[ahhafat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhaf|epe=}}

