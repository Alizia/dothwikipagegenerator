{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aħ.ˈhas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[ahhasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhas|epe=}}

