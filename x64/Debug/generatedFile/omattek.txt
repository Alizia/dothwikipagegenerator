{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[omat̪ˈt̪ek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[mattelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=matt|thema=e}}

