{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvexuki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[vekhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vekh|epe=}}

