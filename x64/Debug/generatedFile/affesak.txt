{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[affeˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[affesat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affes|epe=}}

