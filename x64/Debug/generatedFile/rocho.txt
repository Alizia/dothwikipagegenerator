{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrot͡ʃo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[rochat]]''
# singular past tense negative conjugation of ''[[rochat]]''
# third person singular present tense negative conjugation of ''[[rochat]]''


=====Inflection=====
{{Template:dothra-vCC|root=roch|epe=}}

