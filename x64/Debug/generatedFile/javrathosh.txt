{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[avɾaˈθoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[javrathat]]''


=====Inflection=====
{{Template:dothra-vCC|root=javrath|epe=}}

