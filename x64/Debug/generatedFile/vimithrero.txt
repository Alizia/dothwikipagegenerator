{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvimiθɾeɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[vimithrerat]]''
# singular past tense negative conjugation of ''[[vimithrerat]]''
# third person singular present tense negative conjugation of ''[[vimithrerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vimithrer|epe=}}

