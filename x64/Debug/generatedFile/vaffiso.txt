{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaffiso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[affisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affis|epe=}}

