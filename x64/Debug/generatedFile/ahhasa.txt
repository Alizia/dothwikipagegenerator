{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaħ.hasa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[ahhasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhas|epe=}}

