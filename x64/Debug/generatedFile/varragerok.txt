{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[var.rageˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[varragerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=varrager|epe=}}

