{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθn̪iˈθar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# pain

=====Inflection=====
{{Template:dothra-ni|root=athnithar|thema=}}

