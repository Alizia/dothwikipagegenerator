{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvesit̪t̪esao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[esittesalat]]''
# third person singular future tense negative conjugation of ''[[esittesalat]]''
# second person plural future tense negative conjugation of ''[[esittesalat]]''
# third person plural future tense negative conjugation of ''[[esittesalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esittes|thema=a}}

