{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvesin̪asoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[esinasolat]]''
# third person singular future tense positive conjugation of ''[[esinasolat]]''
# second person plural future tense positive conjugation of ''[[esinasolat]]''
# third person plural future tense positive conjugation of ''[[esinasolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esinas|thema=o}}

