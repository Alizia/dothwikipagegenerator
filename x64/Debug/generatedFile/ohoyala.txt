{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈohojal̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[hoyalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hoyal|epe=}}

