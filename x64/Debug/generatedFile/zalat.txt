{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[zaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to hope for, to want (to own something)
to hope for, to want (something to happen)
# to hope for, to want (something to happen)

#:Khaleesi zala meme adakha esinakh ajjalan.
#::The khaleesi wants to eat something different tonight.

#:Ifak asta meme zala firikhnharen.
#::The foreigner says that he wants a crown.

=====Inflection=====
{{Template:dothra-vCC|root=zal|epe=}}

