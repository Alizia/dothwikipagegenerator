{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈl̪ɤr.ri]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[lorrat]]''
# second person plural future tense negative conjugation of ''[[lorrat]]''
# third person plural future tense negative conjugation of ''[[lorrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lorr|epe=e}}

