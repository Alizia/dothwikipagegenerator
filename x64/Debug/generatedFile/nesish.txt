{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪eˈsiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[nesat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nes|epe=}}

