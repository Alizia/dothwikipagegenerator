{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[meˈʃak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[meshat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mesh|epe=}}

