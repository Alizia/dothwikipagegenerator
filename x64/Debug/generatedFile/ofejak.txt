{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ofeˈak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[fejat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fej|epe=}}

