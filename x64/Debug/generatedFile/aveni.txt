{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaven̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[venat]]''
# second person plural future tense positive conjugation of ''[[venat]]''
# third person plural future tense positive conjugation of ''[[venat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ven|epe=}}

