{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈozigan̪eki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[ziganelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zigan|thema=e}}

