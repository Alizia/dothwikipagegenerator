{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪ɾoˈʒiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[drozhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=drozh|epe=}}

