{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[xeˈfok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[khefat]]''


=====Inflection=====
{{Template:dothra-vCC|root=khef|epe=}}

