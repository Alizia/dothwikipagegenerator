{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[van̪n̪eˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[annevalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=annev|thema=a}}

