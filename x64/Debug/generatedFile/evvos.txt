{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[evˈvos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[evvat]]''


=====Inflection=====
{{Template:dothra-vVC|root=evv|epe=}}

