{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eɾˈvas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[jervat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jerv|epe=}}

