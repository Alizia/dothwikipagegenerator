{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kaɾˈt̪aj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[kartat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kart|epe=}}

