{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aqˈqes]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[aqqisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aqqis|epe=}}

