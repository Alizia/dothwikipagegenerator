{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈan̪n̪eva]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[annevalat]]''
# singular past tense positive conjugation of ''[[annevalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=annev|thema=a}}

