{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kaˈd̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[kadat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kad|epe=}}

