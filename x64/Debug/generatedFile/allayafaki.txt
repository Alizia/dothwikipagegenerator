{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪l̪ajafaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[allayafat]]''


=====Inflection=====
{{Template:dothra-vVC|root=allayaf|epe=}}

