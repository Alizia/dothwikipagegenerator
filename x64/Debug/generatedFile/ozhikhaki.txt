{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoʒixaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[zhikhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zhikh|epe=}}

