{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪eʃit̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[leshitat]]''


=====Inflection=====
{{Template:dothra-vCC|root=leshit|epe=}}

