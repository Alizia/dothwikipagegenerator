{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪ɾoˈak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[nrojat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nroj|epe=}}

