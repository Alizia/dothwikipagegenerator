{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvefen̪aɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[vefenarat]]''
# second person singular present tense positive conjugation of ''[[vefenarat]]''
# second person plural present tense positive conjugation of ''[[vefenarat]]''
# third person plural present tense positive conjugation of ''[[vefenarat]]''
# second person singular present tense negative conjugation of ''[[vefenarat]]''
# second person plural present tense negative conjugation of ''[[vefenarat]]''
# third person plural present tense negative conjugation of ''[[vefenarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vefenar|epe=}}

