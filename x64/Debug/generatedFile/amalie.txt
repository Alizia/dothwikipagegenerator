{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈamal̪ie]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[malilat]]''
# third person singular future tense positive conjugation of ''[[malilat]]''
# second person plural future tense positive conjugation of ''[[malilat]]''
# third person plural future tense positive conjugation of ''[[malilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mal|thema=i}}

