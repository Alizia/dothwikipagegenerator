{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪ɾeka]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[drekat]]''


=====Inflection=====
{{Template:dothra-vCC|root=drek|epe=}}

