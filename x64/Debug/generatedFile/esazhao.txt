{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈesaʒao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[esazhalat]]''
# third person singular present tense negative conjugation of ''[[esazhalat]]''
# second person plural present tense negative conjugation of ''[[esazhalat]]''
# third person plural present tense negative conjugation of ''[[esazhalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esazh|thema=a}}

