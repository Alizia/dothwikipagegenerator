{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ar.ˈragga]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[arraggat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arragg|epe=e}}

