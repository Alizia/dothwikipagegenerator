{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[esit̪t̪eˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[esittesalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esittes|thema=a}}

