{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt͡ʃil̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[chilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chil|epe=}}

