{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪iθaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[nithat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nith|epe=}}

