{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eʒiˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ezhirat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ezhir|epe=}}

