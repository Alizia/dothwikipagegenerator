{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qɑˈjos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[qayelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=qay|thema=e}}

