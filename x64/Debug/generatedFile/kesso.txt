{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈkesso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[kesselat]]''
# singular past tense negative conjugation of ''[[kesselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=kess|thema=e}}

