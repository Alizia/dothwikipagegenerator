{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈsoqe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[soqat]]''
# second person singular present tense positive conjugation of ''[[soqat]]''
# second person plural present tense positive conjugation of ''[[soqat]]''
# third person plural present tense positive conjugation of ''[[soqat]]''
# second person singular present tense negative conjugation of ''[[soqat]]''
# second person plural present tense negative conjugation of ''[[soqat]]''
# third person plural present tense negative conjugation of ''[[soqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=soq|epe=e}}

