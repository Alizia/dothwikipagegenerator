{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈjaɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[yarat]]''
# singular past tense negative conjugation of ''[[yarat]]''
# third person singular present tense negative conjugation of ''[[yarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yar|epe=}}

