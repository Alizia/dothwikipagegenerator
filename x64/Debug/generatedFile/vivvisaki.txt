{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvivvisaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[ivvisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ivvis|epe=}}

