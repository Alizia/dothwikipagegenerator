{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaħ.hafi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ahhafat]]''
# second person singular present tense positive conjugation of ''[[ahhafat]]''
# second person plural present tense positive conjugation of ''[[ahhafat]]''
# third person plural present tense positive conjugation of ''[[ahhafat]]''
# second person singular present tense negative conjugation of ''[[ahhafat]]''
# second person plural present tense negative conjugation of ''[[ahhafat]]''
# third person plural present tense negative conjugation of ''[[ahhafat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhaf|epe=}}

