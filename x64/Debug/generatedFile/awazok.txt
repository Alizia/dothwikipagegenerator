{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[awaˈzok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[awazat]]''


=====Inflection=====
{{Template:dothra-vVC|root=awaz|epe=}}

