{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈext̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[jekhtat]]''
# second person singular present tense positive conjugation of ''[[jekhtat]]''
# second person plural present tense positive conjugation of ''[[jekhtat]]''
# third person plural present tense positive conjugation of ''[[jekhtat]]''
# second person singular present tense negative conjugation of ''[[jekhtat]]''
# second person plural present tense negative conjugation of ''[[jekhtat]]''
# third person plural present tense negative conjugation of ''[[jekhtat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jekht|epe=}}

