{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvel̪t̪ɤɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[veltorat]]''
# singular past tense negative conjugation of ''[[veltorat]]''
# third person singular present tense negative conjugation of ''[[veltorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=veltor|epe=}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[veltorolat]]''
# singular past tense positive conjugation of ''[[veltorolat]]''
# formal negative imperative conjugation of ''[[veltorolat]]''
# singular past tense negative conjugation of ''[[veltorolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=veltor|thema=o}}

