{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaθoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[atholat]]''
# third person singular present tense positive conjugation of ''[[atholat]]''
# second person plural present tense positive conjugation of ''[[atholat]]''
# third person plural present tense positive conjugation of ''[[atholat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ath|thema=o}}

