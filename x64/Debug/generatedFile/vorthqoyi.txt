{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈvoɾθqɔji]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# fang

=====Inflection=====
{{Template:dothra-ni|root=vorthqoy|thema=i}}

