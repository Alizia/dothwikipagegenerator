{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪iˈθij]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[nithilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nith|thema=i}}

