{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪axa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[nakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nakh|epe=}}

