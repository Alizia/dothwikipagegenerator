{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oqɔvˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[qovvolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=qovv|thema=o}}

