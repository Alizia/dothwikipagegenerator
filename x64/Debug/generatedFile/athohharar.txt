{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθoħ.haˈɾar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# collapse, defeat

=====Inflection=====
{{Template:dothra-ni|root=athohharar|thema=}}

