{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ammat̪ˈt̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[ammattelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ammatt|thema=e}}

