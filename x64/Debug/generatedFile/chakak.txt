{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃaˈkak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[chakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chak|epe=}}

