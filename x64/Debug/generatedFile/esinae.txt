{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈesin̪ae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[esinalat]]''
# third person singular present tense positive conjugation of ''[[esinalat]]''
# second person plural present tense positive conjugation of ''[[esinalat]]''
# third person plural present tense positive conjugation of ''[[esinalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esin|thema=a}}

