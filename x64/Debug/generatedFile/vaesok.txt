{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vaeˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[vaesilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vaes|thema=i}}

