{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[miɾˈges]|lang=doth}}

===Pronoun===
{{head|doth|pronoun}}

# (pronominal version of a preposition, see irge)

