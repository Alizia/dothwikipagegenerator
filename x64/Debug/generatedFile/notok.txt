{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪ɤˈt̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[notat]]''


=====Inflection=====
{{Template:dothra-vCC|root=not|epe=}}

