{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvimat̪t̪eɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[vimatterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vimatter|epe=}}

