{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[hɾakkaˈɾix]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# powerful, quick and accurate sword strike

=====Inflection=====
{{Template:dothra-ni|root=hrakkarikh|thema=}}

