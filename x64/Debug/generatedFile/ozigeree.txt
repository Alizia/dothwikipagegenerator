{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈozigeɾee]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[zigerelat]]''
# third person singular future tense negative conjugation of ''[[zigerelat]]''
# second person plural future tense negative conjugation of ''[[zigerelat]]''
# third person plural future tense negative conjugation of ''[[zigerelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziger|thema=e}}

