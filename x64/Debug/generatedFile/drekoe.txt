{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪ɾekue]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[drekolat]]''
# third person singular present tense positive conjugation of ''[[drekolat]]''
# second person plural present tense positive conjugation of ''[[drekolat]]''
# third person plural present tense positive conjugation of ''[[drekolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=drek|thema=o}}

