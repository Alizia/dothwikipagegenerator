{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oɾisˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[rissat]]''


=====Inflection=====
{{Template:dothra-vCC|root=riss|epe=}}

