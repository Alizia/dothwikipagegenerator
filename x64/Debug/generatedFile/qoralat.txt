{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[qɔɾaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to seize, to hold

=====Inflection=====
{{Template:dothra-vCV|root=qor|thema=a}}

