{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈavɾaθaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[javrathat]]''


=====Inflection=====
{{Template:dothra-vCC|root=javrath|epe=}}

