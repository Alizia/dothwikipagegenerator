{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[movekˈx]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[movekkhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=movekkh|epe=}}

