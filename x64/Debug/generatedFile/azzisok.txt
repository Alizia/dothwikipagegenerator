{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[azziˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[azzisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azzis|epe=}}

