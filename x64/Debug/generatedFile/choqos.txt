{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃoˈqɔs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[choqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=choq|epe=e}}

