{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈan̪iθa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[nithat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nith|epe=}}

