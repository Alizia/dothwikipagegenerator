{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪ava]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

