{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈan̪ava]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

