{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈagan̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[ganat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gan|epe=}}

