{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qɔɾaˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[qorasolat]]''
# first person singular present tense negative conjugation of ''[[qorasolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=qoras|thema=o}}

