{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aveˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[venat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ven|epe=}}

