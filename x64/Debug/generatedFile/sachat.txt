{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[saˈt͡ʃat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to split, to divide

=====Inflection=====
{{Template:dothra-vCC|root=sach|epe=}}

