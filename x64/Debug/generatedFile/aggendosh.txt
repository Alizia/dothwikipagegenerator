{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aggen̪ˈd̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[aggendat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aggend|epe=}}

