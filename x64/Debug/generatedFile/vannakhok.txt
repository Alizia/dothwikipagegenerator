{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[van̪n̪aˈxuk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[annakhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=annakh|epe=}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[annakholat]]''
# first person singular future tense negative conjugation of ''[[annakholat]]''


=====Inflection=====
{{Template:dothra-vVV|root=annakh|thema=o}}

