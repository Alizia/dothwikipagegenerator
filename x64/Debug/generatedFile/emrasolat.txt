{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[emɾasoˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to sack, to raid

=====Inflection=====
{{Template:dothra-vVV|root=emras|thema=o}}

