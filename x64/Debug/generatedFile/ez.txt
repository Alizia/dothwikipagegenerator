{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈez]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[ezat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ez|epe=}}

