{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈt̪ɤɾga]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# stomach

#:Me dothrakhqoyoon torga Khali Bharbo.
#::He was bloodrider to Khal Bharbo.

=====Inflection=====
{{Template:dothra-ni|root=torg|thema=a|class=b}}

===Preposition===
{{head|doth|preposition}}

# under

#:Me dothrakhqoyoon torga Khali Bharbo.
#::He was bloodrider to Khal Bharbo.

