{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ar.ragˈgus]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[arraggat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arragg|epe=e}}

