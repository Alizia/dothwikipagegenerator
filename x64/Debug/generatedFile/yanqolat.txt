{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[jan̪qɔˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to collect, to gather
to gather (some)
# to gather (some)

=====Inflection=====
{{Template:dothra-vCV|root=yanq|thema=o}}

