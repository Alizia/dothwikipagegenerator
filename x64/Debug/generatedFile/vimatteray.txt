{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vimat̪t̪eˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[vimatterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vimatter|epe=}}

