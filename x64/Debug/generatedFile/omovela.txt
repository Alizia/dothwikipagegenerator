{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈomovel̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[movelat]]''


=====Inflection=====
{{Template:dothra-vCC|root=movel|epe=}}

