{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvemɾasoo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[emrasolat]]''
# third person singular future tense negative conjugation of ''[[emrasolat]]''
# second person plural future tense negative conjugation of ''[[emrasolat]]''
# third person plural future tense negative conjugation of ''[[emrasolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=emras|thema=o}}

