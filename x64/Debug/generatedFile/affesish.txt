{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[affeˈsiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[affesat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affes|epe=}}

