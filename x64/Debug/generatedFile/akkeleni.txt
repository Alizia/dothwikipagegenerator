{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈakkel̪en̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[akkelenat]]''
# second person singular present tense positive conjugation of ''[[akkelenat]]''
# second person plural present tense positive conjugation of ''[[akkelenat]]''
# third person plural present tense positive conjugation of ''[[akkelenat]]''
# second person singular present tense negative conjugation of ''[[akkelenat]]''
# second person plural present tense negative conjugation of ''[[akkelenat]]''
# third person plural present tense negative conjugation of ''[[akkelenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkelen|epe=}}

