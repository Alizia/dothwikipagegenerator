{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ziɾisˈseʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[zirisselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziriss|thema=e}}

