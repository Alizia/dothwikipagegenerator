{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈn̪ir.ra]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[nirrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nirr|epe=e}}

