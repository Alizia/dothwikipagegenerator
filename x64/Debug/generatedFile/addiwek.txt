{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪d̪iˈwek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[addiwelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=addiw|thema=e}}

