{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfon̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[fonat]]''
# second person singular present tense positive conjugation of ''[[fonat]]''
# second person plural present tense positive conjugation of ''[[fonat]]''
# third person plural present tense positive conjugation of ''[[fonat]]''
# second person singular present tense negative conjugation of ''[[fonat]]''
# second person plural present tense negative conjugation of ''[[fonat]]''
# third person plural present tense negative conjugation of ''[[fonat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fon|epe=}}

