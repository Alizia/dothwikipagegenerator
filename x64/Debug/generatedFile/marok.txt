{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[maˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[marilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mar|thema=i}}

