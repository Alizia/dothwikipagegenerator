{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈahoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[holat]]''
# third person singular future tense positive conjugation of ''[[holat]]''
# second person plural future tense positive conjugation of ''[[holat]]''
# third person plural future tense positive conjugation of ''[[holat]]''


=====Inflection=====
{{Template:dothra-vCV|root=h|thema=o}}

