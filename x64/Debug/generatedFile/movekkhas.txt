{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[movekˈxas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[movekkhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=movekkh|epe=}}

