{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈziɾejese]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[zireyeselat]]''
# singular past tense positive conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

