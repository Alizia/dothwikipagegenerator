{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ajoˈl̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ayolat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ayol|epe=}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[yolat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yol|epe=}}

