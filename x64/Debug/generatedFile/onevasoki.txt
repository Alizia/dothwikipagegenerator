{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈon̪evasoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[nevasolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nevas|thema=o}}

