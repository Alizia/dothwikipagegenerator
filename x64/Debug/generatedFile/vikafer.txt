{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vikaˈfer]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[vikaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vikafer|epe=}}

