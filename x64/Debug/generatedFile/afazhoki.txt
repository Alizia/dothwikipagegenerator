{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈafaʒoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[afazhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=afazh|epe=}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[afazholat]]''
# first person plural present tense negative conjugation of ''[[afazholat]]''


=====Inflection=====
{{Template:dothra-vVV|root=afazh|thema=o}}

