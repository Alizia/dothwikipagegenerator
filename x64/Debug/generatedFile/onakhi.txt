{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈon̪axi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[nakhat]]''
# second person plural future tense negative conjugation of ''[[nakhat]]''
# third person plural future tense negative conjugation of ''[[nakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nakh|epe=}}

