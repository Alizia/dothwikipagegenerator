{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vat͡ʃɾaˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[vachrarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vachrar|epe=}}

