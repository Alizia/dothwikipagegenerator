{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈofakaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[fakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fak|epe=}}

