{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈol̪an̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[lanat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lan|epe=}}

