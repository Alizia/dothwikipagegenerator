{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈemi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[emat]]''
# second person singular present tense positive conjugation of ''[[emat]]''
# second person plural present tense positive conjugation of ''[[emat]]''
# third person plural present tense positive conjugation of ''[[emat]]''
# second person singular present tense negative conjugation of ''[[emat]]''
# second person plural present tense negative conjugation of ''[[emat]]''
# third person plural present tense negative conjugation of ''[[emat]]''


=====Inflection=====
{{Template:dothra-vVC|root=em|epe=}}

