{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪ɤr.ˈriʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[lorrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lorr|epe=e}}

