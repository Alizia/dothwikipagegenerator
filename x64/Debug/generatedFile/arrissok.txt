{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ar.risˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[arrissat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arriss|epe=}}

