{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈakemaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[kemat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kem|epe=}}

