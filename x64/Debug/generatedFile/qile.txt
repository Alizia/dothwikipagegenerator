{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈqel̪e]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# island

=====Inflection=====
{{Template:dothra-ni|root=qil|thema=e}}

