{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ziɾejeˈsek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

