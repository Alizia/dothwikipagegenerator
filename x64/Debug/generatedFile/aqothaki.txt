{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaqɔθaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[qothat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qoth|epe=}}

