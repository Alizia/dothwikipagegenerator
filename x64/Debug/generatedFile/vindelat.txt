{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[vin̪d̪eˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to stab
to stab at
# to stab at

=====Inflection=====
{{Template:dothra-vCV|root=vind|thema=e}}

