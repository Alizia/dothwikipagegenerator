{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvejel̪io]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[eyelilat]]''
# third person singular future tense negative conjugation of ''[[eyelilat]]''
# second person plural future tense negative conjugation of ''[[eyelilat]]''
# third person plural future tense negative conjugation of ''[[eyelilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=eyel|thema=i}}

