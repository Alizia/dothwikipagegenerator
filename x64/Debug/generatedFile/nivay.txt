{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪iˈvaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[nivat]]''


=====Inflection=====
{{Template:dothra-vCC|root=niv|epe=}}

