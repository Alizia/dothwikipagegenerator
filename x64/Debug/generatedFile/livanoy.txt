{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪ivaˈn̪ɤj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[livanolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=livan|thema=o}}

