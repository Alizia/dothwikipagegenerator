{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aguˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[gorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gor|epe=}}

