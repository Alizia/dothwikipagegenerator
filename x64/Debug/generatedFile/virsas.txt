{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[viɾˈsas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[virsalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=virs|thema=a}}

