{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aθ.θaˈsas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[atthasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=atthas|epe=}}

