{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[saˈiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[sajat]]''


=====Inflection=====
{{Template:dothra-vCC|root=saj|epe=}}

