{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oviqɑfeˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[viqaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=viqafer|epe=}}

