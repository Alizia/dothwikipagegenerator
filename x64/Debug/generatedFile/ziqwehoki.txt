{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈziqwehoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[ziqwehelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziqweh|thema=e}}

