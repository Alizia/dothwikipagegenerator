{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat͡ʃoakaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[choakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=choak|epe=}}

