{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[an̪iˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[nirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nir|epe=}}

