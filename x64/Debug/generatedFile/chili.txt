{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt͡ʃil̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[chilat]]''
# second person singular present tense positive conjugation of ''[[chilat]]''
# second person plural present tense positive conjugation of ''[[chilat]]''
# third person plural present tense positive conjugation of ''[[chilat]]''
# second person singular present tense negative conjugation of ''[[chilat]]''
# second person plural present tense negative conjugation of ''[[chilat]]''
# third person plural present tense negative conjugation of ''[[chilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chil|epe=}}

