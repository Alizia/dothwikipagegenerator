{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθvil̪ˈl̪ar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# wisdom (that is gained from experience or learned)

=====Inflection=====
{{Template:dothra-ni|root=athvillar|thema=}}

