{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[on̪ˈd̪ek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ondelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ond|thema=e}}

