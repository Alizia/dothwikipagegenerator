{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aheʒaˈhak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[hezhahat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hezhah|epe=}}

