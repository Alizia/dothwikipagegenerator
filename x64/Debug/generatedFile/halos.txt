{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈl̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[halelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hal|thema=e}}

