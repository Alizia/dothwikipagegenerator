{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfit͡ʃaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[fichat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fich|epe=}}

