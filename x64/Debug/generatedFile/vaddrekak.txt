{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vad̪d̪ɾeˈkak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[addrekat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addrek|epe=}}

