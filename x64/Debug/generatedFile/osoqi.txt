{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈosoqe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[soqat]]''
# second person plural future tense negative conjugation of ''[[soqat]]''
# third person plural future tense negative conjugation of ''[[soqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=soq|epe=e}}

