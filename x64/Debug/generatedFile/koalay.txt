{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kuaˈl̪aj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[koalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=koal|epe=}}

