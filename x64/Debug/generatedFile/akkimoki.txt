{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈakkimoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[akkimilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=akkim|thema=i}}

