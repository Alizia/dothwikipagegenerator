{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[kaɾl̪iˈn̪ak]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# light horse

=====Inflection=====
{{Template:dothra-na|root=karlinak|thema=}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[karlinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=karlin|epe=}}

