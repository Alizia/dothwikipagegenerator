{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈogi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ogat]]''
# second person singular present tense positive conjugation of ''[[ogat]]''
# second person plural present tense positive conjugation of ''[[ogat]]''
# third person plural present tense positive conjugation of ''[[ogat]]''
# second person singular present tense negative conjugation of ''[[ogat]]''
# second person plural present tense negative conjugation of ''[[ogat]]''
# third person plural present tense negative conjugation of ''[[ogat]]''


=====Inflection=====
{{Template:dothra-vVC|root=og|epe=e}}

