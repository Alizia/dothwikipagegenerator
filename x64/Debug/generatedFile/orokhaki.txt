{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoɾoxaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[rokhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rokh|epe=}}

