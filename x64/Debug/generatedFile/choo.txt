{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈt͡ʃoo]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# pika

=====Inflection=====
{{Template:dothra-na|root=cho|thema=o}}

