{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈahakee]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[hakelat]]''
# third person singular future tense positive conjugation of ''[[hakelat]]''
# second person plural future tense positive conjugation of ''[[hakelat]]''
# third person plural future tense positive conjugation of ''[[hakelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hak|thema=e}}

