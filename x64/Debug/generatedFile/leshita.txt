{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪eʃit̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[leshitat]]''


=====Inflection=====
{{Template:dothra-vCC|root=leshit|epe=}}

