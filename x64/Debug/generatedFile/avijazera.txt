{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaviazeɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[vijazerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vijazer|epe=}}

