{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[t͡ʃaˈfak]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# mercenary, turncloak, traitor

=====Inflection=====
{{Template:dothra-na|root=chafak|thema=}}

