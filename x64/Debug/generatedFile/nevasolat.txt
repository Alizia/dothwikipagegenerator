{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[n̪evasoˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to sit down

=====Inflection=====
{{Template:dothra-vCV|root=nevas|thema=o}}

