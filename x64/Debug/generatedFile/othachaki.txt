{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoθat͡ʃaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[thachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=thach|epe=}}

