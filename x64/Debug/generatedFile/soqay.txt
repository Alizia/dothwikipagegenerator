{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[soˈqɑj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[soqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=soq|epe=e}}

