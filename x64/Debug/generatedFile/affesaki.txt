{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaffesaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[affesat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affes|epe=}}

