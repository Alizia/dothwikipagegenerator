{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈsaqɔjaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[saqoyalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=saqoy|thema=a}}

