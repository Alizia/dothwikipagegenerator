{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈvin̪t̪e]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# portion of meat

=====Inflection=====
{{Template:dothra-ni|root=vint|thema=e}}

