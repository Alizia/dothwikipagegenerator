{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪ɤn̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[donat]]''


=====Inflection=====
{{Template:dothra-vCC|root=don|epe=}}

