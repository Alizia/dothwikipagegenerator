{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈohasaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[hasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=has|epe=}}

