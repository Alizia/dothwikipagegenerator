{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[hoeˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[hoerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hoer|epe=}}

