{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈovid̪ɾogeɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[vidrogerat]]''
# second person plural future tense negative conjugation of ''[[vidrogerat]]''
# third person plural future tense negative conjugation of ''[[vidrogerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vidroger|epe=}}

