{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvazzohoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[azzohat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azzoh|epe=}}

