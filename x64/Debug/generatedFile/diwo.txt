{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪iwo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[diwelat]]''
# singular past tense negative conjugation of ''[[diwelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=diw|thema=e}}

