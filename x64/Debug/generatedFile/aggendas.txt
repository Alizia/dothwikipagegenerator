{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aggen̪ˈd̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[aggendat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aggend|epe=}}

