{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaqɑfi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[qafat]]''
# second person plural future tense positive conjugation of ''[[qafat]]''
# third person plural future tense positive conjugation of ''[[qafat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qaf|epe=}}

