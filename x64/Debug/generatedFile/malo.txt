{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmal̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[malilat]]''
# singular past tense negative conjugation of ''[[malilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mal|thema=i}}

