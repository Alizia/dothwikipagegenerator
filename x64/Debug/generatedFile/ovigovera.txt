{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoviguveɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[vigoverat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vigover|epe=}}

