{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vassamˈvak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[assamvat]]''


=====Inflection=====
{{Template:dothra-vVC|root=assamv|epe=}}

