{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪avaxaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

