{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[an̪n̪eˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[annevalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=annev|thema=a}}

