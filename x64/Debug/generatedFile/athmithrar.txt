{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθmiθˈɾar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# rest

=====Inflection=====
{{Template:dothra-ni|root=athmithrar|thema=}}

