{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaɾan̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[aranat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aran|epe=}}

