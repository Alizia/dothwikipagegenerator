{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪aθoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[lathat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lath|epe=}}

