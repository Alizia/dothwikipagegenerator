{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈomesa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[mesat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mes|epe=}}

