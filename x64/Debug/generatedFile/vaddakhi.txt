{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvad̪d̪axi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[addakhat]]''
# second person plural future tense positive conjugation of ''[[addakhat]]''
# third person plural future tense positive conjugation of ''[[addakhat]]''
# second person singular future tense negative conjugation of ''[[addakhat]]''
# second person plural future tense negative conjugation of ''[[addakhat]]''
# third person plural future tense negative conjugation of ''[[addakhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addakh|epe=}}

