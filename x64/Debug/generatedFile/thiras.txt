{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[θiˈɾas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[thirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=thir|epe=}}

