{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfil̪kaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[filkat]]''


=====Inflection=====
{{Template:dothra-vCC|root=filk|epe=}}

