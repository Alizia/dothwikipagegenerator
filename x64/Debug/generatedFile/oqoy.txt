{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈqɔj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[oqolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=oq|thema=o}}

