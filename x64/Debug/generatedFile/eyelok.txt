{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ejeˈl̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[eyelilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=eyel|thema=i}}

