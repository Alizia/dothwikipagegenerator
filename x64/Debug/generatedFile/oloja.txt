{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈol̪ɤa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[lojat]]''


=====Inflection=====
{{Template:dothra-vCC|root=loj|epe=}}

