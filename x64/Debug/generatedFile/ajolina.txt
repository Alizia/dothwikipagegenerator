{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaol̪in̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[jolinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jolin|epe=}}

