{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvammen̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[ammenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammen|epe=}}

