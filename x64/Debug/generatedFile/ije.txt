{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈie]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ijelat]]''
# singular past tense positive conjugation of ''[[ijelat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ij|thema=e}}

