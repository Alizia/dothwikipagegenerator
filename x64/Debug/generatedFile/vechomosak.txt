{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vet͡ʃomoˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[echomosalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=echomos|thema=a}}

