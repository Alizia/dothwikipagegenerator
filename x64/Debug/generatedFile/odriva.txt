{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈod̪ɾiva]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[drivat]]''


=====Inflection=====
{{Template:dothra-vCC|root=driv|epe=}}

