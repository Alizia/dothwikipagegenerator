{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈkesseki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[kesselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=kess|thema=e}}

