{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[emˈmiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[emmat]]''


=====Inflection=====
{{Template:dothra-vVC|root=emm|epe=}}

