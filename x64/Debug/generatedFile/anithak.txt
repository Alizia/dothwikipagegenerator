{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[an̪iˈθak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[nithat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nith|epe=}}

