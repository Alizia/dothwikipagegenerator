{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kafˈfaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[kaffat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kaff|epe=}}

