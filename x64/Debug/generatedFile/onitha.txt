{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈon̪iθa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[nithat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nith|epe=}}

