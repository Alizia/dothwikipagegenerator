{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈavit͡ʃit̪eɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[vichiterat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vichiter|epe=}}

