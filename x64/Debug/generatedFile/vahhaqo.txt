{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaħ.haqɔ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[ahhaqat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhaq|epe=e}}

