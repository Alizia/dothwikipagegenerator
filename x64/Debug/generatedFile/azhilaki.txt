{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʒil̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[zhilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zhil|epe=}}

