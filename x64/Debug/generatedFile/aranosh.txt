{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aɾaˈn̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[aranat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aran|epe=}}

