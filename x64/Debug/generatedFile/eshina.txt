{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈeʃin̪a]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# fish

=====Inflection=====
{{Template:dothra-ni|root=eshin|thema=a}}

