{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪avaˈxus]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

