{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈʒokwoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[zhokwalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zhokw|thema=a}}

