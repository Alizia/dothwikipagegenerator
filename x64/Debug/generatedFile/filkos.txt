{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[fil̪ˈkus]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[filkat]]''


=====Inflection=====
{{Template:dothra-vCC|root=filk|epe=}}

