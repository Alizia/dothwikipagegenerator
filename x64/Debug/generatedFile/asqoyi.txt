{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈasqɔji]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# oath, vow

=====Inflection=====
{{Template:dothra-ni|root=asqoy|thema=i}}

