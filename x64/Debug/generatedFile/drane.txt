{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈd̪ɾan̪e]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# mother who is still breast feeding

=====Inflection=====
{{Template:dothra-na|root=dran|thema=e}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[dranelat]]''
# singular past tense positive conjugation of ''[[dranelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=dran|thema=e}}

