{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈasa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[jasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jas|epe=}}

