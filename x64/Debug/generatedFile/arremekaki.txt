{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈar.remekaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[arremekat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arremek|epe=}}

