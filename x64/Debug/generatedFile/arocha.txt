{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaɾot͡ʃa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[rochat]]''


=====Inflection=====
{{Template:dothra-vCC|root=roch|epe=}}

