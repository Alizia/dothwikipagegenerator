{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθeʒiˈɾar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# dancing

=====Inflection=====
{{Template:dothra-ni|root=athezhirar|thema=}}

