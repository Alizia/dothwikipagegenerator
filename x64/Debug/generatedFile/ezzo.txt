{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈezzo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ezzolat]]''
# singular past tense positive conjugation of ''[[ezzolat]]''
# formal negative imperative conjugation of ''[[ezzolat]]''
# singular past tense negative conjugation of ''[[ezzolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ezz|thema=o}}

