{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[guˈʃas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[goshat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gosh|epe=}}

