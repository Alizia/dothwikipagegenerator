{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[mat̪t̪eˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to limp

=====Inflection=====
{{Template:dothra-vCV|root=matt|thema=e}}

