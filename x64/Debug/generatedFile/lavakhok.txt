{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪avaˈxuk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

