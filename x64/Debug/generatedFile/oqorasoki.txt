{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoqɔɾasoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[qorasolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=qoras|thema=o}}

