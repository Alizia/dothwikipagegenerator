{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat͡ʃoqɑki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[choqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=choq|epe=e}}

