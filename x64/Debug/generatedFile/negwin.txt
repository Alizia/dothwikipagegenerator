{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[n̪eˈgwin̪]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# stone

=====Inflection=====
{{Template:dothra-ni|root=negwin|thema=}}

