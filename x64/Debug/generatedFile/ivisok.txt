{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[iviˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[ivisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ivis|epe=}}

