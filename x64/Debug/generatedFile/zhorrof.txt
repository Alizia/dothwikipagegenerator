{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ʒor.ˈrof]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# fig (fresh fruit of a fig tree)

=====Inflection=====
{{Template:dothra-ni|root=zhorrof|thema=}}

