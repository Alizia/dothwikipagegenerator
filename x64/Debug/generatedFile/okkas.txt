{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[okˈkas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[okkat]]''


=====Inflection=====
{{Template:dothra-vVC|root=okk|epe=}}

