{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[affeˈn̪iʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[affenat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affen|epe=}}

