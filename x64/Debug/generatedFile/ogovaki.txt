{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoguvaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[govat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gov|epe=}}

