{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈammemo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[ammemat]]''
# singular past tense negative conjugation of ''[[ammemat]]''
# third person singular present tense negative conjugation of ''[[ammemat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammem|epe=}}

