{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[θimaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to leak

=====Inflection=====
{{Template:dothra-vCV|root=thim|thema=a}}

