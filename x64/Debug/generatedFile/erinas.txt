{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eɾiˈn̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[erinat]]''


=====Inflection=====
{{Template:dothra-vVC|root=erin|epe=}}

