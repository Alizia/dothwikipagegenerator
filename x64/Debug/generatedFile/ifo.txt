{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈifo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[ifat]]''
# singular past tense negative conjugation of ''[[ifat]]''
# third person singular present tense negative conjugation of ''[[ifat]]''


=====Inflection=====
{{Template:dothra-vVC|root=if|epe=}}

