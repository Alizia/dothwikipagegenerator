{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[eˈfat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to squeeze

=====Inflection=====
{{Template:dothra-vVC|root=ef|epe=}}

