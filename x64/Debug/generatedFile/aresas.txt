{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aɾeˈsas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[aresat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ares|epe=}}

