{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfen̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[fenat]]''
# second person singular present tense positive conjugation of ''[[fenat]]''
# second person plural present tense positive conjugation of ''[[fenat]]''
# third person plural present tense positive conjugation of ''[[fenat]]''
# second person singular present tense negative conjugation of ''[[fenat]]''
# second person plural present tense negative conjugation of ''[[fenat]]''
# third person plural present tense negative conjugation of ''[[fenat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fen|epe=}}

