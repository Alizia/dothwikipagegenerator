{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[agˈgen̪d̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[aggendat]]''
# singular past tense negative conjugation of ''[[aggendat]]''
# third person singular present tense negative conjugation of ''[[aggendat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aggend|epe=}}

