{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪ajafoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[layafat]]''


=====Inflection=====
{{Template:dothra-vCC|root=layaf|epe=}}

