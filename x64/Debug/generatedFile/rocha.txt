{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrot͡ʃa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[rochat]]''


=====Inflection=====
{{Template:dothra-vCC|root=roch|epe=}}

