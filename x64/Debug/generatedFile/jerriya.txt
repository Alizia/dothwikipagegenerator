{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈer.rija]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# a discussion

=====Inflection=====
{{Template:dothra-ni|root=jerriy|thema=a}}

