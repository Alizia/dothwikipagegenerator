{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈl̪ir]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# tattoo

=====Inflection=====
{{Template:dothra-ni|root=lir|thema=}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[lirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lir|epe=}}

