{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[id̪d̪eˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to welcome, to make someone drink

=====Inflection=====
{{Template:dothra-vVV|root=idd|thema=e}}

