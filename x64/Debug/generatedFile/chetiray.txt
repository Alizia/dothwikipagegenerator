{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃet̪iˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[chetirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chetir|epe=}}

