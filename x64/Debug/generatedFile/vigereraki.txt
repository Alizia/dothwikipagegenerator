{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvigeɾeɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[vigererat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vigerer|epe=}}

