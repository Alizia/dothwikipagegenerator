{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈot̪iha]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[tihat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tih|epe=}}

