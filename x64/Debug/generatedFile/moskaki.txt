{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmoskaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[moskat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mosk|epe=}}

