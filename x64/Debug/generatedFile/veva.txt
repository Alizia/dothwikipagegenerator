{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈveva]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[evat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ev|epe=}}

