{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ziɾejeˈsoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

