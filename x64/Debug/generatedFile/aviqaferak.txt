{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aviqɑfeˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[viqaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=viqafer|epe=}}

