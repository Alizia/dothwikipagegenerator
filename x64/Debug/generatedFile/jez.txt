{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈez]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[jezat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jez|epe=}}

