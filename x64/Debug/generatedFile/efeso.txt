{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈefeso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[efesalat]]''
# singular past tense negative conjugation of ''[[efesalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efes|thema=a}}

