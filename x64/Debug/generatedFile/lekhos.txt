{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪eˈxus]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[lekhilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=lekh|thema=i}}

