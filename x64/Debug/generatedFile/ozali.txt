{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈozal̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[zalat]]''
# second person plural future tense negative conjugation of ''[[zalat]]''
# third person plural future tense negative conjugation of ''[[zalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zal|epe=}}

