{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[asaqɔˈjak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[saqoyalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=saqoy|thema=a}}

