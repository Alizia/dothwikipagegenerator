{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ol̪iˈwak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[liwalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=liw|thema=a}}

