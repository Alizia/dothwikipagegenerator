{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvammemo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[ammemat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammem|epe=}}

