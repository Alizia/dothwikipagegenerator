{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[remeˈkuʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[remekat]]''


=====Inflection=====
{{Template:dothra-vCC|root=remek|epe=}}

