{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ol̪ˈd̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[oldat]]''


=====Inflection=====
{{Template:dothra-vVC|root=old|epe=}}

