{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[exugaˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[ekhogaralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ekhogar|thema=a}}

