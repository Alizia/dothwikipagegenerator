{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[maˈd̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[madat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mad|epe=}}

