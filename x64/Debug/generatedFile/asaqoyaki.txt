{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈasaqɔjaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[saqoyalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=saqoy|thema=a}}

