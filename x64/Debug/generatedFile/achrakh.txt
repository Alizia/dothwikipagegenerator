{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aˈt͡ʃɾax]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# smell
# stink
# stink

=====Inflection=====
{{Template:dothra-ni|root=achrakh|thema=}}

