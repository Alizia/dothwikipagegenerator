{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eˈzos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[jezat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jez|epe=}}

