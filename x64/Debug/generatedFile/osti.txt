{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈosti]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ostat]]''
# second person singular present tense positive conjugation of ''[[ostat]]''
# second person plural present tense positive conjugation of ''[[ostat]]''
# third person plural present tense positive conjugation of ''[[ostat]]''
# second person singular present tense negative conjugation of ''[[ostat]]''
# second person plural present tense negative conjugation of ''[[ostat]]''
# third person plural present tense negative conjugation of ''[[ostat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ost|epe=}}

