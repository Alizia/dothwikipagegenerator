{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvakkuvaɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[akkovarat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkovar|epe=}}

