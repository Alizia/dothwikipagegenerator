{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[akkiˈmiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[akkimilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=akkim|thema=i}}

