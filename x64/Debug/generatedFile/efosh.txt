{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eˈfoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[efat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ef|epe=}}

