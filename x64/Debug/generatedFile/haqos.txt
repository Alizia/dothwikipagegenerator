{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈqɔs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[haqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=haq|epe=e}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[haqolat]]''
# informal negative imperative conjugation of ''[[haqolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=haq|thema=o}}

