{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vasˈsamvi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[assamvat]]''
# second person plural future tense positive conjugation of ''[[assamvat]]''
# third person plural future tense positive conjugation of ''[[assamvat]]''
# second person singular future tense negative conjugation of ''[[assamvat]]''
# second person plural future tense negative conjugation of ''[[assamvat]]''
# third person plural future tense negative conjugation of ''[[assamvat]]''


=====Inflection=====
{{Template:dothra-vVC|root=assamv|epe=}}

