{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈafit͡ʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[fichat]]''
# second person plural future tense positive conjugation of ''[[fichat]]''
# third person plural future tense positive conjugation of ''[[fichat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fich|epe=}}

