{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈovel̪t̪ɤɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[veltorolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=veltor|thema=o}}

