{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈn̪emo siˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

# to wander off

#:Kisha nemo silish Jereseroon Jima majin leisosh.
#::We wandered off from the Western Market and got lost.

=====Inflection=====
{{Template:dothra-vCC|root=sil|epe=}}

