{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃoˈqɑk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[choqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=choq|epe=e}}

