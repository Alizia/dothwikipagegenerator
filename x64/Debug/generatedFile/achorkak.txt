{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[at͡ʃoɾˈkak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[chorkat]]''


=====Inflection=====
{{Template:dothra-vCC|root=chork|epe=}}

