{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈosaa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[sajat]]''


=====Inflection=====
{{Template:dothra-vCC|root=saj|epe=}}

