{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈosiki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[silat]]''


=====Inflection=====
{{Template:dothra-vCV|root=s|thema=i}}

