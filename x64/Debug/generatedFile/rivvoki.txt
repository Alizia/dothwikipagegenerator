{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈrivvoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[rivvat]]''


=====Inflection=====
{{Template:dothra-vCC|root=rivv|epe=}}

