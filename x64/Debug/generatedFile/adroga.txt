{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪ɾoga]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[drogat]]''


=====Inflection=====
{{Template:dothra-vCC|root=drog|epe=e}}

