{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[elˈziʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[elzat]]''


=====Inflection=====
{{Template:dothra-vVC|root=elz|epe=}}

