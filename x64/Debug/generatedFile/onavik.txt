{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[on̪aˈvik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[navilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nav|thema=i}}

