{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[eɾakaˈsar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# trader's guild, "company"

=====Inflection=====
{{Template:dothra-na|root=jerakasar|thema=}}

