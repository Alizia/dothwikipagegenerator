{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[fiˈɾix]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# ring

=====Inflection=====
{{Template:dothra-ni|root=firikh|thema=}}

