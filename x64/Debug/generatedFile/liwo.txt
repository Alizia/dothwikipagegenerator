{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪iwo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[liwalat]]''
# singular past tense negative conjugation of ''[[liwalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=liw|thema=a}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# knot
# link
# link

=====Inflection=====
{{Template:dothra-ni|root=liw|thema=o|class=b}}

