{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oziɾejeˈsek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

