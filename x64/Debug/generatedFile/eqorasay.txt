{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eqɔɾaˈsaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[eqorasalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=eqoras|thema=a}}

