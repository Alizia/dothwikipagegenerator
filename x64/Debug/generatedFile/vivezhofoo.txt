{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈviveʒofoo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[ivezhofolat]]''
# third person singular future tense negative conjugation of ''[[ivezhofolat]]''
# second person plural future tense negative conjugation of ''[[ivezhofolat]]''
# third person plural future tense negative conjugation of ''[[ivezhofolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ivezhof|thema=o}}

