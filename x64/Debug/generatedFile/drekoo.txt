{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪ɾekuo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[drekolat]]''
# third person singular present tense negative conjugation of ''[[drekolat]]''
# second person plural present tense negative conjugation of ''[[drekolat]]''
# third person plural present tense negative conjugation of ''[[drekolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=drek|thema=o}}

