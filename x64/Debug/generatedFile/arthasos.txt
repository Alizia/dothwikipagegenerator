{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aɾθaˈsos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[arthasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arthas|epe=}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[arthasolat]]''
# informal negative imperative conjugation of ''[[arthasolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=arthas|thema=o}}

