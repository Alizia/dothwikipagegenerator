{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaɾan̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[aranat]]''


=====Inflection=====
{{Template:dothra-vVC|root=aran|epe=}}

