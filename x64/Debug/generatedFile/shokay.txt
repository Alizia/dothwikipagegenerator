{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʃoˈkaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[shokat]]''


=====Inflection=====
{{Template:dothra-vCC|root=shok|epe=}}

