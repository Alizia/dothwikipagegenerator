{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈragguki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[raggat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ragg|epe=e}}

