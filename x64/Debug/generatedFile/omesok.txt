{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[omeˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[mesolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mes|thema=o}}

