{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[efit͡ʃisaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to disagree

=====Inflection=====
{{Template:dothra-vVV|root=efichis|thema=a}}

