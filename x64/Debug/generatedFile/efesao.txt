{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈefesao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[efesalat]]''
# third person singular present tense negative conjugation of ''[[efesalat]]''
# second person plural present tense negative conjugation of ''[[efesalat]]''
# third person plural present tense negative conjugation of ''[[efesalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efes|thema=a}}

