{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[jasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jas|epe=}}

