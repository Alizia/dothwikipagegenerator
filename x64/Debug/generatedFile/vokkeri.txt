{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvokkeɾi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[vokkerat]]''
# second person singular present tense positive conjugation of ''[[vokkerat]]''
# second person plural present tense positive conjugation of ''[[vokkerat]]''
# third person plural present tense positive conjugation of ''[[vokkerat]]''
# second person singular present tense negative conjugation of ''[[vokkerat]]''
# second person plural present tense negative conjugation of ''[[vokkerat]]''
# third person plural present tense negative conjugation of ''[[vokkerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vokker|epe=}}

