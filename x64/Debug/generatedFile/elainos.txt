{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[el̪aiˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[elainat]]''


=====Inflection=====
{{Template:dothra-vVC|root=elain|epe=}}

