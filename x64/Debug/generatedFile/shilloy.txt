{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʃil̪ˈl̪ɤj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[shillolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=shill|thema=o}}

