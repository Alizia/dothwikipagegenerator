{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪aaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[lajat]]''


=====Inflection=====
{{Template:dothra-vCC|root=laj|epe=}}

