{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪aˈvaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

