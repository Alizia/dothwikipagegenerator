{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈziɾisseo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[zirisselat]]''
# third person singular present tense negative conjugation of ''[[zirisselat]]''
# second person plural present tense negative conjugation of ''[[zirisselat]]''
# third person plural present tense negative conjugation of ''[[zirisselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziriss|thema=e}}

