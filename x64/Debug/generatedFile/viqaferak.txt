{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[viqɑfeˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[viqaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=viqafer|epe=}}

