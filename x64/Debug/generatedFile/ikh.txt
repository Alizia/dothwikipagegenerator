{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈix]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# ashes

=====Inflection=====
{{Template:dothra-ni|root=ikh|thema=}}

