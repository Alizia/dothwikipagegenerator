{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈazijen̪eki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[ziyenelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziyen|thema=e}}

