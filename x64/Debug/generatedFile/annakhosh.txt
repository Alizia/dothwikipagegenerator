{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[an̪n̪aˈxuʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[annakhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=annakh|epe=}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[annakholat]]''
# plural past tense negative conjugation of ''[[annakholat]]''


=====Inflection=====
{{Template:dothra-vVV|root=annakh|thema=o}}

