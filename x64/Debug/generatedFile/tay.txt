{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪aj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[tat]]''


=====Inflection=====
{{Template:dothra-vCC|root=t|epe=}}

