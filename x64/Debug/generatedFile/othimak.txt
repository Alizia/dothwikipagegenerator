{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oθiˈmak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[thimalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=thim|thema=a}}

