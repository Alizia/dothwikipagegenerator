{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kaˈʃiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[kashat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kash|epe=}}

