{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[heʒaˈhoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[hezhahat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hezhah|epe=}}

