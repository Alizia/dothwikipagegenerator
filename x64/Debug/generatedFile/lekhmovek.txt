{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[l̪exmoˈvek]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# conlanger

=====Inflection=====
{{Template:dothra-na|root=lekhmovek|thema=}}

