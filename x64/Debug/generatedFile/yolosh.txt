{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[joˈl̪ɤʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[yolat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yol|epe=}}

