{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[θiˈɾat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to live

=====Inflection=====
{{Template:dothra-vCC|root=thir|epe=}}

