{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʒil̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[zhilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zhil|epe=}}

