{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[efit͡ʃiˈsos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[efichisalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efichis|thema=a}}

