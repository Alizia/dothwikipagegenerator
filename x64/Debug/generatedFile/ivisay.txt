{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[iviˈsaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[ivisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ivis|epe=}}

