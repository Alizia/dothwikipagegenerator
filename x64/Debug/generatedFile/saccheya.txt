{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈsat.t͡ʃeja]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# category, part, piece, quarter

=====Inflection=====
{{Template:dothra-ni|root=sacchey|thema=a}}

