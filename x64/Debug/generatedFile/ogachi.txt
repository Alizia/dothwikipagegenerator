{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈogat͡ʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[gachat]]''
# second person plural future tense negative conjugation of ''[[gachat]]''
# third person plural future tense negative conjugation of ''[[gachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gach|epe=}}

