{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪aqeso]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[naqisat]]''
# singular past tense negative conjugation of ''[[naqisat]]''
# third person singular present tense negative conjugation of ''[[naqisat]]''


=====Inflection=====
{{Template:dothra-vCC|root=naqis|epe=}}

