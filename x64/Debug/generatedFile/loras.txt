{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪ɤˈɾas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[lorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lor|epe=}}

