{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[veˈn̪ɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[venat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ven|epe=}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[venolat]]''
# informal negative imperative conjugation of ''[[venolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ven|thema=o}}

