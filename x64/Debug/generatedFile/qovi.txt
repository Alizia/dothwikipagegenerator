{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈqɔvi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[qovat]]''
# second person singular present tense positive conjugation of ''[[qovat]]''
# second person plural present tense positive conjugation of ''[[qovat]]''
# third person plural present tense positive conjugation of ''[[qovat]]''
# second person singular present tense negative conjugation of ''[[qovat]]''
# second person plural present tense negative conjugation of ''[[qovat]]''
# third person plural present tense negative conjugation of ''[[qovat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qov|epe=}}

