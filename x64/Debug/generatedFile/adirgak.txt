{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪iɾˈgak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[dirgat]]''


=====Inflection=====
{{Template:dothra-vCC|root=dirg|epe=e}}

