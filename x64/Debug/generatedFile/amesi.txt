{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈamesi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[mesat]]''
# second person plural future tense positive conjugation of ''[[mesat]]''
# third person plural future tense positive conjugation of ''[[mesat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mes|epe=}}

