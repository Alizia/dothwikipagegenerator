{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[afˈfin̪]|lang=doth}}

===Adverb===
{{head|doth|adverb}}

# when

#:Anha vazhak maan firikhnharen hoshora ma mahrazhi aqovi affin mori atihi mae.
#::I will give to him a golden crown and men will tremble when they will see it.

