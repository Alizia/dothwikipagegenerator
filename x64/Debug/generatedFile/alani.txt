{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈal̪an̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[lanat]]''
# second person plural future tense positive conjugation of ''[[lanat]]''
# third person plural future tense positive conjugation of ''[[lanat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lan|epe=}}

