{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kesˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[kesselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=kess|thema=e}}

