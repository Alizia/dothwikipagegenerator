{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmai]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[majilat]]''
# singular past tense positive conjugation of ''[[majilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=maj|thema=i}}

