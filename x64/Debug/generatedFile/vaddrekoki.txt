{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvad̪d̪ɾekuki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[addrekat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addrek|epe=}}

