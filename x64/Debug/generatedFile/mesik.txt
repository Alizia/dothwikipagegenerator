{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[meˈsik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[mesilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=mes|thema=i}}

