{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvit̪ihiɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[vitihirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitihir|epe=}}

