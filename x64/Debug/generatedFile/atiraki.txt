{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈat̪iɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[tirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=tir|epe=}}

