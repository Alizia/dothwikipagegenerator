{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ivviˈsiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[ivvisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ivvis|epe=}}

