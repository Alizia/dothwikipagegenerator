{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vad̪aˈxer]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[vadakherat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vadakher|epe=}}

