{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aexˈt̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[jekhtat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jekht|epe=}}

