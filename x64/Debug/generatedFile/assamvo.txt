{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[asˈsamvo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[assamvat]]''
# singular past tense negative conjugation of ''[[assamvat]]''
# third person singular present tense negative conjugation of ''[[assamvat]]''


=====Inflection=====
{{Template:dothra-vVC|root=assamv|epe=}}

