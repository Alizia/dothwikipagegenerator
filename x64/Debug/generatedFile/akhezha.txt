{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaxeʒa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[khezhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=khezh|epe=}}

