{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[feˈos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[fejat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fej|epe=}}

