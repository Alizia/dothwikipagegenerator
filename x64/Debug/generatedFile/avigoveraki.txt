{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaviguveɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[vigoverat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vigover|epe=}}

