{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈod̪aɾin̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[darinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=darin|epe=}}

