{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈovikafeɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[vikaferat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vikafer|epe=}}

