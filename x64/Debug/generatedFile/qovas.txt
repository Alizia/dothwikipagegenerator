{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[qɔˈvas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[qovat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qov|epe=}}

