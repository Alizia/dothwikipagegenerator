{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ʒeaˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[zheanalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zhean|thema=a}}

