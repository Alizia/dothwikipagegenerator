{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈmiθɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[mithrat]]''
# singular past tense negative conjugation of ''[[mithrat]]''
# third person singular present tense negative conjugation of ''[[mithrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mithr|epe=e}}

