{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈmoski]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[moskat]]''
# second person plural future tense negative conjugation of ''[[moskat]]''
# third person plural future tense negative conjugation of ''[[moskat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mosk|epe=}}

