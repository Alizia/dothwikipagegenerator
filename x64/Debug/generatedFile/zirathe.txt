{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈziɾaθe]|lang=doth}}

====Adjective====
{{doth-adj}}

# beat up, ragged
