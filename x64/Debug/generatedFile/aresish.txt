{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aɾeˈsiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[aresat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ares|epe=}}

