{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪akˈxaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[lakkhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lakkh|epe=}}

