{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈohaoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[hajolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=haj|thema=o}}

