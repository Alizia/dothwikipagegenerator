{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈan̪aqesi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[naqisat]]''
# second person plural future tense positive conjugation of ''[[naqisat]]''
# third person plural future tense positive conjugation of ''[[naqisat]]''


=====Inflection=====
{{Template:dothra-vCC|root=naqis|epe=}}

