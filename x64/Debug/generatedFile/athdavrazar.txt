{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθd̪avɾaˈzar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# usefulness (also means good or excellent, when used as an exclamation)

=====Inflection=====
{{Template:dothra-ni|root=athdavrazar|thema=}}

