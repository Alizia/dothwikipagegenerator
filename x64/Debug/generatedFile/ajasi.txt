{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaasi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[jasat]]''
# second person plural future tense positive conjugation of ''[[jasat]]''
# third person plural future tense positive conjugation of ''[[jasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jas|epe=}}

