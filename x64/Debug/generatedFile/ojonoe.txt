{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoon̪ɤe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[jonolat]]''
# third person singular future tense negative conjugation of ''[[jonolat]]''
# second person plural future tense negative conjugation of ''[[jonolat]]''
# third person plural future tense negative conjugation of ''[[jonolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=jon|thema=o}}

