{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoasi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[jasat]]''
# second person plural future tense negative conjugation of ''[[jasat]]''
# third person plural future tense negative conjugation of ''[[jasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jas|epe=}}

