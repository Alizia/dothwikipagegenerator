{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[on̪ɤkit̪ˈt̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[nokittat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nokitt|epe=}}

