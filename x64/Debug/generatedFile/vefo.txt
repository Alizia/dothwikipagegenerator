{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvefo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[efat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ef|epe=}}

