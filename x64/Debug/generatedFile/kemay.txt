{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[keˈmaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[kemat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kem|epe=}}

