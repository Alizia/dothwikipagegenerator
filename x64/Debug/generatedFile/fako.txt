{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfaku]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[fakat]]''
# singular past tense negative conjugation of ''[[fakat]]''
# third person singular present tense negative conjugation of ''[[fakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fak|epe=}}

