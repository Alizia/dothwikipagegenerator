{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈkuvaɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[kovarat]]''
# singular past tense negative conjugation of ''[[kovarat]]''
# third person singular present tense negative conjugation of ''[[kovarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kovar|epe=}}

