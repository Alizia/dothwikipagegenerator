{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oˈguk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[ogat]]''


=====Inflection=====
{{Template:dothra-vVC|root=og|epe=e}}

