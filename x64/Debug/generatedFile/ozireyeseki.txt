{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoziɾejeseki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[zireyeselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zireyes|thema=e}}

