{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ovin̪ˈd̪ek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[vindelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=vind|thema=e}}

