{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪ir.ˈraj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[nirrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nirr|epe=e}}

