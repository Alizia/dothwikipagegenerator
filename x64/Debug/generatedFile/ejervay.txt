{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eeɾˈvaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[ejervalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ejerv|thema=a}}

