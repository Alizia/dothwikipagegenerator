{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈsik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[silat]]''


=====Inflection=====
{{Template:dothra-vCV|root=s|thema=i}}

