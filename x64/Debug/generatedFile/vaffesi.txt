{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaffesi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[affesat]]''
# second person plural future tense positive conjugation of ''[[affesat]]''
# third person plural future tense positive conjugation of ''[[affesat]]''
# second person singular future tense negative conjugation of ''[[affesat]]''
# second person plural future tense negative conjugation of ''[[affesat]]''
# third person plural future tense negative conjugation of ''[[affesat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affes|epe=}}

