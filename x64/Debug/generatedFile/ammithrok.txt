{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ammiθˈɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[ammithrat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ammithr|epe=e}}

