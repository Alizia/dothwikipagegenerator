{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈl̪exi]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# taste

#:At lekhi ma shafka ahakee yal hakesoon anni.
#::One taste and you'll name your first child after me.

=====Inflection=====
{{Template:dothra-ni|root=lekh|thema=i}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[lekhilat]]''
# singular past tense positive conjugation of ''[[lekhilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=lekh|thema=i}}

