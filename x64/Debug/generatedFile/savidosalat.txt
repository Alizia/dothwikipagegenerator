{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[savid̪ɤsaˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to be protected

=====Inflection=====
{{Template:dothra-vCV|root=savidos|thema=a}}

