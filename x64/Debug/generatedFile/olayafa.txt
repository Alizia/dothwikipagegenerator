{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈol̪ajafa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[layafat]]''


=====Inflection=====
{{Template:dothra-vCC|root=layaf|epe=}}

