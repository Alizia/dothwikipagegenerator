{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈhaqɑki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[haqat]]''


=====Inflection=====
{{Template:dothra-vCC|root=haq|epe=e}}

