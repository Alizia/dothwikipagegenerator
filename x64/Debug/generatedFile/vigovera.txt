{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈviguveɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[vigoverat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vigover|epe=}}

