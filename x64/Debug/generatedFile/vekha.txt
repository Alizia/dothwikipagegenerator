{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvexa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[vekhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vekh|epe=}}

