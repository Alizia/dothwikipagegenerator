{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[soˈɾoħ]|lang=doth}}

===Interjection===
{{head|doth|interjection}}

# halt! (horse command)

