{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[asˈtik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[astilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ast|thema=i}}

