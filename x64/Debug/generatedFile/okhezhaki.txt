{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoxeʒaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[khezhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=khezh|epe=}}

