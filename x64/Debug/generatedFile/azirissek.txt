{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aziɾisˈsek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[zirisselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziriss|thema=e}}

