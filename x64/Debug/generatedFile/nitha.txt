{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪iθa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[nithat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nith|epe=}}

