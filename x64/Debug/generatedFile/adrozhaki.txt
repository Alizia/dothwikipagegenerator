{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪ɾoʒaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[drozhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=drozh|epe=}}

