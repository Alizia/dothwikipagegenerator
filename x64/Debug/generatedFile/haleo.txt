{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈhal̪eo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[halelat]]''
# third person singular present tense negative conjugation of ''[[halelat]]''
# second person plural present tense negative conjugation of ''[[halelat]]''
# third person plural present tense negative conjugation of ''[[halelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hal|thema=e}}

