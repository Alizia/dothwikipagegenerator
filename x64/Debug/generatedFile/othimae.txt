{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoθimae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[thimalat]]''
# third person singular future tense negative conjugation of ''[[thimalat]]''
# second person plural future tense negative conjugation of ''[[thimalat]]''
# third person plural future tense negative conjugation of ''[[thimalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=thim|thema=a}}

