{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ziɾoqɔˈsok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[ziroqoselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziroqos|thema=e}}

