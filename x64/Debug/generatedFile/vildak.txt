{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vil̪ˈd̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[ildat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ild|epe=}}

