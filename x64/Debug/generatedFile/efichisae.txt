{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈefit͡ʃisae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[efichisalat]]''
# third person singular present tense positive conjugation of ''[[efichisalat]]''
# second person plural present tense positive conjugation of ''[[efichisalat]]''
# third person plural present tense positive conjugation of ''[[efichisalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efichis|thema=a}}

