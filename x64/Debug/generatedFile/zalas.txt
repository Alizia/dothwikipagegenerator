{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zaˈl̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[zalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zal|epe=}}

