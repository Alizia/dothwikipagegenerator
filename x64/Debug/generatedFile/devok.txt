{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪eˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[devolat]]''
# first person singular present tense negative conjugation of ''[[devolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=dev|thema=o}}

