{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈsak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[hasat]]''


=====Inflection=====
{{Template:dothra-vCC|root=has|epe=}}

