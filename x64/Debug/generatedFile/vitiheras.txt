{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vit̪iheˈɾas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[vitiherat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vitiher|epe=}}

