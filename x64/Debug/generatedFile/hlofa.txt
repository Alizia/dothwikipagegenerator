{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈhl̪ɤfa]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# wrist, ankle

=====Inflection=====
{{Template:dothra-ni|root=hlof|thema=a}}

