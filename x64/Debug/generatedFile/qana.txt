{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈqɑn̪a]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# black stork
# stork (of any kind)
# stork (of any kind)

=====Inflection=====
{{Template:dothra-na|root=qan|thema=a}}

