{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[joˈl̪aj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[yolat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yol|epe=}}

