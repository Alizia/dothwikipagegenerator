{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[od̪iɾˈgak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[dirgat]]''


=====Inflection=====
{{Template:dothra-vCC|root=dirg|epe=e}}

