{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈvoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[havolat]]''
# plural past tense negative conjugation of ''[[havolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hav|thema=o}}

