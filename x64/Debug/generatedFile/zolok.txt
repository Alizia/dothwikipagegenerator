{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zoˈl̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[zolat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zol|epe=}}

