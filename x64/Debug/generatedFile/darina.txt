{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈd̪aɾin̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[darinat]]''


=====Inflection=====
{{Template:dothra-vCC|root=darin|epe=}}

