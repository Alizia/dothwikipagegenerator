{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kuˈvar]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[kovarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kovar|epe=}}

