{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[okaɾˈt̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[kartat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kart|epe=}}

