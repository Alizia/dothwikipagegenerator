{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvoqɔki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[oqolat]]''
# first person plural future tense negative conjugation of ''[[oqolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=oq|thema=o}}

