{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʒean̪ae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[zheanalat]]''
# third person singular future tense positive conjugation of ''[[zheanalat]]''
# second person plural future tense positive conjugation of ''[[zheanalat]]''
# third person plural future tense positive conjugation of ''[[zheanalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zhean|thema=a}}

