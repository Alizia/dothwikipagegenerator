{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[feˈok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[fejat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fej|epe=}}

