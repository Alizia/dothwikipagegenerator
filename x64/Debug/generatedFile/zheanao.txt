{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈʒean̪ao]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense negative conjugation of ''[[zheanalat]]''
# third person singular present tense negative conjugation of ''[[zheanalat]]''
# second person plural present tense negative conjugation of ''[[zheanalat]]''
# third person plural present tense negative conjugation of ''[[zheanalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zhean|thema=a}}

