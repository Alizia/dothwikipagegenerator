{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈt̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[tat]]''


=====Inflection=====
{{Template:dothra-vCC|root=t|epe=}}

