{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈon̪axue]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[nakholat]]''
# third person singular future tense negative conjugation of ''[[nakholat]]''
# second person plural future tense negative conjugation of ''[[nakholat]]''
# third person plural future tense negative conjugation of ''[[nakholat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nakh|thema=o}}

