{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈot͡ʃaɾoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[charolat]]''
# third person singular future tense negative conjugation of ''[[charolat]]''
# second person plural future tense negative conjugation of ''[[charolat]]''
# third person plural future tense negative conjugation of ''[[charolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=char|thema=o}}

