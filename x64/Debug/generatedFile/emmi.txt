{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈemmi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[emmat]]''
# second person singular present tense positive conjugation of ''[[emmat]]''
# second person plural present tense positive conjugation of ''[[emmat]]''
# third person plural present tense positive conjugation of ''[[emmat]]''
# second person singular present tense negative conjugation of ''[[emmat]]''
# second person plural present tense negative conjugation of ''[[emmat]]''
# third person plural present tense negative conjugation of ''[[emmat]]''


=====Inflection=====
{{Template:dothra-vVC|root=emm|epe=}}

