{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪ɾoˈgak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[drogat]]''


=====Inflection=====
{{Template:dothra-vCC|root=drog|epe=e}}

