{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈakkual̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[akkoalat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkoal|epe=}}

