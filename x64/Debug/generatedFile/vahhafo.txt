{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvaħ.hafo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[ahhafat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhaf|epe=}}

