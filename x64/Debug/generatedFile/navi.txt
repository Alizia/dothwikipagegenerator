{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪avi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[navat]]''
# second person singular present tense positive conjugation of ''[[navat]]''
# second person plural present tense positive conjugation of ''[[navat]]''
# third person plural present tense positive conjugation of ''[[navat]]''
# second person singular present tense negative conjugation of ''[[navat]]''
# second person plural present tense negative conjugation of ''[[navat]]''
# third person plural present tense negative conjugation of ''[[navat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nav|epe=}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[navilat]]''
# singular past tense positive conjugation of ''[[navilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nav|thema=i}}

