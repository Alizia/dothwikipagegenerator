{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[okˈkuʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[okkat]]''


=====Inflection=====
{{Template:dothra-vVC|root=okk|epe=}}

