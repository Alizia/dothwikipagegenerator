{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪iˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[nirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nir|epe=}}

