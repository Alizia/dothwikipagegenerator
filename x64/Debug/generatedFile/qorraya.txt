{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈqɔr.raja]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# forearm, "foot (measure)"

=====Inflection=====
{{Template:dothra-ni|root=qorray|thema=a}}

