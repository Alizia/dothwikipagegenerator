{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[joˈmaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[yomat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yom|epe=}}

