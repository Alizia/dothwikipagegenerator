{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈan̪iɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[nirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nir|epe=}}

