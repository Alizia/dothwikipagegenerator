{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[efeˈsaʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[efesalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efes|thema=a}}

