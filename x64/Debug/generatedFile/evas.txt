{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[eˈvas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[evat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ev|epe=}}

