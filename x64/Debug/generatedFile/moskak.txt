{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[mosˈkak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[moskat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mosk|epe=}}

