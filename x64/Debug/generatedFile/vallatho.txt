{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈval̪l̪aθo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[allathat]]''


=====Inflection=====
{{Template:dothra-vVC|root=allath|epe=}}

