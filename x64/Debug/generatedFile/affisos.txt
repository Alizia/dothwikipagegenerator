{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[affiˈsos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[affisat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affis|epe=}}

