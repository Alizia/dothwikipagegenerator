{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[voˈsan̪]|lang=doth}}

===Adverb===
{{head|doth|adverb}}

# not much, not a lot

