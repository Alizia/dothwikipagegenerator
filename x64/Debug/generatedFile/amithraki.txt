{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈamiθɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[mithrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mithr|epe=e}}

