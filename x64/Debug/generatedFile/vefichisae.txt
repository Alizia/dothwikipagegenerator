{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvefit͡ʃisae]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[efichisalat]]''
# third person singular future tense positive conjugation of ''[[efichisalat]]''
# second person plural future tense positive conjugation of ''[[efichisalat]]''
# third person plural future tense positive conjugation of ''[[efichisalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=efichis|thema=a}}

