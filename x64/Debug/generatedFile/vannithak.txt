{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[van̪n̪iˈθak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[annithat]]''


=====Inflection=====
{{Template:dothra-vVC|root=annith|epe=}}

