{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈiz]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[izat]]''


=====Inflection=====
{{Template:dothra-vVC|root=iz|epe=}}

