{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[vod.ˈd͡ʒor]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# deity, god

=====Inflection=====
{{Template:dothra-ni|root=vojjor|thema=}}

