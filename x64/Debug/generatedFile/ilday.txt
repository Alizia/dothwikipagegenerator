{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[il̪ˈd̪aj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[ildat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ild|epe=}}

