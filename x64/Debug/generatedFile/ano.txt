{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈan̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[anat]]''
# singular past tense negative conjugation of ''[[anat]]''
# third person singular present tense negative conjugation of ''[[anat]]''


=====Inflection=====
{{Template:dothra-vVC|root=an|epe=}}

