{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪iˈvoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[nivat]]''


=====Inflection=====
{{Template:dothra-vCC|root=niv|epe=}}

