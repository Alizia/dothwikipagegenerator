{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪ɤsˈtɤs]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[lostat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lost|epe=}}

