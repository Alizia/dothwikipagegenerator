{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoqɑt͡ʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[qachat]]''
# second person plural future tense negative conjugation of ''[[qachat]]''
# third person plural future tense negative conjugation of ''[[qachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qach|epe=}}

