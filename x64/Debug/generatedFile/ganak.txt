{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[gaˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ganat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gan|epe=}}

