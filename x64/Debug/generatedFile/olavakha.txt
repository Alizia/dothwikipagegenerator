{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈol̪avaxa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

