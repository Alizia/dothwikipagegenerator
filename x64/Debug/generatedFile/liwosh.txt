{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪iˈwoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[liwalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=liw|thema=a}}

