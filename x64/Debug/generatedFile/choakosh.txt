{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t͡ʃoaˈkuʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[choakat]]''


=====Inflection=====
{{Template:dothra-vCC|root=choak|epe=}}

