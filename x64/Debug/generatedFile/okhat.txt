{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[oˈxat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to ring

=====Inflection=====
{{Template:dothra-vVC|root=okh|epe=}}

