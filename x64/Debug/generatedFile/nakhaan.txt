{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[n̪axaˈan̪]|lang=doth}}

===Adverb===
{{head|doth|adverb}}

# completely, totally, fully (lit. 'to stopping')

