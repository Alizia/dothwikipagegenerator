{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aħ.haˈsaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[ahhasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ahhas|epe=}}

