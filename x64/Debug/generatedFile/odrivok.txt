{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[od̪ɾiˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[drivolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=driv|thema=o}}

