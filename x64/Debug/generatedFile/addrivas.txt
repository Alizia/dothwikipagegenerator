{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ad̪d̪ɾiˈvas]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[addrivat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addriv|epe=}}

