{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[θaˈʃoʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[thasholat]]''
# plural past tense negative conjugation of ''[[thasholat]]''


=====Inflection=====
{{Template:dothra-vCV|root=thash|thema=o}}

