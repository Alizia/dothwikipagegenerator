{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[n̪ir.ˈras]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[nirrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nirr|epe=e}}

