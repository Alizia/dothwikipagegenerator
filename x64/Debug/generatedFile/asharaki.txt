{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʃaɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[sharat]]''


=====Inflection=====
{{Template:dothra-vCC|root=shar|epe=}}

