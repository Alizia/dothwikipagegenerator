{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈfil̪l̪a]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# dried plum

=====Inflection=====
{{Template:dothra-ni|root=fill|thema=a|class=b}}

