{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[qɑjeˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to float

=====Inflection=====
{{Template:dothra-vCV|root=qay|thema=e}}

