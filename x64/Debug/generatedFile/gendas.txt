{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[gen̪ˈd̪as]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[gendat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gend|epe=}}

