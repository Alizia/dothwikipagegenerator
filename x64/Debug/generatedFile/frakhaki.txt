{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfɾaxaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[frakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=frakh|epe=}}

