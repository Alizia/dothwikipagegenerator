{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈʒixuki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[zhikhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zhikh|epe=}}

