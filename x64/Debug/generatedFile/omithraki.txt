{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈomiθɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[mithrat]]''


=====Inflection=====
{{Template:dothra-vCC|root=mithr|epe=e}}

