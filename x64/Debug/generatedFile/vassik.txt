{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vasˈsik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[assilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ass|thema=i}}

