{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvar.remeki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[arremekat]]''
# second person plural future tense positive conjugation of ''[[arremekat]]''
# third person plural future tense positive conjugation of ''[[arremekat]]''
# second person singular future tense negative conjugation of ''[[arremekat]]''
# second person plural future tense negative conjugation of ''[[arremekat]]''
# third person plural future tense negative conjugation of ''[[arremekat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arremek|epe=}}

