{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈohojal̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[hoyalat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hoyal|epe=}}

