{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈassamvaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[assamvat]]''


=====Inflection=====
{{Template:dothra-vVC|root=assamv|epe=}}

