{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[gɾad̪ˈd̪ax]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# waste, refuse

=====Inflection=====
{{Template:dothra-ni|root=graddakh|thema=}}

===Interjection===
{{head|doth|interjection}}

# shit (generic undirected profanity)

