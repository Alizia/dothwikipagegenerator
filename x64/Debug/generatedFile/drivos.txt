{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪ɾiˈvos]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal negative imperative conjugation of ''[[drivat]]''


=====Inflection=====
{{Template:dothra-vCC|root=driv|epe=}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[drivolat]]''
# informal negative imperative conjugation of ''[[drivolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=driv|thema=o}}

