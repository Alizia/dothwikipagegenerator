{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪iwa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[liwalat]]''
# singular past tense positive conjugation of ''[[liwalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=liw|thema=a}}

