{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaviguveɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[vigoverat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vigover|epe=}}

