{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈt͡ʃɾok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[achralat]]''


=====Inflection=====
{{Template:dothra-vVV|root=achr|thema=a}}

