{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[gen̪ˈd̪]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[gendat]]''


=====Inflection=====
{{Template:dothra-vCC|root=gend|epe=}}

