{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaɾθasaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[arthasat]]''


=====Inflection=====
{{Template:dothra-vVC|root=arthas|epe=}}

