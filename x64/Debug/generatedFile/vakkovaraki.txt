{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvakkuvaɾaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[akkovarat]]''


=====Inflection=====
{{Template:dothra-vVC|root=akkovar|epe=}}

