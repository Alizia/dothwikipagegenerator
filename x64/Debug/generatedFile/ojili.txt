{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoil̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense negative conjugation of ''[[jilat]]''
# second person plural future tense negative conjugation of ''[[jilat]]''
# third person plural future tense negative conjugation of ''[[jilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jil|epe=}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ojilat]]''
# second person singular present tense positive conjugation of ''[[ojilat]]''
# second person plural present tense positive conjugation of ''[[ojilat]]''
# third person plural present tense positive conjugation of ''[[ojilat]]''
# second person singular present tense negative conjugation of ''[[ojilat]]''
# second person plural present tense negative conjugation of ''[[ojilat]]''
# third person plural present tense negative conjugation of ''[[ojilat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ojil|epe=}}

