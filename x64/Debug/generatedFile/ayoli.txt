{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈajol̪i]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[ayolat]]''
# second person singular present tense positive conjugation of ''[[ayolat]]''
# second person plural present tense positive conjugation of ''[[ayolat]]''
# third person plural present tense positive conjugation of ''[[ayolat]]''
# second person singular present tense negative conjugation of ''[[ayolat]]''
# second person plural present tense negative conjugation of ''[[ayolat]]''
# third person plural present tense negative conjugation of ''[[ayolat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ayol|epe=}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[yolat]]''
# second person plural future tense positive conjugation of ''[[yolat]]''
# third person plural future tense positive conjugation of ''[[yolat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yol|epe=}}

