{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈaqqɑ]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# "executioner", person responsible for someones death

=====Inflection=====
{{Template:dothra-ni|root=jaqq|thema=a|class=b}}

