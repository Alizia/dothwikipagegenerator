{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[kaˈd̪iʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[kadat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kad|epe=}}

