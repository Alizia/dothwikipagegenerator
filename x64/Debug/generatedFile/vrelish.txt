{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vɾeˈl̪iʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[vrelat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vrel|epe=}}

