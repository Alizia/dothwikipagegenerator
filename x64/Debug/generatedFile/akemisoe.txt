{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈakemisoe]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular future tense positive conjugation of ''[[kemisolat]]''
# third person singular future tense positive conjugation of ''[[kemisolat]]''
# second person plural future tense positive conjugation of ''[[kemisolat]]''
# third person plural future tense positive conjugation of ''[[kemisolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=kemis|thema=o}}

