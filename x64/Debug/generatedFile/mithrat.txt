{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[miθˈɾat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to rest

=====Inflection=====
{{Template:dothra-vCC|root=mithr|epe=e}}

