{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[t̪iˈhik]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[tihilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=tih|thema=i}}

