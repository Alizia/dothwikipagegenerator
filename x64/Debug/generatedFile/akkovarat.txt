{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[akkuvaˈɾat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to stand (something) up

=====Inflection=====
{{Template:dothra-vVC|root=akkovar|epe=}}

