{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[fl̪aˈsix]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# update

=====Inflection=====
{{Template:dothra-ni|root=flasikh|thema=}}

