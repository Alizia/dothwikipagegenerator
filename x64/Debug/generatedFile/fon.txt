{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈfon̪]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[fonat]]''


=====Inflection=====
{{Template:dothra-vCC|root=fon|epe=}}

