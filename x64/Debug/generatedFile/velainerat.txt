{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[vel̪ain̪eˈɾat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to sow

=====Inflection=====
{{Template:dothra-vCC|root=velainer|epe=}}

