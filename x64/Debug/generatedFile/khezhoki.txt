{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈxeʒoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[khezhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=khezh|epe=}}

