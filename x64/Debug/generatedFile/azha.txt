{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈaʒa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[azhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azh|epe=}}

