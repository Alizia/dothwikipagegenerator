{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪ɤstɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[lostat]]''
# singular past tense negative conjugation of ''[[lostat]]''
# third person singular present tense negative conjugation of ''[[lostat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lost|epe=}}

