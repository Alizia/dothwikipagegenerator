{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oveˈxak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[vekhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vekh|epe=}}

