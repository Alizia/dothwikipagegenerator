{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[al̪l̪ajaˈfaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[allayafat]]''


=====Inflection=====
{{Template:dothra-vVC|root=allayaf|epe=}}

