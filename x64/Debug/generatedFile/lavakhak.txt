{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[l̪avaˈxak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[lavakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lavakh|epe=}}

