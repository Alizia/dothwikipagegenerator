{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈelzo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[elzat]]''
# singular past tense negative conjugation of ''[[elzat]]''
# third person singular present tense negative conjugation of ''[[elzat]]''


=====Inflection=====
{{Template:dothra-vVC|root=elz|epe=}}

