{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈad̪d̪ɾivi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[addrivat]]''
# second person singular present tense positive conjugation of ''[[addrivat]]''
# second person plural present tense positive conjugation of ''[[addrivat]]''
# third person plural present tense positive conjugation of ''[[addrivat]]''
# second person singular present tense negative conjugation of ''[[addrivat]]''
# second person plural present tense negative conjugation of ''[[addrivat]]''
# third person plural present tense negative conjugation of ''[[addrivat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addriv|epe=}}

