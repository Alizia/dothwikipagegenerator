{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vawaˈzak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[awazat]]''


=====Inflection=====
{{Template:dothra-vVC|root=awaz|epe=}}

