{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈeʃo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[eshat]]''
# singular past tense negative conjugation of ''[[eshat]]''
# third person singular present tense negative conjugation of ''[[eshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=esh|epe=}}

