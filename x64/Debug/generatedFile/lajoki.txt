{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪aoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[lajat]]''


=====Inflection=====
{{Template:dothra-vCC|root=laj|epe=}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[lajilat]]''


=====Inflection=====
{{Template:dothra-vCV|root=laj|thema=i}}

