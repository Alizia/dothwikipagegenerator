{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈsieɾa]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# nephew

=====Inflection=====
{{Template:dothra-na|root=sier|thema=a}}

