{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ageˈɾak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[gerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ger|epe=}}

