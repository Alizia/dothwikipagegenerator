{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[asˈt]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[astat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ast|epe=}}

