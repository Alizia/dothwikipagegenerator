{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈven̪]|lang=doth}}

===Conjunction===
{{head|doth|conjunction}}

# like, as

#:Anha adothrak hrazef ido yomme Havazzhifi Kazga ven et vo khal avvos.
#::I will ride wooden horses across the black salt sea as no khal has done before.

===Verb===
{{head|doth|verb form}}

# singular past tense positive conjugation of ''[[venat]]''


=====Inflection=====
{{Template:dothra-vCC|root=ven|epe=}}

