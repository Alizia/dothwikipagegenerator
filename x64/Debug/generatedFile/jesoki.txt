{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈesoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[jesat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jes|epe=}}

