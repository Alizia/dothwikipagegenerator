{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[samˈvok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[samvolat]]''
# first person singular present tense negative conjugation of ''[[samvolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=samv|thema=o}}

