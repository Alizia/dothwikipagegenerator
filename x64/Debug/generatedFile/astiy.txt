{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[asˈtij]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[astilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ast|thema=i}}

