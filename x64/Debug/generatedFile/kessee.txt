{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈkessee]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# second person singular present tense positive conjugation of ''[[kesselat]]''
# third person singular present tense positive conjugation of ''[[kesselat]]''
# second person plural present tense positive conjugation of ''[[kesselat]]''
# third person plural present tense positive conjugation of ''[[kesselat]]''


=====Inflection=====
{{Template:dothra-vCV|root=kess|thema=e}}

