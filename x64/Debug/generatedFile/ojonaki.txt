{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoon̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[jonat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jon|epe=}}

