{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈhoeɾoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[hoerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=hoer|epe=}}

