{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈjol̪ɤ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[yolat]]''
# singular past tense negative conjugation of ''[[yolat]]''
# third person singular present tense negative conjugation of ''[[yolat]]''


=====Inflection=====
{{Template:dothra-vCC|root=yol|epe=}}

