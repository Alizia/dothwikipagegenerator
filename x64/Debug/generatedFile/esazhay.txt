{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[esaˈʒaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[esazhalat]]''


=====Inflection=====
{{Template:dothra-vVV|root=esazh|thema=a}}

