{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈkuʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense negative conjugation of ''[[hakelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hak|thema=e}}

