{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈt̪ɤk]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[tat]]''


=====Inflection=====
{{Template:dothra-vCC|root=t|epe=}}

