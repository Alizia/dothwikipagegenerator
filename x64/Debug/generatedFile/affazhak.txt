{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[affaˈʒak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[affazhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=affazh|epe=}}

