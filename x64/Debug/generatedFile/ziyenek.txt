{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zijeˈn̪ek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[ziyenelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=ziyen|thema=e}}

