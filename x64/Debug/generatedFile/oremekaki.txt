{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoɾemekaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[remekat]]''


=====Inflection=====
{{Template:dothra-vCC|root=remek|epe=}}

