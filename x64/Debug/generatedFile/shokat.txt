{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ʃoˈkat̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to jump over

=====Inflection=====
{{Template:dothra-vCC|root=shok|epe=}}

