{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[d̪ɤˈn̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense positive conjugation of ''[[donat]]''


=====Inflection=====
{{Template:dothra-vCC|root=don|epe=}}

