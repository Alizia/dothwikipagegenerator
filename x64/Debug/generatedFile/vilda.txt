{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvil̪d̪a]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense positive conjugation of ''[[ildat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ild|epe=}}

