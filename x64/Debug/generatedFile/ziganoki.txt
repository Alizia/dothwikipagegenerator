{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈzigan̪ɤki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[ziganelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zigan|thema=e}}

