{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪eva]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[nevalat]]''
# singular past tense positive conjugation of ''[[nevalat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nev|thema=a}}

