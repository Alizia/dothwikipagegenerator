{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oqɔˈθak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[qothat]]''


=====Inflection=====
{{Template:dothra-vCC|root=qoth|epe=}}

