{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈl̪iɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[lirat]]''


=====Inflection=====
{{Template:dothra-vCC|root=lir|epe=}}

