{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[rhaˈeʃ]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, animate}}

# land, country

=====Inflection=====
{{Template:dothra-na|root=rhaesh|thema=}}

