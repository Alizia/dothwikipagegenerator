{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈt͡ʃaɾe]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# ear

=====Inflection=====
{{Template:dothra-ni|root=char|thema=e}}

