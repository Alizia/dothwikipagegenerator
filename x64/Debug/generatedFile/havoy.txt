{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[haˈvoj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[havolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hav|thema=o}}

