{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvɾel̪aki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[vrelat]]''


=====Inflection=====
{{Template:dothra-vCC|root=vrel|epe=}}

