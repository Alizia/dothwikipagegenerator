{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈn̪axu]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[nakhat]]''
# singular past tense negative conjugation of ''[[nakhat]]''
# third person singular present tense negative conjugation of ''[[nakhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=nakh|epe=}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# stop, end

#:K'asi assikhqoyisiri vezh adothrae nakhaan rhaesheseri.
#::According to the prophecy the stallion will ride to the ends of the world.

=====Inflection=====
{{Template:dothra-ni|root=nakh|thema=o}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[nakholat]]''
# singular past tense positive conjugation of ''[[nakholat]]''
# formal negative imperative conjugation of ''[[nakholat]]''
# singular past tense negative conjugation of ''[[nakholat]]''


=====Inflection=====
{{Template:dothra-vCV|root=nakh|thema=o}}

