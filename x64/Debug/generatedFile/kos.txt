{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈkus]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# informal positive imperative conjugation of ''[[kolat]]''
# informal negative imperative conjugation of ''[[kolat]]''


=====Inflection=====
{{Template:dothra-vCV|root=k|thema=o}}

