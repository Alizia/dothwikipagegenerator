{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈokɾe]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, unknown animate type}}

# tent

=====Inflection=====
{{Template:dothra-ni|root=okr|thema=e}}

