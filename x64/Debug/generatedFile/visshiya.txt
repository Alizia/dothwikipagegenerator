{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈvisʃija]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# color

=====Inflection=====
{{Template:dothra-ni|root=visshiy|thema=a}}

