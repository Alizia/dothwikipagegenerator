{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[vad̪d̪aˈxak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[addakhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=addakh|epe=}}

