{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aiˈl̪ak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[jilat]]''


=====Inflection=====
{{Template:dothra-vCC|root=jil|epe=}}

