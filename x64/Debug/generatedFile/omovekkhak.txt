{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[omovekˈxak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[movekkhat]]''


=====Inflection=====
{{Template:dothra-vCC|root=movekkh|epe=}}

