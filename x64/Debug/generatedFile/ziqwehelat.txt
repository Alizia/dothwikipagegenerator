{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ziqweheˈl̪at̪]|lang=doth}}

===Verb===
{{head|doth|verb|infinitive}}

to retch

=====Inflection=====
{{Template:dothra-vCV|root=ziqweh|thema=e}}

