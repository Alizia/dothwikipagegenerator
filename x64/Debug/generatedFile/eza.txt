{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈeza]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[ezat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ez|epe=}}

