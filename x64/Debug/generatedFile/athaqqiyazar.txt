{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[aθaqqejaˈzar]|lang=doth}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# spilling of blood

#:Me izvena, jin athaqqiyazar she vaesof.
#::It is forbidden to spill blood in the sacred city.

=====Inflection=====
{{Template:dothra-ni|root=athaqqiyazar|thema=}}

