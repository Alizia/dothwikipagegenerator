{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[zoˈɾaj]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# past participle conjugation of ''[[zorat]]''


=====Inflection=====
{{Template:dothra-vCC|root=zor|epe=}}

