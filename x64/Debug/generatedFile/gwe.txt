{{lexeme|Dothraki}}

=== Etymology ===
{{invis}}

====Pronunciation====
{{IPA|[ˈgwe]|lang=doth}}

===Interjection===
{{head|doth|interjection}}

# Here! Let's go! Go!

