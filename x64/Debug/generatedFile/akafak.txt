{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[akaˈfak]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense positive conjugation of ''[[kafat]]''


=====Inflection=====
{{Template:dothra-vCC|root=kaf|epe=}}

