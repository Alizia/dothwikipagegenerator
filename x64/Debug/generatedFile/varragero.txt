{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvar.rageɾo]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal negative imperative conjugation of ''[[varragerat]]''
# singular past tense negative conjugation of ''[[varragerat]]''
# third person singular present tense negative conjugation of ''[[varragerat]]''


=====Inflection=====
{{Template:dothra-vCC|root=varrager|epe=}}

