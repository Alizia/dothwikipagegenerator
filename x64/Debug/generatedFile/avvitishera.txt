{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈavvit̪iʃeɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular present tense positive conjugation of ''[[avvitisherat]]''


=====Inflection=====
{{Template:dothra-vVC|root=avvitisher|epe=}}

