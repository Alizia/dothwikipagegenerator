{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈezoki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense negative conjugation of ''[[ezat]]''


=====Inflection=====
{{Template:dothra-vVC|root=ez|epe=}}

===Verb===
{{head|doth|verb form}}

# first person plural present tense positive conjugation of ''[[ezolat]]''
# first person plural present tense negative conjugation of ''[[ezolat]]''


=====Inflection=====
{{Template:dothra-vVV|root=ez|thema=o}}

