{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[aˈʒiʃ]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[azhat]]''


=====Inflection=====
{{Template:dothra-vVC|root=azh|epe=}}

===Verb===
{{head|doth|verb form}}

# plural past tense positive conjugation of ''[[azhilat]]''


=====Inflection=====
{{Template:dothra-vVV|root=azh|thema=i}}

