{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈohemeki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense negative conjugation of ''[[hemelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hem|thema=e}}

