{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[oheˈmek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[hemelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=hem|thema=e}}

