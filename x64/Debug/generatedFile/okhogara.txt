{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈoxugaɾa]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# third person singular future tense negative conjugation of ''[[khogarat]]''


=====Inflection=====
{{Template:dothra-vCC|root=khogar|epe=}}

