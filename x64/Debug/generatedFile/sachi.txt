{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈsat͡ʃi]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# formal positive imperative conjugation of ''[[sachat]]''
# second person singular present tense positive conjugation of ''[[sachat]]''
# second person plural present tense positive conjugation of ''[[sachat]]''
# third person plural present tense positive conjugation of ''[[sachat]]''
# second person singular present tense negative conjugation of ''[[sachat]]''
# second person plural present tense negative conjugation of ''[[sachat]]''
# third person plural present tense negative conjugation of ''[[sachat]]''


=====Inflection=====
{{Template:dothra-vCC|root=sach|epe=}}

====Noun====
{{head|doth|noun|nominative, inanimate}}

# half

=====Inflection=====
{{Template:dothra-ni|root=sach|thema=i|class=b}}

