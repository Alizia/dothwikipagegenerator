{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[saˈok]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular present tense negative conjugation of ''[[sajat]]''


=====Inflection=====
{{Template:dothra-vCC|root=saj|epe=}}

