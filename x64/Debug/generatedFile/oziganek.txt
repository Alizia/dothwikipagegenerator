{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ozigaˈn̪ek]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person singular future tense negative conjugation of ''[[ziganelat]]''


=====Inflection=====
{{Template:dothra-vCV|root=zigan|thema=e}}

