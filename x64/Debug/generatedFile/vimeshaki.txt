{{lexeme|Dothraki}}

====Pronunciation====
{{IPA|[ˈvimeʃaki]|lang=doth}}

===Verb===
{{head|doth|verb form}}

# first person plural future tense positive conjugation of ''[[imeshat]]''


=====Inflection=====
{{Template:dothra-vVC|root=imesh|epe=}}

